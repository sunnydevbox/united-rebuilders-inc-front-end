import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
  'id' : [null],
  
  'type': [null, Validators.compose([Validators.required])],
  'payroll_id' : [null, Validators.compose([Validators.required])],
  'name' : [null, Validators.compose([Validators.required])],
  'value' : [null, Validators.compose([Validators.required])],
  'multiplier' : [null, Validators.compose([Validators.required])],
  'reference' : [null, Validators.compose([])],
  'data' : [null, Validators.compose([])],
  
  // type
 
}
