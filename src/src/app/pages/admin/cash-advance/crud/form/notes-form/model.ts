import { FormBuilder, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
  'notes': [null, Validators.compose([Validators.required])],
}