import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class EmploymentStatusService extends AbstractService {
    endpoint = 'employment-status';
}
