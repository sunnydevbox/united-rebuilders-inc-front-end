import { Component, OnInit } from '@angular/core';
import { settings } from './settings';

@Component({
  selector: 'app-employee-template',
  templateUrl: './employee-template.component.html',
  styleUrls: ['./employee-template.component.css']
})
export class EmployeeTemplateComponent implements OnInit {
  
  public settings;
  public object;
  public triggerReload;

  constructor() { 
    this.settings = settings;
  }

  selectedObject(object) {
    this.object = object;
  }
  
  triggerReloadList(e) {
    this.triggerReload = e;
  }

  ngOnInit() {
  }

}
