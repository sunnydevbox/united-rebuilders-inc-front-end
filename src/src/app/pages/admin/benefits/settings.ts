import { BenefitsService } from '@services/benefits.service';

export const settings = {
  plural: 'Benefits',
  singular: 'Benefit',
  service: BenefitsService,

  crud: {
    create: {
      allow: true, // true/false
      url: '/app/system/benefits'
    },
    read: {

    },
    update: {
      allow: true,
      url: '/app/system/benefits/edit/'
    },
    delete: {
      allow: true
    },
    list: {
      allow: true,
      url: '/app/system/benefits'
    }
  }
};
