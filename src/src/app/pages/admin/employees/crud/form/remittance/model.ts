import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';

export const MODEL = {
  'id' : [null],
  'type': ['remittance'],
  'deduction_id': [null],
  'employee_id': [null]
};
