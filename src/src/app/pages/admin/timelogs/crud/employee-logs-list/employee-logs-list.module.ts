import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutingModule } from './routing.module';
import { EmployeeLogsListComponent } from './employee-logs-list.component';
import { UserService } from '@services/user.service';
import { ListModule } from './list/list.module';
import { NewTimelogModule } from './../../crud/new-timelog/new-timelog.module';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    ListModule,
    NewTimelogModule,
  ],
  declarations: [
    EmployeeLogsListComponent,
  ],
  exports: [
    EmployeeLogsListComponent,
  ],
  providers: [
    UserService,  
  ]
})
export class EmployeeLogsListModule { }
