import { Component, OnInit } from '@angular/core';
import { settings } from './../settings';
import { ListComponent as BaseListComponent } from '@shared/base/list/list.component' ;

@Component({
  selector: 'app-generate-report',
  templateUrl: './generate-report.component.html',
  styleUrls: ['./generate-report.component.css']
})
export class GenerateReportComponent extends BaseListComponent {

  _postConstruct() { 
    this.loadListOnInit = false;
    this.service = this.injector.get(settings.service)
    this.items = [];
  }

  objectChanged(e) {
    this.items = e;
  }
}
