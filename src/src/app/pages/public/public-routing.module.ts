import { NgModule } from '@angular/core';
import { Routes, RouterModule, CanActivate } from '@angular/router';

import { Route } from './../../core/theme/public-theme/public-routing.service';
import { GuestOnlyGuard } from '../../core/guards/guest-only.guard';


const routes: Routes = Route.withShell([
  {
    path: '',
    // loadChildren: './phome/phome.module#PhomeModule',
    redirectTo: 'login', pathMatch: 'full',
  },
  {
    path: 'login',
    loadChildren: './login/login.module#LoginModule',
    data: { title: 'Login' },
    canActivate: [
      GuestOnlyGuard
    ]
  }
]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [
    GuestOnlyGuard,
  ]
})
export class PublicRoutingModule { }
