
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
    'name' : [null, Validators.compose([Validators.required, Validators.minLength(2)])],
    'description': [null],
    'country': ['PH'],
    'region': [null],
  }

export const ERRORS = [
  { name: 'required', text: 'This field is required', rules: ['touched', 'dirty'] },
];