import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayrollGeneratorComponent } from './payroll-generator.component';

describe('PayrollGeneratorComponent', () => {
  let component: PayrollGeneratorComponent;
  let fixture: ComponentFixture<PayrollGeneratorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayrollGeneratorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayrollGeneratorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
