import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class CivilStatusService extends AbstractService {
    endpoint = 'civil-status';
}
