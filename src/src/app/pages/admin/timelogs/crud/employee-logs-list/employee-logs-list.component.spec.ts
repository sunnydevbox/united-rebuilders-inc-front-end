import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeLogsListComponent } from './employee-logs-list.component';

describe('EmployeeLogsListComponent', () => {
  let component: EmployeeLogsListComponent;
  let fixture: ComponentFixture<EmployeeLogsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeLogsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeLogsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
