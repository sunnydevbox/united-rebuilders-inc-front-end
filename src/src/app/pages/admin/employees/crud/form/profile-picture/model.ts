import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';

export const MODEL = {
  'id' : [null],
  'type': ['profile-picture'],

  'profile_picture' : [null, Validators.compose([])],
};
