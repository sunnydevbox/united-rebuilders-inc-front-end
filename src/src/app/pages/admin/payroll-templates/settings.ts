import { PayrollTemplateService } from '@services/payroll-template.service';

export const settings = {
    plural: 'Payroll Templates',
    singular: 'Payroll Template',
    service: PayrollTemplateService,
  
    crud: {
      create: {
        allow: true, // true/false
        url: '/admin/roles/create'
      },
      read:{
  
      },
      update:{
        allow: true,
        url: '/admin/roles/edit/'
      },
      delete:{
        allow: true
      },
      list: {
        allow: true,
        url: '/admin/roles'
      }
    }
  };