import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
  'id' : [null],
  'first_name' : [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(30)])],
  'last_name' : [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(30)])],
  'email' : [null, Validators.compose([Validators.required, Validators.pattern("[^ @]*@[^ @]*")])],
}


export const MODEL_PASSWORD_CHANGE = {
  'id' : [null],
  'password' : [null, Validators.compose([Validators.required, Validators.minLength(8)])],
  'repeat_password' : [null, Validators.compose([Validators.required, Validators.minLength(8)])],
}

/*

{
      validator: validators.fieldMatchValidator(['password', 'confirmPassword'],
        'Password does not match')
    }
*/


export const ERRORS = [
  { name: 'required', text: 'This field is required', rules: ['touched', 'dirty'] },
  { name: 'minlength', text: 'Minimum number of characters ', rules: ['touched', 'dirty'] },
  { name: 'maxlength', text: 'Maximum number of characters ', rules: ['touched', 'dirty'] }
];
