import { Component, OnInit, Output } from '@angular/core';

import { FormComponent } from './../form/form.component';
@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css']
})
export class FiltersComponent extends FormComponent {

  public query = [];
  public model;
  protected patchValueOnReset = true;

  @Output() filtersChanged;

  startFilter() {
    // BUILD THE QUERY
    this.query = [];

    for (const field in this.model) {
      if (true) {
        this.fieldProcessor(field);
      }
    }

    this.filtersChanged.emit({
      query: this.query.join(';'),
    });
  }


  /**
   * fieldProcessor
   *
   * This can be overriden to add conditions
   */
  fieldProcessor(field) {
    if (this.form.get(field) && this.form.get(field).value) {
      this.query.push(field + '|' + this.form.get(field).value);
    }
  }

  /**
   * setPatchValueOnReset
   *
   * @param flag
   */
  setPatchValueOnReset(flag: boolean) {
    if (typeof flag === 'undefined') {
      flag = true;
    }

    this.patchValueOnReset = flag;
  }

  /**
   * getPatchValueOnReset
   */
  getPatchValueOnReset() {
    return this.patchValueOnReset;
  }

  /**
   * reset
   */
  reset() {
    this.form.reset();

    if (this.patchValueOnReset) {
      this.form.patchValue(this.model);
    }

    this.setPatchValueOnReset(true);

    this.startFilter();
  }

}
