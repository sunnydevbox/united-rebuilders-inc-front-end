import { Routes } from '@angular/router';

import { PublicThemeComponent } from './public-theme.component';
//import { AuthenticationGuard } from './authentication/authentication.guard';

/**
 * Provides helper methods to create routes.
 */
export class Route {

  /**
   * Creates routes using the shell component and authentication.
   * @param routes The routes to add.
   * @return {Routes} The new routes using shell as the base.
   */
  static withShell(routes: Routes, theme?: string): Routes {
    
      return [{
        path: '',
        component: PublicThemeComponent,
        children: routes,
        canActivate: [
          
        ],
        // Reuse ShellComponent instance when navigating between child views
        data: { reuse: true }
      }];
  }

}
