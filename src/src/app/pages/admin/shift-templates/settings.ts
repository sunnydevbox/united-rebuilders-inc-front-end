import { ShiftTemplateService } from '@services/shift-template.service';

export const settings = {
    plural: 'Shift Templates',
    singular: 'Shift Template',
    service: ShiftTemplateService,

    crud: {
      create: {
        allow: true, // true/false
        url: '/admin/settings/system/shift-templates/create'
      },
      read: {

      },
      update: {
        allow: true,
        url: '/admin/settings/system/shift-templates/edit/'
      },
      delete: {
        allow: true
      },
      list: {
        allow: true,
        url: '/admin/settings/system/shift-templates'
      }
    }
  };
