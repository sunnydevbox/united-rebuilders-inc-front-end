
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
    'name': [null],
    'status': ['all'],
    'with-status': ['all'],
    'date_start': [null],
    // 'date_end': [null],
};
