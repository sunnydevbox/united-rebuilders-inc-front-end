import { Component, OnChanges, SimpleChanges, Input } from '@angular/core';
import { PayrollService } from '@services/payroll.service';
import { ListComponent as BaseListComponent } from '@shared/base/list/list.component';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent extends BaseListComponent implements OnChanges {

  private payrollService;
  public payrolls = null;
  public pageLimit = 25;

  @Input() payrollLogId;

  _postConstruct() { 
    this.payrollService = this.injector.get(PayrollService);
  }

  refreshList(payrollLogId) {
    if (!payrollLogId) {
      payrollLogId = this.payrollLogId;
    }
    this.toggleOverlay = true;
    this.payrollService.index({
      filter: 'id;company_id;name;job_position;department;basic_pay;total_gross;total_deduction;total_net;hourly_rate;status;'
              + 'payroll_log_id;total_adjustments;wtax;total_adj_earnings;total_adj_deductions;daily_rate;total_hours;total_days;'
              + 'total_undertime;total_holiday_pay;total_paid_leaves_pay;total_overtime_pay;total_overtime;total_restday_pay;'
              + 'total_restday;total_holiday;total_holiday_pay',
      search: 'payroll_log_id:'+ payrollLogId,
      with: 'items',
      limit: this.pageLimit,
    })
    .subscribe((res) => {
      this.toggleOverlay = false;
      this.payrolls = res.data;
    });
  }

  grab(items, id, includeData = false) {
    let value = 0;
    items.forEach(function(item) {
      if (item.name.toLowerCase() == id) {
        value = (includeData) ? item : item.total_net;
      }
    }); 

    return value;
  }

  setPageLimit(e) {
    this.pageLimit = e.target.value;
    this.refreshList(this.payrollLogId);
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.payrollLogId && changes.payrollLogId.currentValue) {
      this.refreshList(changes.payrollLogId.currentValue);
    }
  }


}
