import { Injectable, Inject, ComponentFactoryResolver } from '@angular/core';

import { NgbModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from './modal.component';

@Injectable()
export class ModalService {

  closeResponse: boolean;
  closeResult: string;
  isOpen: Boolean = false;
  template;

  constructor(
    private modalService: NgbModal
  ) {
  }

  setSize(settings) {
    let size = 'lg';

    if (settings['size']) {
      size = settings['size'];
    }

    return size;
  }

  open(modalOptions: NgbModalOptions = {size: 'lg'}, modalSettings, successCallback = null, failureCallback = null) {
 
    // let options: NgbModalOptions = {
    //   size: this.setSize(modalSettings)
    // };

    // console.log(options);
    const modalRef = this.modalService.open(ModalComponent, modalOptions);
    
    // this.componentInstance(modalSettings);

    modalRef.componentInstance.createComponent(this.template, modalSettings.data);

    if (typeof modalSettings.ctaPositive != 'undefined') {
      modalRef.componentInstance.ctaPositive = modalSettings.ctaPositive;
    }

    if (typeof modalSettings.ctaNegative != 'undefined') {
      modalRef.componentInstance.ctaNegative  = modalSettings.ctaNegative;
    }

    if (typeof modalSettings.title != 'undefined') {
      modalRef.componentInstance.title = modalSettings.title;
    }

    modalRef.result.then((result) => {
      this.closeResponse = result;
      this.closeResult = `Closed with: ${result}`;

      if (result) {
        if (successCallback) {
          successCallback(result)
        }
      } else {
        if (failureCallback) {
          failureCallback(result)
        }
      }
    }, (reason) => {
      this.closeResponse = null;
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });

    return modalRef;
  }

  public setComponent(component) {
    this.template = component;
  }

  private componentInstance(modalSettings) {
    let component = null;
    if (this.template) {
      component = this.template;
    } else {
      component = (!modalSettings.template) ? ModalComponent : modalSettings.template;
    }
    
    return this.modalService.open(component);
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
