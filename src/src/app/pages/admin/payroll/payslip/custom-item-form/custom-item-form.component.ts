import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';
import { PayrollItemService } from '@services/payroll-item.service';
import { MODEL } from './model';

@Component({
  selector: 'app-custom-item-form',
  templateUrl: './custom-item-form.component.html',
  styleUrls: ['./custom-item-form.component.css']
})
export class CustomItemFormComponent extends BaseFormComponent implements OnChanges, OnInit {

  // @Input() payslip = 123;
  public payslip;
  @Input() item;
  private activeModal;
  @Output() closeForm = new EventEmitter();
  @Output() triggerReloadPayslipChanged = new EventEmitter();

  _postConstruct() {
    this.form = this.formBuilder.group(MODEL);
    this.service = this.injector.get(PayrollItemService);
    this.form.get('payroll_id').setValue(this.router.parseUrl(this.router.url).root.children.primary.segments[5].path);
  }

  _preSubmit() {
    this.form.get('data').setValue({
      value: this.form.get('value').value,
      multiplier: this.form.get('multiplier').value,
      reference: this.form.get('reference').value
    });
  }

  cancel() {
    this.closeForm.emit(true);
  }

  _onUpdateSuccess(res) {
    super._onUpdateSuccess(res);
    this.triggerReloadPayslipChanged.emit(true);
  }

  _onSaveSuccess(res) {
    super._onSaveSuccess(res);
    this.triggerReloadPayslipChanged.emit(true);
  }

  _submitSave() {
    this.service.customEntry(this.form.value).subscribe(
      (res: any) => {
        if (typeof res === 'string') {
          res = JSON.parse(res);
        }

        this._onSaveSuccess(res);
      },
      (err: any) => {
        this._onSaveError(err);
      },
      () => {
        this._onSaveComplete();
      }
    );
  }

  ngOnInit() {
    if (this.item) {
      this.objectId = this.item.id;
      this.formMode = 'update';
      this.form.get('type').setValue(this.item.type);
      this.form.get('name').setValue(this.item.name);
      this.form.get('value').setValue(this.item.data.value);
      console.log(this.form.get('multiplier').value, this.item.data.multiplier, this.item);
      this.form.get('multiplier').setValue(this.item.data.multiplier);
      console.log(this.form.get('multiplier').value, this.item.data.multiplier, this.item);
      this.form.get('reference').setValue(this.item.data.reference ? this.item.data.reference.id : this.item.data.reference);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.payslip && changes.payslip.currentValue) {
      // console.log(changes.payslip);
    }
  }
  
}
