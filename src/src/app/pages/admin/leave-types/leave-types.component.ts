import { Component, OnInit } from '@angular/core';

import { settings } from './settings';

@Component({
  selector: 'app-leave-types',
  templateUrl: './leave-types.component.html',
  styleUrls: ['./leave-types.component.css']
})
export class LeaveTypesComponent implements OnInit {

  public settings;

  constructor() {
    this.settings = settings;
  }

  ngOnInit() {
  }

}
