import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class TaxStatusService extends AbstractService {
    endpoint = 'tax-status';
}