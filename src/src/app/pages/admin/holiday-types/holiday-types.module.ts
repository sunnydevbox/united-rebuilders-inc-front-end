import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HolidayTypesComponent } from './holiday-types.component';
import { HolidayTypeService } from '@services/holiday-type.service';
import { RoutingModule } from './routing.module';
import { FormModule } from './crud/form/form.module';
import { ListModule } from './crud/list/list.module';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    FormModule,
    ListModule,
  ],
  declarations: [
    HolidayTypesComponent,
  ],
  exports: [
    HolidayTypesComponent,
  ],
  providers: [
    HolidayTypeService,
  ]
})
export class HolidayTypesModule { }
