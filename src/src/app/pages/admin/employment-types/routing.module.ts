import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { Route } from './../../../core/route.service';

import { ListComponent } from './crud/list/list.component';
import { FormComponent } from './crud/form/form.component';
import { EmploymentTypesComponent } from './employment-types.component';

const routes: Routes = [
    {
        path: '',
        //component: ListComponent,
        data: { title: 'Employment Types'  },
        children:[
            {
              path: '',
              component: EmploymentTypesComponent,
            },
            {
              path: 'edit/:id',
              component: FormComponent,
              data: { title: 'Edit Employment Status'  },
            },
          ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}