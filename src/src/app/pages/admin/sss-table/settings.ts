import { SSSService } from '@services/sss.service';

export const settings = {
    plural: 'SSS Table',
    singular: 'SSS Table',
    service: SSSService,

    crud: {
      create: {
        allow: true, // true/false
        url: '/admin/settings/system/sss-table/create'
      },
      read: {

      },
      update: {
        allow: true,
        url: '/admin/settings/system/sss-table/edit/'
      },
      delete: {
        allow: true
      },
      list: {
        allow: true,
        url: '/admin/settings/system/sss-table'
      }
    }
  };
