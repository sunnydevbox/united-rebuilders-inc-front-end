
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
  'start': [null, Validators.compose([Validators.required])],
  'end': [null, Validators.compose([Validators.required])],
  'payroll_template_id': [null, Validators.compose([Validators.required])],
  'custom': [true],
  'include_sss': [null],
  'pay_date': [null, Validators.compose([Validators.required])],
  'period_count': [null, Validators.compose([])],
  'pay_coverage_start': [null, Validators.compose([Validators.required])],
  'pay_coverage_end': [null, Validators.compose([Validators.required])],
};
