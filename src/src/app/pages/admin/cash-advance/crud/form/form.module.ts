import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@shared/forms.module';

import { FormModule as BaseFormModule } from '@shared/base/form/form.module';

/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { OverlayModule } from '@shared/overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */

import { FormComponent } from './form.component';
import { MyDatePickerModule } from 'mydatepicker';
import { EmployeeService } from '@services/employee.service';
import { CashAdvanceLogService } from '@services/cash-advance-log.service';
import { StatusBadgeModule } from '@shared/status-badge/status-badge.module';
import { ModalService } from '@shared/modal/modal.service';
import { NotesFormModule } from './notes-form/notes-form.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BaseFormModule,
    OverlayModule,
    NgbModule,
    MyDatePickerModule,
    StatusBadgeModule,
    NotesFormModule,
  ],
  declarations: [
    FormComponent,
  ],
  exports: [
    FormComponent,
  ],
  providers: [
    EmployeeService,
    ModalService,
    CashAdvanceLogService,
  ]
})
export class FormModule { }
