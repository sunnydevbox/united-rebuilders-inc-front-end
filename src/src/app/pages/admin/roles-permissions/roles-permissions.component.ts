import { Component, OnInit, Injector } from '@angular/core';

import { settings } from './settings';
import { PermissionService } from '@services/permission.service';

@Component({
  selector: 'app-roles-permissions',
  templateUrl: './roles-permissions.component.html',
  styleUrls: ['./roles-permissions.component.scss']
})
export class RolesPermissionsComponent implements OnInit {

  public settings;
  private rolesService;
  private permissionService;
  public lists = [];
  public selectedRoledId = null;
  public roleAttachedPermissions = null;

  constructor(private injector: Injector) {
    this.settings = settings;
    this.permissionService = this.injector.get(PermissionService);
    this.rolesService = this.injector.get(this.settings.service);
    this.getRoles();
    this.getPermissions();
  }

  selectRole(e) {
    this.selectedRoledId = (e.target.value !== '--') ? e.target.value : null;
    this.roleAttachedPermissions = null;
    // Clear the perimission checkboxes;

    if (this.selectedRoledId) {
      // Load the permissions that are currently attached to a role
      this.rolesService.getPermissions(this.selectedRoledId)
      .subscribe(
        (res) => {
          this.roleAttachedPermissions = res.data;
        }
      );
    }
  }

  getRoles() {
    this.rolesService.index({
      filter: 'id;name',
      orderBy: 'name',
      sortedBy: 'asc',
      limit: 0,
    })
    .subscribe(
      (res) => {
        this.lists['roles'] = res.data;
      }
    );
  }

  getPermissions() {
    this.permissionService.index({
      filter: 'id;name',
      orderBy: 'name',
      sortedBy: 'asc',
      limit: 0,
    })
    .subscribe(
      (res) => {
        this.lists['permissions'] = res.data;

        const p = [];
        for (const key in res.data) {
          if (typeof res.data[key] !== 'undefined') {
            const split = res.data[key].name.split('.');

            const label = split.length > 1
              ? split[0].toUpperCase() + ' - ' + split[1].toUpperCase()
              : split[0];

            res.data[key].label = label;
            p.push(res.data[key]);
          }
        }
        this.lists['permissions'] = p; // res.data;
      }
    );
  }

  ngOnInit() {
  }

}
