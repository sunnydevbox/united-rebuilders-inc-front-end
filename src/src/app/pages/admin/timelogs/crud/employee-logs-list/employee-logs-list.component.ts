import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '@services/user.service';
import { EmployeeService } from '@services/employee.service';

@Component({
  selector: 'app-employee-logs-list',
  templateUrl: './employee-logs-list.component.html',
  styleUrls: ['./employee-logs-list.component.css']
})
export class EmployeeLogsListComponent implements OnInit {

  public Employee;

  constructor(
    public userService: UserService,
    private employeeService: EmployeeService,
    private activatedRoute: ActivatedRoute
  ) { 
    const employeeId = this.activatedRoute.snapshot.params.id;

    this.employeeService.show(employeeId).subscribe(
      (res) => {
        this.Employee = res.data;
      }
    );
  }

  ngOnInit() {
  }

}
