import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class TitleService extends AbstractService {
    endpoint = 'titles';
}
