import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';
import { MODEL } from './model';
import { settings } from './../../settings';

@Component({
  selector: 'app-custom-item-form',
  templateUrl: './custom-item-form.component.html',
  styleUrls: ['./custom-item-form.component.css']
})
export class CustomItemFormComponent extends BaseFormComponent implements OnInit {

  
  private activeModal;
  public socialSecurityTotal;
  public totalContributionTotal;
  
  @Input() item;
  @Output() closeForm = new EventEmitter();
  @Output() triggerReloadList = new EventEmitter();

  _postConstruct() {
    this.service = this.injector.get(settings.service);
    this.form = this.formBuilder.group(MODEL);
  }

  cancel() {
    this.closeForm.emit(true);
  }

  _onUpdateSuccess(res) {
    super._onUpdateSuccess(res);
    this.triggerReloadList.emit(true);
  }

  _onSaveSuccess(res) {
    super._onSaveSuccess(res);
    this.triggerReloadList.emit(true);
  }

  ngOnInit() {
    if (this.item) {
      this.objectId = this.item.id;
      this.formMode = 'update';

      this.form.get('salary_bracket').setValue(this.item.salary_bracket);
      this.form.get('salary_range_from').setValue(this.item.salary_range_from);
      this.form.get('salary_range_to').setValue(this.item.salary_range_to);
      this.form.get('salary_base').setValue(this.item.salary_base);
      this.form.get('monthly_premium').setValue(this.item.monthly_premium);
      this.form.get('employee_share').setValue(this.item.employee_share);
      this.form.get('employer_share').setValue(this.item.employer_share);
    }
  }  
}
