import { Component, OnInit, Output, EventEmitter, OnDestroy, Input } from '@angular/core';

import { FiltersComponent as BaseFiltersComponent } from '@shared/base/filters/filters.component';

import {Observable} from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { MODEL } from './model';
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import { datePickerOptions1 } from '@admin/config';
import { SupplementDataService } from '@services/supplement-data.service'; 

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css']
})
export class FiltersComponent extends BaseFiltersComponent implements OnDestroy {

  private formControleName: Subscription;
  public datePickerOptions1 = datePickerOptions1;
  private supplementDataService = null;
  public lists = {
    'payrollTemplates': []
  };

  // @Output() filtersChanged = new EventEmitter();
  // @Output() searchChangeEmitter = new EventEmitter();
  @Output() selectedPayrollTemplateId = new EventEmitter();
  @Input() suppData;

  _postConstruct() {
    this.model = MODEL;
    this.form = this.formBuilder.group(this.model);

    this.supplementDataService = this.injector.get(SupplementDataService);
    this.supplementDataService.index({data: 'payroll-templates'}).subscribe(
      (res) => {
        this.lists['payrollTemplates'] = res['payroll-templates'];
      },
      (err) => {
        console.log(err)
      }
    )
  }

  selectTemplate(t) {
    // if (t.target.value != 0) 
    {
      this.selectedPayrollTemplateId.emit(t.target.value);
    }
  }

  fieldProcessor(field) {
    super.fieldProcessor(field);

    if (field === 'start') {
      this.query.push('start|' + this.form.get(field).value);
    }
  }

  dateChanged(field, event: IMyDateModel) {
    if (field === 'start') {
      this.form.get(field).setValue(event.formatted);
      this.query.push('start|' + this.form.get(field).value);
    }

    this.startFilter();
  }

  ngOnDestroy() {}
}
