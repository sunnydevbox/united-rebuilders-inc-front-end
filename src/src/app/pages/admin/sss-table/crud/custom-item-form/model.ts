import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
  'id' : [null],

  'compensation_from': [null, Validators.compose([])],
  'compensation_to' : [null, Validators.compose([])],
  'monthly_salary_credit' : [null, Validators.compose([Validators.required])],
  'social_security_er' : [null, Validators.compose([Validators.required])],
  'social_security_ee' : [null, Validators.compose([Validators.required])],
  'social_security_total' : [null, Validators.compose([])],
  'ec_er' : [null, Validators.compose([])],
  'total_contribution_er' : [null, Validators.compose([])],
  'total_contribution_ee' : [null, Validators.compose([])],
  'total_contribution_er_ee' : [null, Validators.compose([])],
  'total_contribution_se_vm_ofw' : [null, Validators.compose([])],

 
}
