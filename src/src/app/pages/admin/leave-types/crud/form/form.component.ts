import { Component, Output, EventEmitter, Input, SimpleChanges, OnChanges } from '@angular/core';

import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';

import { MODEL } from './../../model';
import { settings } from './../../settings';

import { ERRORS, datePickerOptions1 } from '@admin/config';

import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import { SupplementDataService } from '@services/supplement-data.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent extends BaseFormComponent implements OnChanges {
  @Output() triggerReloadList = new EventEmitter();
  @Output() objectChanged = new EventEmitter();
  @Input() object;

  public datePickerOptions = datePickerOptions1;
  private supplementDataService;
  public lists = {
    'shift-templates': []
  };

  _postConstruct() {
    this.service = this.injector.get(settings.service);
    this.form = this.formBuilder.group(MODEL);
    this.supplementDataService = this.injector.get(SupplementDataService);

    this.supplementDataService.index({data: 'shift-templates'}).subscribe(
      (res) => {
        this.lists['shift-templates'] = res['shift-templates'];
      }
    );
  }

  _onSaveSuccess(res) {
    super._onSaveSuccess(res);

    // Trigger Relaod list
    this.triggerReloadList.emit(true);

    // Reset the form
    this.form.reset();
  }

  _onUpdateSuccess(res) {
    super._onUpdateSuccess(res);
    // Trigger Relaod list
    this.triggerReloadList.emit(true);

    // Reset the form
    this.objectId = null;
    this.form.reset();

    // REset back to create mode
    this.formMode = 'create';

    this.objectChanged.emit(null);
  }


  cancel() {
    this.form.reset();
    this.objectChanged.emit(null);
  }

  _preSubmit() {

    this.form.get('time_in').setValue(
      this.form.get('time_in_date').value.date.year + '-' +
      this.form.get('time_in_date').value.date.month + '-' +
      this.form.get('time_in_date').value.date.day + ' ' +
      this.form.get('time_in_time').value
    );


    this.form.get('time_out').setValue(
      this.form.get('time_out_date').value.date.year + '-' +
      this.form.get('time_out_date').value.date.month + '-' +
      this.form.get('time_out_date').value.date.day + ' ' +
      this.form.get('time_out_time').value
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    // console.log(' BASELIST ', changes);
    if (changes.object && changes.object.currentValue) {
      this.formMode = 'update';
      this.objectId = changes.object.currentValue.id;

      this.form.get('shift_template_id').setValue(changes.object.currentValue.shift_template.id);
      if (changes.object.currentValue.shift_template_period) {
        this.form.get('shift_template_period_id').setValue(changes.object.currentValue.shift_template_period.id);
      }

      let time_in_date;
      let time_in_time;
      if (changes.object.currentValue.time_in) {
        const bd = new Date(changes.object.currentValue.time_in.date);
        time_in_date = {
          date: {
            year: bd.getFullYear(),
            month: bd.getMonth() + 1,
            day:  bd.getDate()
          }
        };
        time_in_time = ('0' + bd.getHours()).slice(-2) + ':' + ('0' + bd.getMinutes()).slice(-2);

        this.form.get('time_in_date').setValue(time_in_date);
        this.form.get('time_in_time').setValue(time_in_time);
      }

      let time_out_date;
      let time_out_time;
      if (changes.object.currentValue.time_out) {
        const bd = new Date(changes.object.currentValue.time_out.date);
        time_out_date = {
          date: {
            year: bd.getFullYear(),
            month: bd.getMonth() + 1,
            day:  bd.getDate()
          }
        };
        time_out_time = ('0' + bd.getHours()).slice(-2) + ':' + ('0' + bd.getMinutes()).slice(-2);

        this.form.get('time_out_date').setValue(time_out_date);
        this.form.get('time_out_time').setValue(time_out_time);
      }
    }
  }
}
