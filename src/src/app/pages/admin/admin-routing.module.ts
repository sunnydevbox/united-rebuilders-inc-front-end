import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { environment } from './../../../environments/environment';
import { Route } from './../../core/theme/admin/admin-routing.service';
import { CheckPermissionGuard } from './../../core/guards/check-permissions.guard';
import { NoPermissionViewComponent } from './../no-permission-view/no-permission-view.component';

const routes: Routes = Route.withShell([
  {
    path: environment.admin.REDIRECT_URL,
    children: [
      {
        path: '',
        loadChildren: './home/home.module#HomeModule',
        data: { title: 'Admin'  },
      },

      {
        path: 'no-permission',
        component: NoPermissionViewComponent,
        data: { title: 'Admin'  },
      },

      {
        path: 'employees',
        loadChildren: './employees/employees.module#EmployeesModule',
        data: { title: 'Admin'  },
      },
      {
        path: 'reports',
        children: [
          {
            path: 'payroll',
            loadChildren: './../reports/payroll/payroll.module#PayrollModule',
          }
        ]
      },
      {
        path: 'settings',
        children: [
          {
            path: 'system',
            children: [
              {
                path: 'benefits',
                loadChildren: './benefits/benefits.module#BenefitsModule',
                data: { title: 'Admin :: Civil Status'  },
              },
              {
                path: 'cash-advances',
                loadChildren: './cash-advance/cash-advance.module#CashAdvanceModule',
                data: { title: 'Admin :: Civil Status'  },
              },
              {
                path: 'civil-status',
                loadChildren: './civil-status/civil-status.module#CivilStatusModule',
                data: { title: 'Admin :: Civil Status'  },
              },
              {
                path: 'company-departments',
                loadChildren: './company-departments/company-departments.module#CompanyDepartmentsModule',
                data: { title: 'Admin :: Company Departments'  },
              },
              {
                path: 'deductions',
                loadChildren: './deductions/deductions.module#DeductionsModule',
                data: { title: 'Admin :: Deductions'  },
              },
              {
                path: 'employment-status',
                loadChildren: './employment-status/employment-status.module#EmploymentStatusModule',
                data: { title: 'Admin :: Employment Status'  },
              },
              {
                path: 'employee-templates',
                loadChildren: './employee-template/employee-template.module#EmployeeTemplateModule',
                data: { title: 'Admin :: Employment Types'  },
              },
              {
                path: 'employment-types',
                loadChildren: './employment-types/employment-types.module#EmploymentTypesModule',
                data: { title: 'Admin :: Employment Types'  },
              },
              {
                path: 'holidays',
                loadChildren: './holidays/holidays.module#HolidaysModule',
                data: { title: 'Admin :: Holidays'  },
              },
              {
                path: 'holiday-types',
                loadChildren: './holiday-types/holiday-types.module#HolidayTypesModule',
                data: { title: 'Admin :: Holidays'  },
              },
              {
                path: 'job-positions',
                loadChildren: './job-positions/job-positions.module#JobPositionsModule',
                data: { title: 'Admin :: Job Positions'  },
              },
              {
                path: 'leave-applications',
                loadChildren: './leave-applications/leave-applications.module#LeaveApplicationsModule',
                data: { title: 'Admin :: Job Positions'  },
              },
              {
                path: 'leave-types',
                loadChildren: './leave-types/leave-types.module#LeaveTypesModule',
                data: { title: 'Admin :: Job Positions'  },
              },
              {
                path: 'payrolls',
                loadChildren: './payroll/payroll.module#PayrollModule',
                data: { title: 'Admin :: Payroll'  },
              },
              {
                path: 'payroll-generator',
                loadChildren: './payroll-generator/payroll-generator.module#PayrollGeneratorModule',
                data: { title: 'Admin :: Job Positions'  },
              },
              {
                path: 'payroll-templates',
                loadChildren: './payroll-templates/payroll-templates.module#PayrollTemplatesModule',
                data: { title: 'Admin :: Job Positions'  },
              },
              {
                path: 'philhealth-table',
                loadChildren: './philhealth-table/philhealth-table.module#PhilhealthTableModule',
                data: { title: 'Admin :: Job Positions'  },
              },
              {
                path: 'shift-templates',
                loadChildren: './shift-templates/shift-templates.module#ShiftTemplatesModule',
                data: { title: 'Admin :: Shift Templates'  },
              },
              {
                path: 'shift-template-periods',
                loadChildren: './shift-template-periods/shift-template-periods.module#ShiftTemplatePeriodsModule',
                data: { title: 'Admin :: Shift Template Periods'  },
              },
              {
                path: 'sss-table',
                loadChildren: './sss-table/sss-table.module#SssTableModule',
                data: { title: 'Admin :: Timelogs'  },
              },
              {
                path: 'tax-status',
                loadChildren: './tax-status/tax-status.module#TaxStatusModule',
                data: { title: 'Admin :: Tax Status'  },
              },
              {
                path: 'tables/tax',
                loadChildren: './tables/tax/tax.module#TaxModule',
                data: { title: 'Admin :: Tax Table'  },
              },
              {
                path: 'timelogs',
                loadChildren: './timelogs/timelogs.module#TimelogsModule',
                data: { title: 'Admin :: Timelogs'  },
              },
              {
                path: 'titles',
                loadChildren: './titles/titles.module#TitlesModule',
                data: { title: 'Admin :: Titles'  },
              },
            ],
          },
          {
            path: 'security',
            children: [
              {
                path: 'users',
                loadChildren: './users/users.module#UsersModule',
                data: { title: 'Admin :: Users'  },
              },
              {
                path: 'roles',
                loadChildren: './roles/roles.module#RolesModule',
                data: { title: 'Admin :: Roles'  },
              },
              {
                path: 'roles-permissions',
                loadChildren: './roles-permissions/roles-permissions.module#RolesPermissionsModule',
                data: { title: 'Admin :: Roles'  },
              },
            ]
          },
        ],
      },
    ],
    canActivate: [
      CheckPermissionGuard,
    ],
    data: {
      title: 'Admin',
      permissions: [
        'can_access_admin'
      ],
      roles: [
        'admin'
      ]
    }
  },

]);

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminRoutingModule {
}
