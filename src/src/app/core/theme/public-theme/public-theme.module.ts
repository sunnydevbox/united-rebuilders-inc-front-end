import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { PublicThemeComponent } from './public-theme.component';

import { UserService } from '@services/user.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    PublicThemeComponent,
  ],
  providers: [
    UserService,
  ]

})
export class PublicThemeModule { }
