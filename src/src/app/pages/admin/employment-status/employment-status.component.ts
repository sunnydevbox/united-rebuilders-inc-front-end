import { Component, OnInit } from '@angular/core';

import { settings } from './settings';

@Component({
  selector: 'app-employment-status',
  templateUrl: './employment-status.component.html',
  styleUrls: ['./employment-status.component.css']
})
export class EmploymentStatusComponent implements OnInit {

  public settings; 
  public _triggerReloadList;
  public _object;

  constructor() { 
    this.settings = settings;
  }

  triggerReloadList(e) {
    if (e) {
      this._triggerReloadList = e;
    }
  }

  getObject(object) {
    this._object = object;
  }

  ngOnInit() {
  }

}
