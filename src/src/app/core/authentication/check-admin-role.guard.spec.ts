import { TestBed, async, inject } from '@angular/core/testing';

import { CheckAdminRoleGuard } from './check-admin-role.guard';

describe('CheckAdminRoleGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CheckAdminRoleGuard]
    });
  });

  it('should ...', inject([CheckAdminRoleGuard], (guard: CheckAdminRoleGuard) => {
    expect(guard).toBeTruthy();
  }));
});
