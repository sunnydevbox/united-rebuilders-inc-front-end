import { Component, OnInit } from '@angular/core';

import { settings } from './settings';
import { SupplementDataService } from '@services/supplement-data.service';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  public settings;

  constructor() {
    this.settings = settings;

   }

  ngOnInit() {
  }

}
