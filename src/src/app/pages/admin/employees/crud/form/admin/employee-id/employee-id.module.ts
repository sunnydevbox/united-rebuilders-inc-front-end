import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@shared/forms.module';

/*****************************************/
/**** IMPORT THE BASE MODULE for FORM ****/
/*****************************************/
/***/
/***/ import { FormModule as BaseFormModule } from '@shared/base/form/form.module';
/***/
/** INSTRUCTED FROM BASE FORM MODULE */
/***/
/***/ import { OverlayModule } from '@shared/overlay/overlay.module';
/***/


import { EmployeeIdComponent } from './employee-id.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BaseFormModule,
    OverlayModule,
  ],
  declarations: [
    EmployeeIdComponent,
  ],
  exports: [
    EmployeeIdComponent,
  ]
})
export class EmployeeIdModule { }
