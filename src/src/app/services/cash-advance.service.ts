import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class CashAdvanceService extends AbstractService {
    endpoint = 'cash-advances';


    /**
     * {
     *  id: ###,
     *  status: "####"
     * }
     * @param params 
     */
    setStatus(params) {
        return this.http.post(this.apiURL() + '/set-status/', params)
        .map((result: any) => {
            return result;
        })
        .catch(this.handleError);
    }
}
