
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
    'name' : [null, Validators.compose([Validators.required, Validators.minLength(2)])],
    'description': [null],
    'start_date': [null, Validators.compose([Validators.required])],
    'holiday_type_id': [null, Validators.compose([Validators.required])],
};
