import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import { HttpHeaders } from '@angular/common/http';

export const ERRORS = [
    { name: 'required', text: 'This field is required', rules: ['touched'] },
    { name: 'minlength', text: 'Minimum number of characters ', rules: ['touched'] },
    { name: 'maxlength', text: 'Maximum number of characters ', rules: ['touched'] }
];


export const responseErrors = {
    'duplicate_employee_id': 'That ID is already used. Please select another',
    'invalid_format_unmatched_columns': 'Invalid format',
    'timelog_delete': 'Timelog successfully deleted',
};

export const responsMessage = responseErrors;

const httpOptions = {
    
  };
export const fileUploaderHeaders = {
    headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'enctype': 'multipart/form-data'
      })
}


const dateNow = new Date();
export const datePickerOptions1: IMyDpOptions = {
    dateFormat: 'mmm dd, yyyy',
    editableDateField: false,
    openSelectorOnInputClick: true,
    sunHighlight: true,
    satHighlight: true,
    minYear: dateNow.getFullYear() - 90,
    maxYear: dateNow.getFullYear() + 2,
    height: '25px',
    selectionTxtFontSize: '11px',
    showTodayBtn: false,
    // showClearDateBtn: false,
};

export function minutesToHoursMinutes(minutes) {
    if (minutes < 1) {
        return;
    }
    const $hours = Math.floor(minutes / 60);
    const $minutes = (minutes % 60);
    return $hours + 'h' + $minutes + 'm';
}


export const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

export function range(start, end, step = 1) { 
    
    let data = [];

    if (start < end) {
        for(let i = start; i <= end; step) {
            data.push(i);
            i += step;
        }
    } else {
        for(let i = start; i >= end; step) {
            data.push(i);
            i -= step;
        }
    }

    return data;
}


export function formatDateFix(d: any) {
    return d.toString().replace(/\s/g, "T");
}

/**
 * @param v format: "date" property must exist
 */
export function myDatePickerFormatter(v: any, defaultValue:any = null) {
    // console.log('v', typeof v.date);

    if (!v || typeof v == 'undefined') {
        return defaultValue;
    } else  if (v && typeof v.date != 'undefined') {
        let d:any = defaultValue;

        if (typeof v.date == 'string') {
            d = new Date(formatDateFix(v.date));
        }

        return {
            date: {
                year: d.getFullYear(),
                month: d.getMonth() + 1,
                day:  d.getDate(),
            }
        }
    }
}