
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
  'name' : [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(20)])],
  'description': [null],
  'shift_template_id': [null, Validators.compose([Validators.required])],
  'start': [null, Validators.compose([Validators.required])],
  'end': [null, Validators.compose([Validators.required])],
  // 'start_hour': [null, Validators.compose([Validators.required])],
  // 'start_minute': [null, Validators.compose([Validators.required])],
  // 'end_hour': [null, Validators.compose()],
  // 'end_minute': [null, Validators.compose()],
  'grace_period': [null],
  'day_m': [null],
  'day_t': [null],
  'day_w': [null],
  'day_th': [null],
  'day_f': [null],
  'day_s': [null],
  'day_sun': [null],
};
