import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CompanyDepartmentsComponent } from './company-departments.component';
import { RoutingModule } from './routing.module';

import { ListModule } from './crud/list/list.module';
import { FormModule } from './crud/form/form.module';
import { settings } from './settings';


@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    FormModule,
    ListModule
  ],
  declarations: [
    CompanyDepartmentsComponent
  ],
  exports: [
    CompanyDepartmentsComponent,
  ],
  providers: [
    settings.service
  ],
})
export class CompanyDepartmentsModule { }
