import { Component, OnInit } from '@angular/core';
import { settings } from './settings';

@Component({
  selector: 'app-tax',
  templateUrl: './tax.component.html',
  styleUrls: ['./tax.component.css']
})
export class TaxComponent implements OnInit {

  public settings;
  
  constructor() { 
    this.settings = settings;
  }

  ngOnInit() {
  }

}
