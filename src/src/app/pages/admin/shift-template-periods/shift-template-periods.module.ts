import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShiftTemplatePeriodsComponent } from './shift-template-periods.component';
import { ShiftTemplatePeriodService  } from '@services/shift-template-period.service';
import { SupplementDataService  } from '@services/supplement-data.service';
import { RoutingModule } from './routing.module';
import { ListModule } from './crud/list/list.module';
import { FormModule } from './crud/form/form.module';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    ListModule,
    FormModule,
  ],
  declarations: [
    ShiftTemplatePeriodsComponent,
  ],
  exports: [
    ShiftTemplatePeriodsComponent,
  ],
  providers: [
    ShiftTemplatePeriodService,
    SupplementDataService,
  ],
})
export class ShiftTemplatePeriodsModule { }
