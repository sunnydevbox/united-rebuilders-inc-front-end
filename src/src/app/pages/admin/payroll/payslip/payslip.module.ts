import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PayslipComponent } from './payslip.component';
import { RoutingModule } from './routing.module';
import { FormsModule } from '@shared/forms.module';

import { FormModule as BaseFormModule } from '@shared/base/form/form.module';

/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { OverlayModule } from '@shared/overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */

import { CustomItemFormModule } from './custom-item-form/custom-item-form.module';
import { PayrollItemService } from '@services/payroll-item.service';
import { ModalService } from '@shared/modal/modal.service';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    FormsModule,
    BaseFormModule,
    CustomItemFormModule,
    // OverlayModule,
    NgbModule,
  ],
  declarations: [
    PayslipComponent,
  ],
  exports: [
    PayslipComponent,
  ],
  providers: [
    PayrollItemService,
    ModalService,
  ]
})
export class PayslipModule { }
