import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class PayrollTemplateService extends AbstractService {
    endpoint = 'payroll-templates';

    /**
     * data = {
     *  periodTemplateID: 1,    // required, numeric
     *  month: 1,               // required, numeric
     *  year: 2018              // required, numeric
     * }
     */
    getSelectablePeriods(data) {
        return this.http.post(this.apiURL() + 'period-selections', data, {
            reportProgress: true,
            responseType: 'json'
          })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
    }
}
