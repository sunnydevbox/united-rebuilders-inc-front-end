import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
  'id' : [null],
  'compensation_level_period': [null, Validators.compose([Validators.required])],
  'salary_range_from' : [null, Validators.compose([])],
  'salary_range_to' : [null, Validators.compose([])],
  'withholding_tax' : [null, Validators.compose([Validators.required])],
  'withholding_percentage' : [null, Validators.compose([Validators.required, Validators.min(0), Validators.max(100)])],
}
