
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
    'name': [null],
    'company_department_id': [0],
    'job_position_id': [0],
    'status': [0],
    'employment_status_id': [0],
    'employment_type_id': [0],
};
