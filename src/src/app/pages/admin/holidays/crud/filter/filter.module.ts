import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterComponent } from './filter.component';
import { FormsModule } from '@shared/forms.module';
import { FormModule as BaseFormModule } from '@shared/base/form/form.module';
/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { OverlayModule } from '@shared/overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */

 import { settings } from './../../settings';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BaseFormModule,
  ],
  declarations: [
    FilterComponent
  ],

  exports: [
    FilterComponent,
  ],
  providers: [
    settings.service,
  ]
})
export class FilterModule { }
