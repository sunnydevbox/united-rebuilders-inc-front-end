import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomPayrollPeriodFormComponent } from './custom-payroll-period-form.component';

describe('CustomPayrollPeriodFormComponent', () => {
  let component: CustomPayrollPeriodFormComponent;
  let fixture: ComponentFixture<CustomPayrollPeriodFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomPayrollPeriodFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomPayrollPeriodFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
