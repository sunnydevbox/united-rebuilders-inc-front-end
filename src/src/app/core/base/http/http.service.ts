import { Injectable } from '@angular/core';
import {
  Http, ConnectionBackend, RequestOptions,
  Headers,
  Request, Response, RequestOptionsArgs, RequestMethod, ResponseOptions
} from '@angular/http';


import { HttpClient, HttpHandler } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { Subscriber } from 'rxjs/Subscriber';
import { _throw } from 'rxjs/observable/throw';
import { catchError } from 'rxjs/operators';
import { extend } from 'lodash';

import { environment } from './../../../../environments/environment';
import { Logger } from './../../logger.service';
// import { HttpCacheService } from './http-cache.service';
// import { HttpCachePolicy } from './request-options-args';

import { Router } from '@angular/router';

const log = new Logger('HttpService');

/**
 * Provides a base framework for http service extension.
 * The default extension adds support for API prefixing, request caching and default error handler.
 */
@Injectable()
export class HttpService extends Http {

  // constructor(httpHandler: HttpHandler) {
  //   // Customize default options here if needed
  //   super(httpHandler);
  // }


  /**
   * Performs any type of http request.
   * You can customize this method with your own extended behavior.
   */
  request(request: string|Request, options?: RequestOptionsArgs): Observable<Response> {
    const requestOptions = options || {};
    let url: string;

    if (typeof request === 'string') {
      url = request;
      request = environment.API_BASE_URL + url;
    } else {
      url = request.url;
      request.url = environment.API_BASE_URL + url;
    }
    
    return this.httpRequest(request, requestOptions);
  }

  get(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.request(url, extend({}, options, { method: RequestMethod.Get }));
  }

  post(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.request(url, extend({}, options, {
      body: body,
      method: RequestMethod.Post
    }));
  }

  put(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.request(url, extend({}, options, {
      body: body,
      method: RequestMethod.Put
    }));
  }

  delete(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.request(url, extend({}, options, { method: RequestMethod.Delete }));
  }

  patch(url: string, body: any, options?: RequestOptionsArgs): Observable<Response> {
    return this.request(url, extend({}, options, {
      body: body,
      method: RequestMethod.Patch
    }));
  }

  head(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.request(url, extend({}, options, { method: RequestMethod.Head }));
  }

  options(url: string, options?: RequestOptionsArgs): Observable<Response> {
    return this.request(url, extend({}, options, { method: RequestMethod.Options }));
  }

  // Customize the default behavior for all http requests here if needed
  private httpRequest(request: string|Request, options: RequestOptionsArgs): Observable<Response> {
    let req = super.request(request, options);
    if (!options.skipErrorHandler) {
      req = req.pipe(catchError((error: any) => this.errorHandler(error)));
    }
    return req;
  }

  // Customize the default error handler here if needed
  private errorHandler(response: Response): Observable<Response> {

    if (environment.production) {
      // Avoid unchaught exceptions on production
      log.error('Request error', response);
      return _throw(response);
    }

    if (response.status == 401) {
      // redirect to login
    }
    throw response;
  }

}
