import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class ShiftTemplateService extends AbstractService {
    endpoint = 'shift-templates';
}
