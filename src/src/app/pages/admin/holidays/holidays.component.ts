import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { settings } from './settings';

@Component({
  selector: 'app-holidays',
  templateUrl: './holidays.component.html',
  styleUrls: ['./holidays.component.css']
})
export class HolidaysComponent implements OnInit {

  @Output() toggleReloadCalendarChange = false;

  public settings;
  public selectedYear;
  public selectedObject;

  constructor() {
    this.settings = settings;
    let now = new Date();
    this.selectedYear = now.getFullYear();
   }

  ngOnInit() {
  }

  changedYear(year) {
    this.selectedYear = year;
  }

  toggleReloadCalendar() {
    this.toggleReloadCalendarChange = true;
  }
  
  _objectChanged(object) {
    this.selectedObject = object
  }
}
