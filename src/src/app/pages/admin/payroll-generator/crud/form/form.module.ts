import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@shared/forms.module';

import { FormModule as BaseFormModule } from '@shared/base/form/form.module';

/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { OverlayModule } from '@shared/overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */

import { FormComponent } from './form.component';
import { MyDatePickerModule } from 'mydatepicker';
import { SupplementDataService } from '@services/supplement-data.service';
import { settings } from './settings';

import { CustomPayrollPeriodFormModule } from './../custom-payroll-period-form/custom-payroll-period-form.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BaseFormModule,
    OverlayModule,
    NgbModule,
    MyDatePickerModule,
    CustomPayrollPeriodFormModule,
  ],
  declarations: [
    FormComponent,
  ],
  exports: [
    FormComponent,
  ],
  providers: [
    SupplementDataService,
    settings.service,
  ]
})
export class FormModule { }
