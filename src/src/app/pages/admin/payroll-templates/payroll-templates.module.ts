import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutingModule } from './routing.module';
import { PayrollTemplatesComponent } from './payroll-templates.component';
import { settings } from './settings';
import { ListModule } from './crud/list/list.module';
import { FormModule } from './crud/form/form.module';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    ListModule,
    FormModule,
  ],
  declarations: [
    PayrollTemplatesComponent
  ],
  exports: [
    PayrollTemplatesComponent,
  ],
  providers: [
    settings.service,
  ]
})
export class PayrollTemplatesModule { }
