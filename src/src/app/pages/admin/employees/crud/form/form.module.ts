import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoutingModule } from './routing.module';
import { FormComponent } from './form.component';

import { settings } from './../../settings';

/*****************************************/
/**** IMPORT THE BASE MODULE for FORM ****/
/*****************************************/
/***/
/***/ import { FormModule as BaseFormModule } from '@shared/base/form/form.module';
/***/
/** INSTRUCTED FROM BASE FORM MODULE */
/***/
/***/ import { OverlayModule } from '@shared/overlay/overlay.module';
/***/


import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AdminModule } from './admin/admin.module';
import { DetailsModule } from './details/details.module';
import { ProfilePictureModule } from './profile-picture/profile-picture.module';
import { EmploymentModule } from './employment/employment.module';
import { RemittanceModule } from './remittance/remittance.module';
import { LogsModule } from './logs/logs.module';
import { TimelogsModule } from './timelogs/timelogs.module';
import { PayslipsModule } from './payslips/payslips.module';

import { SupplementDataService  } from '@services/supplement-data.service';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,

    BaseFormModule,
    ProfilePictureModule,
    AdminModule,
    DetailsModule,
    EmploymentModule,
    RemittanceModule,
    LogsModule,
    TimelogsModule,
    PayslipsModule,

    OverlayModule,
    NgbModule,
  ],
  declarations: [
    FormComponent,
  ],
  providers: [
    settings.service,
    SupplementDataService
  ]
})
export class FormModule { }
