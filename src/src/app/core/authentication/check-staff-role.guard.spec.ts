import { TestBed, async, inject } from '@angular/core/testing';

import { CheckStaffRoleGuard } from './check-staff-role.guard';

describe('CheckStaffRoleGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CheckStaffRoleGuard]
    });
  });

  it('should ...', inject([CheckStaffRoleGuard], (guard: CheckStaffRoleGuard) => {
    expect(guard).toBeTruthy();
  }));
});
