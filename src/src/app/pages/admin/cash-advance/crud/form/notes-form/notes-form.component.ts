import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';
import { MODEL } from './model';

@Component({
  selector: 'app-notes-form',
  templateUrl: './notes-form.component.html',
  styleUrls: ['./notes-form.component.css']
})
export class NotesFormComponent extends BaseFormComponent {

  public confirmation = false;
  
  @Output() notesChanged = new EventEmitter();
  @Output() cancelModalChanged = new EventEmitter();

  _postConstruct() {
    this.form = this.formBuilder.group(MODEL);
  }

  confirmed() {
    this.notesChanged.emit(this.form.get('notes').value);
  }

  cancelled() {
    this.confirmation = false;
    this.cancelModalChanged.emit(true);
  }

  cancelModal() {
    this.confirmation = false;
    this.cancelModalChanged.emit(true);
  }

  submit() {
    this.confirmation = true;
  }
}


