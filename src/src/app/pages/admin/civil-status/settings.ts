import { CivilStatusService } from '@services/civil-status.service';

export const settings = {
    plural: 'Civil Status',
    singular: 'Civil Status',
    service: CivilStatusService,    
  
    crud: {
      create: {
        allow: true, // true/false
        url: '/app/system/civil-status'
      },
      read:{
  
      },
      update:{
        allow: true,
        url: '/app/system/civil-status/edit/'
      },
      delete:{
        allow: true
      },
      list: {
        allow: true,
        url: '/app/system/civil-status'
      }
    }
  };