import { Component, OnInit, ElementRef, Renderer2 } from '@angular/core';
import { SidebarComponent } from './sidebar/sidebar.component';
import { Router } from '@angular/router';
import { AuthenticationService } from './../../authentication/authentication.service';

import { environment } from './../../../../environments/environment';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private elRef: ElementRef,
    private renderer: Renderer2
  ) {
    // console.log(elRef,
    //   elRef.nativeElement,
    //   elRef.nativeElement.ownerDocument,
    //   elRef.nativeElement.ownerDocument.body,
    //   elRef.nativeElement.ownerDocument.body.style
    // );
    // elRef.nativeElement.ownerDocument.body.style.paddingTop = '54px';
    // elRef.nativeElement.ownerDocument.body.style.overflow = 'hidden';
  }

  ngOnInit() {
    this.renderer.addClass(this.elRef.nativeElement, 'wild');
  }

  logout() {
    this.authService.logout()
    .subscribe(() => {
      // this.router.navigate(['/login'], { replaceUrl: true })
      window.location.href = environment.BASE_URL + 'login';
     });
  }


}
