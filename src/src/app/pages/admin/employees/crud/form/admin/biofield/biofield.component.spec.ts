import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BiofieldComponent } from './biofield.component';

describe('BiofieldComponent', () => {
  let component: BiofieldComponent;
  let fixture: ComponentFixture<BiofieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BiofieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BiofieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
