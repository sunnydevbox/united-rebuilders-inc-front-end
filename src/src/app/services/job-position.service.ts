import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class JobPositionService extends AbstractService {
    endpoint = 'job-positions';
}
