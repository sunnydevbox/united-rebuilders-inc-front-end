import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PayrollComponent } from './payroll.component';
import { RoutingModule } from './routing.module';
import { settings } from './settings';
import { LandingModule } from './landing/landing.module';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    LandingModule,
  ],
  declarations: [
    PayrollComponent
  ],
  exports: [
    PayrollComponent,
  ],
  providers: [
    settings.service,
  ]
})
export class PayrollModule { }
