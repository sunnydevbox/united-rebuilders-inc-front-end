import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PopupsearchComponent } from './popupsearch.component';
import { PopupsearchService } from './popupsearch.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    PopupsearchComponent,
  ],
  exports: [
    PopupsearchComponent,
  ],
  providers: [
    PopupsearchService,
  ],
  entryComponents: [
    PopupsearchComponent
  ]
})
export class PopupsearchModule { }
