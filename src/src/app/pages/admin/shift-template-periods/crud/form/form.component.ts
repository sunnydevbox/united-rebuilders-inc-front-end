import { Component, Output, EventEmitter, Input, SimpleChanges, OnChanges } from '@angular/core';

import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';

import { MODEL } from './../../model';
import { settings } from './../../settings';
import { ERRORS } from '@admin/config';

import { SupplementDataService } from '@services/supplement-data.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent extends BaseFormComponent implements OnChanges {

  private suppDataService: SupplementDataService;
  public shiftTemplates = [];
  public days = {
    day_m: false,
    day_t: false,
    day_w: false,
    day_th: false,
    day_f: false,
    day_s: false,
    day_sun: false,
  };


  @Output() triggerReloadList = new EventEmitter();
  @Input() object;

  _postConstruct() {
    this.service = this.injector.get(settings.service);
    this.suppDataService = this.injector.get(SupplementDataService);
    this.form = this.formBuilder.group(MODEL);
    this.errors = ERRORS;


    this.suppDataService.index({
      data: 'shift-templates'
    }).subscribe(
      (res) => {
        this.shiftTemplates = res['shift-templates'];
      }
    );
  }

  _onSaveSuccess(res) {
    super._onSaveSuccess(res);

    // Trigger Relaod list
    this.triggerReloadList.emit(true);

    // Reset the form
    this.form.reset();
  }

  _onUpdateSuccess(res) {
    super._onUpdateSuccess(res);
    // Trigger Relaod list
    this.triggerReloadList.emit(true);

    // Reset the form
    this.reset();
  }

  reset() {
    this.objectId = null;
    this.form.reset();
    this.setSelectedDays(false);
    this.formMode = 'create';
  }

  cancel() {
    this.triggerReloadList.emit(true);
    this.reset();
  }

  setSelectedDays(item) {

    // RESETS THE checkboxes to unchecked
    if (typeof item === 'boolean') {
      this.days.day_m = false;
      this.days.day_t = false;
      this.days.day_w = false;
      this.days.day_th = false;
      this.days.day_f = false;
      this.days.day_s = false;
      this.days.day_sun = false;
    } else {

      if (typeof item.day_m !== 'undefined') {
        this.days.day_m = item.day_m;
      }

      if (typeof item.day_t !== 'undefined') {
        this.days.day_t = item.day_t;
      }

      if (typeof item.day_w !== 'undefined') {
        this.days.day_w = item.day_w;
      }

      if (typeof item.day_th !== 'undefined') {
        this.days.day_th = item.day_th;
      }

      if (typeof item.day_f !== 'undefined') {
        this.days.day_f = item.day_f;
      }

      if (typeof item.day_s !== 'undefined') {
        this.days.day_s = item.day_s;
      }

      if (typeof item.day_sun !== 'undefined') {
        this.days.day_sun = item.day_sun;
      }
    }

    this.form.get('day_m').setValue(this.days.day_m);
    this.form.get('day_t').setValue(this.days.day_t);
    this.form.get('day_w').setValue(this.days.day_w);
    this.form.get('day_th').setValue(this.days.day_th);
    this.form.get('day_f').setValue(this.days.day_f);
    this.form.get('day_s').setValue(this.days.day_s);
    this.form.get('day_sun').setValue(this.days.day_sun);
  }

  clickedCheckbox(e) {
    if (this.objectId) {
      const data = [];
      if (e.target.checked) {
        data[e.target.name] = true;
      } else {
        data[e.target.name] = false;
      }
      this.setSelectedDays(data);
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.object && changes.object.currentValue) {
      this.formMode = 'update';
      this.form.get('shift_template_id').setValue(changes.object.currentValue.shift_template_id);
      this.form.get('name').setValue(changes.object.currentValue.name);
      this.form.get('description').setValue(changes.object.currentValue.description);
      this.form.get('start').setValue(changes.object.currentValue.start);
      this.form.get('end').setValue(changes.object.currentValue.end);
      this.form.get('grace_period').setValue(changes.object.currentValue.grace_period);
      this.setSelectedDays(changes.object.currentValue);

      this.objectId = changes.object.currentValue.id;
    }
  }
}
