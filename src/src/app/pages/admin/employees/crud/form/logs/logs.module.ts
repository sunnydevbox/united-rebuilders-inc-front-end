import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LogsComponent } from './logs.component';
import { EmployeeLogsService } from '@services/employee-logs.service';

/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { DataTableModule } from 'angular5-data-table';
import { OverlayModule } from '@shared/overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */


import { ListModule as BaseListModule } from '@shared/base/list/list.module' ;


@NgModule({
  imports: [
    CommonModule,
    BaseListModule,
    DataTableModule,
    NgbModule,

  ],
  declarations: [
    LogsComponent
  ],
  exports: [
    LogsComponent,
  ],
  providers: [
    EmployeeLogsService
  ],
})
export class LogsModule { }
