import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

// IMPORT THE BASE COMPONENT
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';

import { ERRORS, responseErrors } from '@admin/config';
import { MODEL } from './model';

import { settings } from './../../../../settings';

@Component({
  selector: 'app-assign-deductions',
  templateUrl: './assign-deductions.component.html',
  styleUrls: ['./assign-deductions.component.css']
})
export class AssignDeductionsComponent extends BaseFormComponent {

  @Input() lists;
  @Input() object;
  @Output() objectChanged = new EventEmitter();

  private employeeService;

  _postConstruct() {
    this.formMode = 'update';
    this.service = this.injector.get(settings.service);
    this.errors = ERRORS;
    this.form = this.formBuilder.group(MODEL);

    this.listErrors = responseErrors;
  }

  assignSelection(items) {
    if (items.id) {
      items = [items];
    }

    items.forEach( (item, index) => {
      if (item.selected) {
        this.service.attachDeduction(this.object.id, item.id).subscribe(
          (res) => {
            this.toastr.success('Deduction added');
          },
          (err) => {
            this.toastr.danger('There was a problem assigning Deduction');
          }
        );
      } else {
        this.service.detachDeduction(this.object.id, item.id).subscribe(
          (res) => {
            this.toastr.success('Deduction removed');
          },
          (err) => {
            this.toastr.danger('There was a problem removing Deduction');
          }
        );
      }
    });
  }

  

  // ngOnChanges(changes: SimpleChanges) {
  //   if (changes.object && changes.object.currentValue) {

  //     this.objectId = changes.object.currentValue.id;
  //     this.service.index({
  //       search        : this.objectId,
  //       searchFields  : 'employee_id',
  //       with          : 'deduction',
  //       limit         : 0,
  //     }).subscribe(
  //       (res) => {
  //         this.employeeDeductions = res.data;
  //       }
  //     );
  //   }
  // }
}
