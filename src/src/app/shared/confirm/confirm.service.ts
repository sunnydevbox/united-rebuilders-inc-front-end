import { Injectable } from '@angular/core';

import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmComponent } from './confirm.component';

@Injectable()
export class ConfirmService {

  closeResponse: boolean;
  closeResult: string;
  isOpen: Boolean = false;

  constructor(
    private modalService: NgbModal
  ) {}


  open(modalSettings, successCallback = null, failureCallback = null) {
    const modalRef = this.modalService.open(ConfirmComponent);

    modalRef.componentInstance.title = modalSettings.title;
    modalRef.componentInstance.body  = modalSettings.body;

    if (typeof modalSettings.ctaPositive != 'undefined') {
      modalRef.componentInstance.ctaPositive = modalSettings.ctaPositive;
    }

    if (typeof modalSettings.ctaNegative != 'undefined') {
      modalRef.componentInstance.ctaNegative  = modalSettings.ctaNegative;
    }

    modalRef.result.then((result) => {
      this.closeResponse = result;
      this.closeResult = `Closed with: ${result}`;

      if (result) {
        if (successCallback) {
          successCallback(result)
        }
      } else {
        if (failureCallback) {
          failureCallback(result)
        }
      }
    }, (reason) => {
      this.closeResponse = null;
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });

    return modalRef;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
