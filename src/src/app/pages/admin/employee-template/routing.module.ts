import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { Route } from './../../../core/route.service';

import { EmployeeTemplateComponent } from './employee-template.component';
import { settings } from './settings';
import { CheckPermissionGuard } from './../../../core/guards/check-permissions.guard';

const routes: Routes = [
    {
        path: '',
        component: EmployeeTemplateComponent,
        data: { title: 'Roles'  },
        children: [
          {
            path: '',
            loadChildren: '@admin/' + settings.uriPrefix + '/crud/list/list.module#ListModule',
          },
          {
            path: 'create',
            loadChildren: '@admin/' + settings.uriPrefix + '/crud/form/form.module#FormModule',
            data: { 
              formMode: 'create', 
              title: 'Create Employee Template'  
            },
          },
          {
            path: 'edit/:id',
            loadChildren: '@admin/' + settings.uriPrefix + '/crud/form/form.module#FormModule',
            data: { 
              formMode: 'update',
              title: 'Update Employee Template'  
            },
          },
        ],
        canActivate: [
          CheckPermissionGuard,
        ],
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}
