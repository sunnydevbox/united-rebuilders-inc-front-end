import { Component, OnInit, Input, SimpleChanges } from '@angular/core';

import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';

// import { RoleService } from '@services/role.service';
import { UserService } from '@services/user.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent extends BaseFormComponent {

  @Input() lists;
  @Input() object;
  @Input() objectId;
  public userService;

  // objectChanged(object) {
  //   console.log('changed ', object);
  // }

  _postConstruct() {
    this.userService = this.injector.get(UserService);
  }
}
