import { Component, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';
import { FormComponent } from '@shared/base/form/form.component';
import { TimelogService } from '@services/timelog.service';
import { MODEL } from './model';
import { BehaviorSubject, Observable } from 'rxjs';
import { datePickerOptions1 } from '@admin/config';
import { formatDateFix } from '@admin/config';

@Component({
  selector: 'app-update-form',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.css']
})
export class UpdateFormComponent extends FormComponent implements OnChanges {

  private timelogService: TimelogService;
  private isRecalculating: Boolean = false;
  public datePickerOptions = datePickerOptions1;

  @Input() object;
  @Input() shiftTemplatePeriodList;
  @Input() employee;
  @Output() objectUpdated = new EventEmitter();


  _postConstruct() {
    this.service = this.injector.get(TimelogService);
    this.formMode = 'update';

    this.form = this.formBuilder.group(MODEL);
  }

  _preSubmit() {
    // BUILD THE DATE/TIME 
    this.form.get('time_in').setValue(
      this.form.get('time_in_date').value.date.year + '-' +
      this.form.get('time_in_date').value.date.month + '-' +
      this.form.get('time_in_date').value.date.day + ' ' +
      this.form.get('time_in_time').value
    );

    this.form.get('time_out').setValue(
      this.form.get('time_out_date').value.date.year + '-' +
      this.form.get('time_out_date').value.date.month + '-' +
      this.form.get('time_out_date').value.date.day + ' ' +
      this.form.get('time_out_time').value
    );
  }

  _onUpdateSuccess(res) {
    super._onUpdateSuccess(res);

    this.objectUpdated.emit(res);
  }

  recalculate() {
    this.isRecalculating = true;
    this.submit().subscribe(
      (res) => {
        console.log(res)
      },
      (err) => {
        console.log('err ', err);
      },
      () => {
        this.isRecalculating = false;
        console.log('complete')
      }
    );
  }

  setStatus(action) {
    if (action == 'delete') {

      this.service.destroy(this.objectId).subscribe(
        (res) => {
          this.toastr.success('Timelog delete');
        }
      )
    } else {
      this.service.setStatus(this.objectId, action).subscribe(
        (res) => {
          this.objectUpdated.emit(res);
        },
        (err) => {
          console.log(err);
          this.toastr.danger('ERR');
        }
      )
    }
  }

  logValid() {
    return (this.object.status == 'PENDING') ? true : false;
  }

  
  getDteValue(rawDate) {

    if (rawDate) {
      const bd = new Date(formatDateFix(rawDate.date));

      return {
        date: {
          year: bd.getUTCFullYear(),
          month: bd.getUTCMonth() + 1,
          day:  bd.getUTCDate(),
        }
      };
    }
  }

  getTimeValue(rawDate) {
    if (rawDate) {
      const bd = new Date(formatDateFix(rawDate.date));
      
      let minutes = bd.getUTCMinutes() < 10 ? "0" + bd.getUTCMinutes() : bd.getUTCMinutes();
      let hours = bd.getUTCHours()<10?"0"+bd.getUTCHours():bd.getUTCHours(); 

      return hours + ':'+ minutes
    }
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.object && changes.object.currentValue) {
      this.objectId = changes.object.currentValue.id;

      if (changes.object.currentValue.time_in) {
        //console.log(this.getDteValue(changes.object.currentValue.time_in))
        this.form.get('time_in_date').setValue(this.getDteValue(changes.object.currentValue.time_in));
        this.form.get('time_in_time').setValue(this.getTimeValue(changes.object.currentValue.time_in));
      }

      if (changes.object.currentValue.time_out) {
        this.form.get('time_out_date').setValue(this.getDteValue(changes.object.currentValue.time_out));
        this.form.get('time_out_time').setValue(this.getTimeValue(changes.object.currentValue.time_out));
      }
      
      // if (changes.object.currentValue.shift_template_period) {
      //   this.form.get('shift_template_period_id').setValue(changes.object.currentValue.shift_template_period.id);
      // }
    }

    
  }

}
