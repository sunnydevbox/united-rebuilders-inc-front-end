
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
  'id' : [null],
  'salaray_bracket': [null],
  'salary_range_from': [null],
  'salary_range_to': [null],
  'salary_base': [null],
  'monthly_premium': [null],
  'employee_share': [null],
  'employer_share': [null],
};
