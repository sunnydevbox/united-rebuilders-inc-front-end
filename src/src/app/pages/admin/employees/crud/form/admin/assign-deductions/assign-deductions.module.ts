import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@shared/forms.module';

/*****************************************/
/**** IMPORT THE BASE MODULE for FORM ****/
/*****************************************/
/***/
/***/ import { FormModule as BaseFormModule } from '@shared/base/form/form.module';
/***/
/** INSTRUCTED FROM BASE FORM MODULE */
/***/
/***/ import { OverlayModule } from '@shared/overlay/overlay.module';
/***/

import { AssignDeductionsComponent } from './assign-deductions.component';

import { EmployeeDeductionsService } from '@services/employee-deductions.service';
import { DropdownMultiselectorModule } from '@shared/dropdown-multiselector/dropdown-multiselector.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BaseFormModule,
    OverlayModule,
    DropdownMultiselectorModule,
  ],
  declarations: [
    AssignDeductionsComponent
  ],
  exports: [
    AssignDeductionsComponent,
  ],
  providers: [
    EmployeeDeductionsService,
  ]
})
export class AssignDeductionsModule { }
