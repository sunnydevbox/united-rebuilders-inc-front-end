import { Component, OnChanges, Input, Output, SimpleChanges, EventEmitter } from '@angular/core';
// IMPORT THE BASE COMPONENT
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';
import { ERRORS, responseErrors } from '@admin/config';
import { MODEL } from './model';
import { EmployeeService } from '@services/employee.service';

@Component({
  selector: 'app-biofield',
  templateUrl: './biofield.component.html',
  styleUrls: ['./biofield.component.css']
})
export class BiofieldComponent extends BaseFormComponent implements OnChanges {

  @Input() object;
  @Output() objectChanged = new EventEmitter();

  _postConstruct() {
    this.errors = ERRORS;
    this.form = this.formBuilder.group(MODEL);
    this.service = this.injector.get(EmployeeService);
    this.formMode = 'update';
  }

  _onUpdateSuccess(res) {
    super._onUpdateSuccess(res);
  }


  ngOnChanges(changes: SimpleChanges) {

    if (changes.object && changes.object.currentValue) {
      this.objectId = this.object.id;
      this.form.get('bio_id').setValue(changes.object.currentValue.bio_id);
    }

  }

}
