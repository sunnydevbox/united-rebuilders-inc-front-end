import { Component, OnInit, Output, EventEmitter, OnChanges, SimpleChanges, Input } from '@angular/core';
import { ListComponent as BaseListComponent } from '@shared/base/list/list.component' ;
import { UserService } from '@services/user.service';
import { minutesToHoursMinutes } from '@admin/config';
import { EmployeeService } from '@services/employee.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseListComponent implements OnChanges {

  public userService;
  public employeeService;

  @Output() object = new EventEmitter();
  @Input() filters;


  _postConstruct() {
    this.userService = this.injector.get(UserService);
    this.service = this.injector.get(EmployeeService);
    // this.service = this.injector.get(settings.service);

    // this.defaultParams = {
    //   filter: 'id;status;time_in;shift_template_id;shift_template_period_id;'
    //           + 'time_elapsed;tardy_minutes;overtime_minutes;billable_minutes;'
    //           + 'time_out;employee_id',
    //   with: 'employee;shiftTemplatePeriod;shiftTemplate',
    //   orderBy: 'time_in',
    //   sortedBy: 'desc'
    // };

    this.defaultParams = {
      filter: 'id;first_name;last_name;employee_id_number',
    };
  }

  edit(object) {
    this.object.emit(object);
  }

  minutesToHoursMinutes(minutes) {
    return minutesToHoursMinutes(minutes);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.filters && changes.filters.currentValue) {
      // console.log(changes.filters.currentValue, this.defaultParams);

      const q = {

      };
      if (changes.filters.currentValue.query) {
        this.defaultParams['query'] = changes.filters.currentValue.query;
        this.defaultParams['page'] = 1;
      }

      // this.datatable.page = 1;

      // console.log(changes.filters.currentValue, q);
      this.reloadItems(q);
    }
  }
}
