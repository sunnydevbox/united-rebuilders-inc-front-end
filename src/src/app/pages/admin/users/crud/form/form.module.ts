import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoutingModule } from './routing.module';
import { FormComponent } from './form.component';
import { FormsModule } from '@shared/forms.module';

import { settings } from './../../settings';

/*****************************************/
/**** IMPORT THE BASE MODULE for FORM ****/
/*****************************************/
/***/
/***/ import { FormModule as BaseFormModule } from '@shared/base/form/form.module';
/***/
/** INSTRUCTED FROM BASE FORM MODULE */
/***/
/***/ import { OverlayModule } from '@shared/overlay/overlay.module';
/***/

/*****************************************/

/***/ import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { RolesModule } from './roles/roles.module';
import { AdminModule } from './admin/admin.module';
import { DetailsComponent } from './details/details.component';
import { SupplementDataService  } from '@services/supplement-data.service';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,

    OverlayModule,
    FormsModule,
    BaseFormModule,

    RolesModule,
    AdminModule,

    NgbModule,
  ],
  declarations: [
    FormComponent,
    DetailsComponent,
  ],
  providers: [
    settings.service,
    SupplementDataService
  ]
})
export class FormModule { }
