import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

// import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class SupplementDataService extends AbstractService {

  endpoint = 'suppdata';


    index(params) {
        const sq = this.buildQueryString(params);

        return this.http.get(this.apiURL() + sq )
        .map((result: any) => {
            return result;
        })
        .catch(this.handleError);
    }
}
