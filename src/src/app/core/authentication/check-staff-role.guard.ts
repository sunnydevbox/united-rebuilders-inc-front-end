import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { Logger } from '../logger.service';
import { UserService } from '@services/user.service';

const log = new Logger('CheckStaffeGuard');

@Injectable()
export class CheckStaffRoleGuard implements CanActivate {

  constructor(
    private router: Router,
    private userService: UserService
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

      console.log('is Staff? ', this.userService.checkUserRole('Staff'));
    if (this.userService.checkUserRole('Staff')) {
      return true;
    } else {
      log.debug('You are not Staff, redirecting to "me"...');
      this.router.navigate(['/login'], { replaceUrl: true });
      return false;
    }
  }
}
