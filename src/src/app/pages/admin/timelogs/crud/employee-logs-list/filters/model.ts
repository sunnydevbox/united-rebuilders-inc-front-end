
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
    'status': ['all'],
    'time_in': [null],
    'time_out': [null],
    'is_rest_day': [null],
    'is_auto_approve': [null],
};
