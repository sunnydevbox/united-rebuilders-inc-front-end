import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoutingModule } from './routing.module';

import { TitlesComponent } from './titles.component';
import { ListModule } from './crud/list/list.module';
import { FormModule } from './crud/form/form.module';
import { settings } from './settings';


@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    ListModule,
    FormModule
  ],
  declarations: [
    TitlesComponent,
  ],
  exports: [
    TitlesComponent,
  ],
  providers: [
    settings.service
  ]
})
export class TitlesModule { }
