import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

@Injectable()
export class EmployeeService extends AbstractService {
    endpoint = 'employees';


    attachShiftTemplate(employeeId, shiftTemplateId) {
        return this.http.post(this.apiURL() + '/' + employeeId + '/attach-shift-template', {
            'shift_template_id': shiftTemplateId
        }, {
            reportProgress: true,
            responseType: 'json'
          })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
    }

    detachShiftTemplate(employeeId, shiftTemplateId) {
        return this.http.post(this.apiURL() + '/' + employeeId + '/detach-shift-template', {
            'shift_template_id': shiftTemplateId
        }, {
            reportProgress: true,
            responseType: 'json'
          })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
    }

    attachPayrollTemplate(employeeId, payrollTemplateId) {
        return this.http.post(this.apiURL() + '/' + employeeId + '/attach-payroll-template', {
            'payroll_template_id': payrollTemplateId
        }, {
            reportProgress: true,
            responseType: 'json'
          })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
    }

    detachPayrollTemplate(employeeId, payrollTemplateId) {
        return this.http.post(this.apiURL() + '/' + employeeId + '/detach-payroll-template', {
            'payroll_template_id': payrollTemplateId
        }, {
            reportProgress: true,
            responseType: 'json'
          })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
    }


    attachBenefit(employeeId, benefitId) {
        return this.http.post(this.apiURL() + '/attach-benefit', {
            'benefit_id': benefitId,
            'employee_id': employeeId
        }, {
            reportProgress: true,
            responseType: 'json'
          })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
    }

    detachBenefit(employeeId, benefitId) {
        return this.http.post(this.apiURL() + '/detach-benefit', {
            'benefit_id': benefitId,
            'employee_id': employeeId
        }, {
            reportProgress: true,
            responseType: 'json'
          })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
    }


    attachDeduction(employeeId, deductionId) {
        return this.http.post(this.apiURL() + '/attach-deduction', {
            'deduction_id': deductionId,
            'employee_id': employeeId
        }, {
            reportProgress: true,
            responseType: 'json'
          })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
    }

    detachDeduction(employeeId, deductionId) {
        return this.http.post(this.apiURL() + '/detach-deduction', {
            'deduction_id': deductionId,
            'employee_id': employeeId
        }, {
            reportProgress: true,
            responseType: 'json'
          })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
    }


    searchName(query: string): Observable<any> {
        return this.index({
            search: query,
            searchfFields: 'first_name;last_name',
            filter: 'first_name;last_name'
        })
          .map((result: Response) => {
              return result['data'];
          })
          .do(data => console.log(data))
          .catch(this.handleError);
      }
}
