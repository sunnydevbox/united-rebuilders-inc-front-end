import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmploymentTypesComponent } from './employment-types.component';
import { RoutingModule } from './routing.module';

import { ListModule } from './crud/list/list.module';
import { FormModule } from './crud/form/form.module';
import { settings } from './settings';


@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    FormModule,
    ListModule
  ],
  declarations: [
    EmploymentTypesComponent
  ],
  exports: [
    EmploymentTypesComponent,
  ],
  providers: [
    settings.service
  ],
})
export class EmploymentTypesModule { }
