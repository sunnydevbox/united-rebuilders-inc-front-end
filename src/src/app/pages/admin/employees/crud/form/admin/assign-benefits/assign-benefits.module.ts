import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DropdownMultiselectorModule } from '@shared/dropdown-multiselector/dropdown-multiselector.module';
import { AssignBenefitsComponent } from './assign-benefits.component';
import { FormsModule } from '@shared/forms.module';
import { EmployeeService } from '@services/employee.service';

/*****************************************/
/**** IMPORT THE BASE MODULE for FORM ****/
/*****************************************/
/***/
/***/ import { FormModule as BaseFormModule } from '@shared/base/form/form.module';
/***/
/** INSTRUCTED FROM BASE FORM MODULE */
/***/
/***/ import { OverlayModule } from '@shared/overlay/overlay.module';
/***/

@NgModule({
  imports: [
    CommonModule,
    DropdownMultiselectorModule,
    FormsModule,
    BaseFormModule,
    OverlayModule,
  ],
  declarations: [
    AssignBenefitsComponent,
  ],
  exports: [
    AssignBenefitsComponent,
  ],
  providers: [
    EmployeeService,
  ]
})
export class AssignBenefitsModule { }
