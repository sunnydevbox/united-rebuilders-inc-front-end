import { HolidayService } from '@services/holiday.service';

export const settings = {
    plural: 'Holidays',
    singular: 'Holiday',
    service: HolidayService,
    uriPrefix: 'holidays',
    crud: {
      create: {
        allow: true, // true/false
        url: '/app/system/holidays'
      },
      read: {
      },
      update: {
        allow: true,
        url: '/app/system/holidays/edit/'
      },
      delete: {
        allow: true
      },
      list: {
        allow: true,
        url: '/app/system/holidays'
      }
    }
  };
