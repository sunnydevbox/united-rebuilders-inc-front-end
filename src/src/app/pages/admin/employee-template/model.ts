
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
  'id' : [null],
  'description': [null, [Validators.required]],
  'name': [null, [Validators.required]],
  
  // boolean
  'deduction_philhealth': [null],
  'deduction_sss': [null],
  'deduction_tardiness': [null],
  'deduction_bir': [null],
  'deduction_absences': [null],
  'deduction_pag_ibig': [null],
  'deduction_cash_advance': [null],
  
  // boolean
  'benefit_basic_pay': [null],
  'benefit_overtime_pay': [null],
  'benefit_holiday_pay': [null],
  'benefit_restday_pay': [null],
  'benefit_night_differential_pay': [null],
  
  // Text
  'total_days_in_year': [null, [Validators.required]],
  'total_hours_in_day': [null, [Validators.required]],
  'night_differential_start': [null],
  'night_differential_end': [null],
  
  // text
  'total_sick_leave': [null],
  'total_emergency_leave': [null],
  'total_maternity_leave': [null],
  'total_paternity_leave': [null],
  'total_vacation_leave': [null],
  'total_birthday_leave': [null],
  'total_solo_parent_leave': [null],

  'tax_period': [null, [Validators.required]],
};
