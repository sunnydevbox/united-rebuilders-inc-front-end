import { Component, OnInit, Input } from '@angular/core';

// IMPORT THE BASE COMPONENT
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';

import { settings } from './../../../../settings';

@Component({
  selector: 'app-delete-employee',
  templateUrl: './delete-employee.component.html',
  styleUrls: ['./delete-employee.component.css']
})
export class DeleteEmployeeComponent extends BaseFormComponent {

  @Input() object;

  _postConstruct() {
    this.service = this.injector.get(settings.service);
  }

  delete() {

    const sta = this.confirmModalService.open({
      title: 'Purge Employee Record',
      body: 'This process irreversible. Proceed with caution. ',
    });

    sta.result.then((result) => {
      if (result) {

        this.toggleOverlay = true;

        // CONTINUE DELETING
        this.service.destroy(this.object.id).subscribe(
          (response: any) => {
            this.toastr.success('Employee successfully deleted.');
            this.router.navigate([settings.crud.list.url]);
            // this._onDeleteSuccess(response);
          },
          (error) => {
            // this._onDeleteError(error);
          },
          () => {
            // this._onDeleteComplete();
          }
        );
      }
    }, (reason) => {
      console.log(reason);
    });
  }
}
