import { Component, OnInit } from '@angular/core';
import { settings } from './settings';

@Component({
  selector: 'app-payroll-generator',
  templateUrl: './payroll-generator.component.html',
  styleUrls: ['./payroll-generator.component.css']
})
export class PayrollGeneratorComponent implements OnInit {
  
  public settings;
  public payrollTemplateId = null;
  public _triggerReloadList = null;
  private object = null;

  constructor() { 
    this.settings = settings;
  }

  setSelectedPayrollTemplateId(payrollTemplateId) {
    this.payrollTemplateId = payrollTemplateId;
  }

  triggerReloadList(i) {
    this._triggerReloadList = true;
  }

  setObject(object) {
    this.object = object;
  }

  ngOnInit() {
  }

}
