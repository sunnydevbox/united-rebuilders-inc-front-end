import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-remittance',
  templateUrl: './remittance.component.html',
  styleUrls: ['./remittance.component.css']
})
export class RemittanceComponent implements OnInit {

  @Input() lists;
  @Input() object;

  constructor() {
  }

  ngOnInit() {}
}
