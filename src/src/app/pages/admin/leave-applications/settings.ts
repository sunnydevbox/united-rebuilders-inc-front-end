import { LeaveApplicationService } from '@services/leave-application.service';

export const settings = {
    plural: 'Applications',
    singular: 'Application',
    service: LeaveApplicationService,

    crud: {
      create: {
        allow: true, // true/false
        url: '/admin/settings/system/leave-applications/create'
      },
      read: {

      },
      update: {
        allow: true,
        url: '/admin/settings/system/leave-applications/edit/'
      },
      delete: {
        allow: true
      },
      list: {
        allow: true,
        url: '/admin/settings/system/leave-applications'
      }
    }
  };
