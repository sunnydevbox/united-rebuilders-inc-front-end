import { Component, OnInit } from '@angular/core';

import { settings } from './settings';

@Component({
  selector: 'app-shift-templates',
  templateUrl: './shift-templates.component.html',
  styleUrls: ['./shift-templates.component.css']
})
export class ShiftTemplatesComponent implements OnInit {

  public settings;
  public _triggerReloadList;
  public _object;

  constructor() {
    this.settings = settings;
  }

  triggerReloadList(e) {
    if (e) {
      this._triggerReloadList = e;
    }
  }

  getObject(object) {
    this._object = object;
  }

  ngOnInit() {
  }
}
