import { Component, OnInit } from '@angular/core';
import { RouterLinkActive, Router } from '@angular/router';

import { AuthenticationService } from './../../../authentication/authentication.service';
import { environment } from './../../../../../environments/environment';
import { UserService } from '@services/user.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router,
    public userService: UserService
  ) { }

  ngOnInit() {
  }

  logout() {
    this.authenticationService.logout()
    .subscribe(() => {
      // this.router.navigate(['/login'], { replaceUrl: true })
      //window.location.href = environment.BASE_URL + 'login';
      this.router.navigateByUrl('login');
     });
  }
}
