import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PhilhealthTableComponent } from './philhealth-table.component';

describe('PhilhealthTableComponent', () => {
  let component: PhilhealthTableComponent;
  let fixture: ComponentFixture<PhilhealthTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PhilhealthTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PhilhealthTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
