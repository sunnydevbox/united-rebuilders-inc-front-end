const path = require('path');
const { AngularCompilerPlugin } = require('@ngtools/webpack');

 let plugins = [];

// plugins.push(new AngularCompilerPlugin({
//     "mainPath": "main.ts",
//     "platform": 0,
//     "sourceMap": true,
//     "tsConfigPath": "src/tsconfig.app.json",
//     "skipCodeGeneration": true,
//     "compilerOptions": {},
//     "hostReplacementPaths": {
//       "environments/index.ts": "environments/index.prod.ts"
//     },
//     "exclude": []
//   }));

module.exports = {
  entry: './src/main.ts',

  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist')
  },
  plugins: plugins,
  "resolve": {
    "extensions": [
      ".ts",
      ".js",
      ".scss",
      ".json"
    ],
    "aliasFields": [],
    "alias": { // WORKAROUND See. angular-cli/issues/5433
      "environments": path.resolve(__dirname, 'src/environments/environment.prod.ts')
    },
    "modules": [
      "./node_modules"
    ],
    "mainFields": [
      "browser",
      "module",
      "main"
    ]
  },
};