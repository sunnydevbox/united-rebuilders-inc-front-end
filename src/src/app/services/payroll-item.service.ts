import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class PayrollItemService extends AbstractService {
    endpoint = 'payroll-items';


    customEntry(data) {
        return this.http.post(this.apiURL() + '/custom', data, {
            reportProgress: true,
            responseType: 'json'
          })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
        
    }
}
