import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LoaderComponent } from './loader/loader.component';
import { OverlayComponent } from './overlay/overlay.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
   //  NgbModule.forRoot(),
  ],
  declarations: [
    LoaderComponent,
    // OverlayComponent
  ],
  exports: [
    LoaderComponent,
    // OverlayComponent,
    NgbModule,
  ]
})
export class SharedModule { }
