import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class LeaveApplicationService extends AbstractService {
    endpoint = 'leave-applications';


    setStatus(id, action) {
        return this.http.post(this.apiURL() + '/' + id + '/' + action, {
            status: action
        }, {
            reportProgress: true,
            responseType: 'json'
          })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
    }
}
