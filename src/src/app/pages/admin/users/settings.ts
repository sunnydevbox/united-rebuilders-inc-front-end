import { UserService } from '@services/user.service';

export const settings = {
    plural: 'Users',
    singular: 'user',
    service: UserService,
    uriPrefix: 'users',

    crud: {
      create: {
        allow: true, // true/false
        url: '/app/settings/security/users/add'
      },
      read: {

      },
      update: {
        allow: true,
        url: '/app/settings/security/users/edit/'
      },
      delete: {
        allow: true
      },
      list: {
        allow: true,
        url: '/app/settings/security/users'
      }
    }
  };
