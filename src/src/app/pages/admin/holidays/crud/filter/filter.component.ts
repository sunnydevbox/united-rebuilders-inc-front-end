import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormComponent } from '@shared/base/form/form.component';
import { settings } from './../../settings';
import { MODEL } from './model';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent extends FormComponent {
  @Input() currentYear;
  @Output() changedYear = new EventEmitter();

  _postConstruct() { 
    this.service = this.injector.get(settings.service);
    this.form = this.formBuilder.group(MODEL);
  }

  ngOnInit() {
  }

  yearChanged(e) { 
    this.changedYear.emit(e.target.value);
  }


  getYearRange() {
    let startYear = 1980;
    let advancedYear = this.currentYear + 5;
    let years = [];
    for (let index = advancedYear; index >= startYear; index--) {

      years.push(index);
      
    }

    return years;
  }
}
