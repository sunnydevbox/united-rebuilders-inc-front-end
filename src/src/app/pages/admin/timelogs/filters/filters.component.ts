import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';

import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';
import { FiltersComponent as BaseFiltersComponent } from '@shared/base/filters/filters.component';


import {Observable} from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { MODEL } from './model';
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import { datePickerOptions1 } from '@admin/config';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css']
})
export class FiltersComponent extends BaseFiltersComponent implements OnInit, OnDestroy {

  private formControleName: Subscription;
  public datePickerOptions1;

  @Output() filtersChanged = new EventEmitter();
  @Output() searchChangeEmitter = new EventEmitter();
  // This is an output component that tells the rest of the world that the user has entered a valid text

  _postConstruct() {
    this.datePickerOptions1 = datePickerOptions1;
    this.model = MODEL;
    this.form = this.formBuilder.group(MODEL);

    // Observable.fromEvent(this.form.get('name') , 'keyup').debounceTime(1000).subscribe(value => /* */)
    this.formControleName = this.form.get('name').valueChanges
      .debounceTime(1000)
      .subscribe((newValue) => {
        if (newValue && newValue.length >= 2) {
          this.startFilter();
        }
      });
  }

  fieldProcessor(field) {
    super.fieldProcessor(field);

    if (field === 'date_start') {
      this.query.push('time_in|' + this.form.get(field).value);
    }
  }

  dateChanged(field, event: IMyDateModel) {
    if (field === 'date_start') {
      this.form.get(field).setValue(event.formatted);
      // this.query.push('time_in|' + this.form.get(field).value);
    }

    this.startFilter();
  }

  ngOnDestroy() {
    // this.formControleName.unsubscribe();
  }
}
