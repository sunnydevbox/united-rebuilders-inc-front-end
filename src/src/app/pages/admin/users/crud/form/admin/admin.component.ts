import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';

import { RoleService } from '@services/role.service';
import { UserService } from '@services/user.service';

import { MODEL } from './model';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent extends BaseFormComponent implements OnChanges {

  @Input() object;
  @Input() objectId;
  public userService;

  _postConstruct() {
    this.service = this.injector.get(RoleService);
    // this.form = this.formBuilder.group(MODEL);
    this.userService = this.injector.get(UserService);

    this.form = this.formBuilder.group(MODEL);

  }

  setStatus(e) {
    this.toggleOverlay = true;
    this.userService.setStatus(this.objectId, (e.srcElement.checked ? 'active' : 'inactive'))
    .subscribe(
      (res) => {
        this.toggleOverlay = false;
        this.toastr.success('Status set');
      },
      (err) => {

      }
    );
  }


  ngOnChanges(changes: SimpleChanges) {

    if (changes.object && changes.object.currentValue) {
      // console.log(changes.object.currentValue);
      // this.form.get('status').sele = true; 
      // this.form.get('status').setValue(changes.object.currentValue.status);
    }
  }
}
