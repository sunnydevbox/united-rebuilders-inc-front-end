import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
  'id' : [null],

  'salary_bracket': [null, Validators.compose([Validators.required])],
  'salary_range_from': [null],
  'salary_range_to': [null],
  'salary_base': [null, Validators.compose([Validators.required])],
  'monthly_premium': [null, Validators.compose([Validators.required])],
  'employee_share': [null, Validators.compose([Validators.required])],
  'employer_share': [null, Validators.compose([Validators.required])], 
}
