
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

export const MODEL = {
  'start_date': [null],
  'end_date': [null],

  'payrollLog': [null]
  // 'payroll_template_id': [null, Validators.compose([Validators.required])],
};
