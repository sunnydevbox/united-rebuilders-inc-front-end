import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class EmployeeLogsService extends AbstractService {
    endpoint = 'employee-logs';
}
