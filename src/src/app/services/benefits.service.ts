import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class BenefitsService extends AbstractService {
    endpoint = 'benefits';
}
