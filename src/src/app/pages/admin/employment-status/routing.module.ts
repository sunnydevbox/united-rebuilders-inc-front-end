import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { Route } from './../../../core/route.service';

import { ListComponent } from './crud/list/list.component';
import { FormComponent } from './crud/form/form.component';
import { EmploymentStatusComponent } from './employment-status.component';

const routes: Routes = [
    {
        path: '',
        //component: ListComponent,
        data: { title: 'Employment Status'  },
        children:[
            {
              path: '',
              component: EmploymentStatusComponent,
            },
            {
              path: 'edit/:id',
              component: FormComponent,
              data: { title: 'Edit Employment Status'  },
            },
          ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}