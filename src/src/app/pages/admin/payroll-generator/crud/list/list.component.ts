import { Component, OnInit, Output, EventEmitter, OnChanges, SimpleChanges, Input } from '@angular/core';

import { ListComponent as BaseListComponent } from '@shared/base/list/list.component' ;

import { settings } from './../../settings';
import { UserService } from '@services/user.service';
import { distanceInWords } from 'date-fns';

import { minutesToHoursMinutes } from '@admin/config';
import { PayrollLogService } from '@services/payroll-log.service';
import { formatDateFix } from '@admin/config';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseListComponent implements OnChanges {

  public userService;
  public settings;

  @Output() object = new EventEmitter();
  @Input() filters;
  @Input() payrollTemplateId = null;


  _postConstruct() {
    // this.loadListOnInit = false;
    this.settings = settings;
    this.service = this.injector.get(PayrollLogService);
    this.userService = this.injector.get(UserService);
    this.defaultParams = {
      search: this.payrollTemplateId,
      // filter: 'id;status;start;end;payroll_template_id;pay_date;period_count;triggered',
      with: 'payroll_template',
      orderBy: 'start',
      sortedBy: 'desc'
    };
  }

  formatDateFix(v) {
    return formatDateFix(v);
  }

  edit(object) {
    this.object.emit(object);
  }

  goToPayroll(item) {
    this.router.navigate(['/app/settings/system/payrolls'], { queryParams: { payroll: item } });
  }

  triggerGenerate(object) {

    const sta = this.confirmModalService.open({
      title: 'Processing Payrolls',
      body: 'Payrolls are processed in the background. Please check back for the progress.'
            + '<br/> <strong class="text-danger">IMPORTANT:</strong> Ensure that timelogs are verified and approved before proceeding.',
      ctaPositive: 'Continue',
    });

    sta.result.then((result) => {
      if (result) {
        this.service.triggerGenerate(object.id).subscribe(
          (res) => {
            console.log(res);
            this.toastr.success(res.message)
          }
        )
      }
    }, (reason) => {
      console.log(reason);
      this.toastr.success(reason.message)
    });
  }

  minutesToHoursMinutes(minutes) {
    return minutesToHoursMinutes(minutes);
  }

  ngOnChanges(changes: SimpleChanges) {
    super.ngOnChanges(changes);

    if (changes.payrollTemplateId && changes.payrollTemplateId.currentValue) {
      this.payrollTemplateId = changes.payrollTemplateId.currentValue;
      this.reloadItems({
        search: this.payrollTemplateId
      });
    }
  }
}
