import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';

import { HolidayService } from '@services/holiday.service';
import { ListComponent as BaseListComponent } from '@shared/base/list/list.component' ;

import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { startOfDay, endOfDay, subDays, addDays, startOfMonth, endOfMonth, startOfWeek, endOfWeek, getMonth } from 'date-fns';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent } from 'angular-calendar';
import { Subject } from 'rxjs/Subject';

interface E {
  id: number;
  title: string;
  start: string;
}


@Component({
  selector: 'app-calendar-list',
  templateUrl: './calendar-list.component.html',
  styleUrls: ['./calendar-list.component.css']
})
export class CalendarListComponent extends BaseListComponent implements OnChanges {

  public selectedMonth = getMonth(new Date());

  @Input() toggleReloadCalendar = false;

  view: String = 'month';

  public viewDate: Date = new Date();

  events: CalendarEvent[] = [
    {
      start: subDays(startOfDay(new Date()), 1),
      end: addDays(new Date(), 1),
      title: 'A 3 day event',
      // color: colors.red,
      // actions: this.actions
    }
  ];


  _postConstruct() {
    this.service = this.injector.get(HolidayService);
    this.fetchMonth()
    this.defaultParams = {
      'limit': 0,
      // 'month':w
    };
  }


  fetchMonth() {
    const getStart: any = {
      month: startOfMonth,
      week: startOfWeek,
      day: startOfDay
    }['month'];

    const getEnd: any = {
      month: endOfMonth,
      week: endOfWeek,
      day: endOfDay
    }['month'];
    let d = new Date();
    console.log(getStart(d), getEnd(d), this.view, this.viewDate);
    this.service.monthList(getStart(d), getEnd(d)).subscribe(
      (res) => {
        console.log(res);
      }
    )
  }

  _onSuccess(res) {
    super._onSuccess(res);
    
    // this.events = res.data
    const entry = [];
    for (const index in res.data) {

      if (res.data[index]) {
        entry.push({
          start: new Date(res.data[index].start_date), // subDays(startOfDay(new Date()), 1),
          end: new Date(res.data[index].end_date), // addDays(new Date(), 1),
          title: res.data[index].name,
        });

        // this.events.push()

      }
    }

    this.events = entry;

    // console.log(this.events)

    // console.log(this.events = e);

  }
}
