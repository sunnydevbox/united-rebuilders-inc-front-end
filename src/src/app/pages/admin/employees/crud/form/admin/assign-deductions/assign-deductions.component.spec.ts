import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignDeductionsComponent } from './assign-deductions.component';

describe('AssignDeductionsComponent', () => {
  let component: AssignDeductionsComponent;
  let fixture: ComponentFixture<AssignDeductionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignDeductionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignDeductionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
