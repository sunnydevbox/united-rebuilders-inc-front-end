import { Component, OnInit } from '@angular/core';
import { RouterLinkActive, Router } from '@angular/router';

import { AuthenticationService } from './../../authentication/authentication.service';
import { environment } from './../../../../environments/environment';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  logout() {
    this.authenticationService.logout()
    .subscribe(() => {
      // this.router.navigate(['/login'], { replaceUrl: true })
      window.location.href = environment.BASE_URL + 'login';
     });
  }
}
