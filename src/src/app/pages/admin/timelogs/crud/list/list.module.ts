import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';


/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { DataTableModule } from 'angular5-data-table';
import { OverlayModule } from '@shared/overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */


import { ListModule as BaseListModule } from '@shared/base/list/list.module' ;
import { ListComponent } from './list.component';
import { StatusBadgeModule } from '@shared/status-badge/status-badge.module';
import { EmployeeService } from '@services/employee.service';


@NgModule({
  imports: [
    CommonModule,
    BaseListModule,
    DataTableModule,
    OverlayModule,
    NgbModule,
    StatusBadgeModule,
    RouterModule,
  ],
  declarations: [
    ListComponent,
  ],
  exports: [
    ListComponent
  ],
  providers: [
    EmployeeService
  ]
})
export class ListModule { }
