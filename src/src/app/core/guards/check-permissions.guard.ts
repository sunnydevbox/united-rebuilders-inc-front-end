import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { Router } from '@angular/router';

import { AuthenticationService } from './../../core/authentication/authentication.service';
import { UserService } from '@services/user.service';
import { environment } from './../../../environments/environment';

@Injectable()
export class CheckPermissionGuard implements CanActivate {
  constructor(
    private userService: UserService,
    private router: Router
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      
      // ADMIN has access to ALL
      if (this.userService.checkRole(['admin'])) {
        return true;
      }

      const permissions = next.data['permissions'] as Array<string>;

      // TEMPORARY:
      if (permissions.indexOf('can_access_admin') > -1) {
        return true;
      }
      

      const hasPermission = this.userService.checkPermission(permissions);
      
      // console.log('PERMISSION', hasPermission, permissions, this.userService.isLoggedIn());
      
      if (!this.userService.isLoggedIn()) {
        window.location.hash = 'login';
        window.location.reload();
        return false;
      }
      
      if (!hasPermission) {
        // DISPLAY alert message
        // that user has no permission
        // console.log(environment.admin.REDIRECT_URL + '/no-permission')
        // console.log('no aperm')
        this.router.navigate(['app/no-permission']);

        // KILL STORED CREDENTIALS?
        // ...

        // console.log(' THIS SHOULD REDIRECT YOU TO THE LOGIN PAGE')
        // this.router.navigate(['login']);
        // window.location.hash = 'login';
        // window.location.reload();
        return false;
      }

      return true;
  }
}
