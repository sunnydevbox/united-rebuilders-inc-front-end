import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class TimelogService extends AbstractService {
    endpoint = 'timelogs';

    import(data) {
        return this.http.post(this.apiURL() + '/import', data, {
            reportProgress: true,
            responseType: 'json'
          })
        .map((result: any) => {
            return result;
        }, (err: any) => {
            return err;
        })
        .catch(this.handleError);
    }

    setStatus(id, action) {
        return this.http.post(this.apiURL() + '/' + id + '/' + action, {}, {
            reportProgress: true,
            responseType: 'json'
          })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
    }

    recalculate(id) {
        return this.http.post(this.apiURL() + '/recalculate/' + id, {}, {
            reportProgress: true,
            responseType: 'json'
          })
            .map(
                (result) => {
                    return result;
                },
                (err) => {
                    return err;
                }
            )
            .catch(this.handleError);
    }
}
