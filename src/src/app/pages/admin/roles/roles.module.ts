import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/** KEEP */
// import { CrudModule } from './../../../core/base/crud/crud.module';
// import { SharedModule } from '@shared/shared.module';
// import { FormsModule } from '@shared/forms.module';
// import { ListComponent, EditComponent, CreateComponent } from './crud';

import { ListModule } from './crud/list/list.module';
import { FormModule } from './crud/form/form.module';
import { RolesComponent } from './roles.component';
import { settings } from './settings';
import { RoutingModule } from './routing.module';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,   
    ListModule,
    FormModule,
  ],
  declarations: [
    RolesComponent
  ],
  exports: [
  ],
  providers: [
    settings.service,
  ]
})
export class RolesModule { }
