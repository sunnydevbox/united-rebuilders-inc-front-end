import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { finalize } from 'rxjs/operators';

import { environment } from './../../../../environments/environment';
import { AuthenticationService } from './../../../core/authentication/authentication.service';

import { Input, Output, AfterContentInit, ContentChild, AfterViewInit, AfterViewChecked, ViewChild, ViewChildren } from '@angular/core';

import { UserService } from '@services/user.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

import { ConfirmService } from '@shared/confirm/confirm.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {

  version: string = environment.version;
  error: any = false;
  loginForm: FormGroup;
  isLoading = false;
  environment = environment;
  @ViewChildren('input') vc;

  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    public toastr: ToastsManager,
    vcr: ViewContainerRef,
    private confirmService: ConfirmService
) {
  this.createForm();
  this.toastr.setRootViewContainerRef(vcr);
}

  ngOnInit() {
    // this.userService.clearCurrentUser();
  }

  ngAfterViewInit() {
    // console.log('ngAfterViewInit', this.vc.first.nativeElement)
    this.vc.first.nativeElement.focus();
  }

  login() {
    this.isLoading = true;
    this.error = false;

    const fdata = {
      email: this.loginForm.value.username,
      password: this.loginForm.value.password,
      remember: this.loginForm.value.remember,
    };

    // log.info('Login attempt - ' + this.loginForm.value.username);
    this.userService.authenticate(fdata)
    .subscribe(
    (response: any) => {

      if (typeof response.status !== 'undefined') {
        // log.error(response);
        this.error = response;
        this.isLoading = false;
        return false;
      }

      const roles = (response.roles
        && typeof response.roles.data !== 'undefined') ? response.roles.data : response.roles;
      roles.forEach((role: any) => {
        // if (role.name === 'admin') {
          // log.info('Admin user verified');
          if (environment.admin.ENABLED) {
            // log.info('Redirecting to Dashboard...');
            // window.location.hash = '/' + environment.admin.REDIRECT_URL;
            // window.location.reload();
            window.location.href = '/'; // #' + environment.admin.REDIRECT_URL;
          }
        // }
      });
    },
    (error: string) => {
      // console.log(error);
        this.error = error;
        this.isLoading = false;
      }
    );
  }


  private createForm() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      remember: true
    });
  }

  openM(content) {
    const settings = {
      title: 'Confirm',
      body: 'Are you sure you want to delete?'
    };
    this.confirmService.open(settings);
  }

}
