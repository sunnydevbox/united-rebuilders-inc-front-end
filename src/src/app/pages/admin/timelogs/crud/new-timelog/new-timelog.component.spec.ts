import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewTimelogComponent } from './new-timelog.component';

describe('NewTimelogComponent', () => {
  let component: NewTimelogComponent;
  let fixture: ComponentFixture<NewTimelogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewTimelogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewTimelogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
