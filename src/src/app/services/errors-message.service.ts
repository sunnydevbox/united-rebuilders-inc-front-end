import { Injectable } from '@angular/core';


@Injectable()
export class ErrorMessageService {

  private errors = {
    po_invalid_items_count: 'You have not added items to this order',
    po_invalid_current_status: 'The current status of this purhcase order does not allow you to do that.',
  };

  _e(code: string) {
    code = code.toLowerCase();

    if (typeof this.errors[code] !== 'undefined') {
      return this.errors[code]
    }

    return '';
  }
}
