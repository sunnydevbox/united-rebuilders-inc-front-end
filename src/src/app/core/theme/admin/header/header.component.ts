import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from './../../../authentication/authentication.service';
import { environment } from './../../../../../environments/environment';

import { UserService } from '@services/user.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {


  menuHidden = true;
  environment = environment;
  @Input() user: any;

  constructor(private router: Router,
              private userService: UserService
  ) { }

  ngOnInit() {}

  toggleMenu() {
    this.menuHidden = !this.menuHidden;
  }

  logout() {
    this.userService.logout()
    .subscribe(() => {
      window.location.hash = '/login';
      window.location.reload();

     });
  }

}
