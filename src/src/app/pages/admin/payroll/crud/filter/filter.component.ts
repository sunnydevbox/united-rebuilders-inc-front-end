import { Component, Output, EventEmitter } from '@angular/core';
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';
import { MODEL } from './model';
import { datePickerOptions1 } from '@admin/config';
import { IMyDateModel} from 'mydatepicker';
import { settings } from './../../settings';
import { PayrollLogService } from '@services/payroll-log.service';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent extends BaseFormComponent {

  public datePickerOptions;
  private startDate;
  private endDate;

  @Output() objectChanged = new EventEmitter();

  _postConstruct() {
    this.form = this.formBuilder.group(MODEL);
    this.datePickerOptions = datePickerOptions1;
    // this.service = this.injector.get(PayrollLogService);
    this.service = this.injector.get(settings.service);
  }

  dateChanged(type, event: IMyDateModel) {
    if (type == 'start') {
      this.startDate = event.date.year + '-' + event.date.month + '-' + event.date.day;
    } else {
      this.endDate = event.date.year + '-' + event.date.month + '-' + event.date.day;
    }
    
    this.fetchList();
  }

  fetchList() {
    if (this.startDate && this.endDate) {
      this.objectChanged.emit(null);
      let dateRange = this.startDate + '|' + this.endDate;
      this.service.index({
        dateRange: dateRange,
        limit: 0,
        with: 'payroll_log;items;employee',
      }).subscribe(
        (res) => {
          console.log(res);
          this.objectChanged.emit(res.data);
        }
      )
    }
  }

}
