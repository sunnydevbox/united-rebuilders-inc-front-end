import { EmployeeService } from '@services/employee.service';

export const settings = {
    plural: 'Employees',
    singular: 'Employee',
    service: EmployeeService,
    uriPrefix: 'employees',

    crud: {
      create: {
        allow: true, // true/false
        url: '/app/employees/add'
      },
      read: {

      },
      update: {
        allow: true,
        url: '/app/employees/edit/'
      },
      delete: {
        allow: true
      },
      list: {
        allow: true,
        url: '/app/employees'
      }
    }
  };
