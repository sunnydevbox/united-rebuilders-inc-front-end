import { Component, Output, EventEmitter, Input, SimpleChanges, OnChanges } from '@angular/core';
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';
import { MODEL } from './model';
import { settings } from './../../settings';
import { ERRORS, datePickerOptions1 } from '@admin/config';
import { EmployeeService } from '@services/employee.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import { TypeaheadMatch } from 'ngx-bootstrap/typeahead'

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent extends BaseFormComponent implements OnChanges {
  @Output() triggerReloadList = new EventEmitter();
  @Output() objectChanged = new EventEmitter();
  @Input() object;
  @Input() suppData;

  public employeesLoaded = true;
  public employeeService;
  public datePickerOptions = datePickerOptions1;
  public isCashAdvance = false;
  public dataSource: Observable<any>;
  public typeaheadLoading: boolean = false;


  _postConstruct() {
    this.service = this.injector.get(settings.service);
    this.form = this.formBuilder.group(MODEL);
    this.employeeService = this.injector.get(EmployeeService);

    this.dataSource = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(observer.destination.outerValue);      
    }).mergeMap((token: string) => {  

      return this.employeeService.index({
        filter: 'id;first_name;last_name',
        orderBy: 'first_name',
        limit: 0,
        query: 'name|' + token,
      })
      .map(response => {
        return response.data.map((item) => {
          let fullName = [item.first_name, item.last_name, '('+item.employee_id_number+')'].join(" ");
          item['full_name'] = fullName;
          return item;
        })
      });
    })
  }

  typeaheadOnSelect(e: TypeaheadMatch): void {
    this.form.get('employee_id').setValue(e.item.id);
  }
  
  changeTypeaheadLoading(e: boolean): void {
    this.typeaheadLoading = e;
  }

  _onSaveSuccess(res) {
    super._onSaveSuccess(res);

    // Trigger Relaod list
    this.triggerReloadList.emit(true);

    // Reset the form
    this.form.reset();
  }

  _onUpdateSuccess(res) {
    super._onUpdateSuccess(res);
    // Trigger Relaod list
    this.triggerReloadList.emit(true);

    // REset back to create mode
    this.cancel();
  }


  cancel() {
    this.formMode = 'create';
    this.objectId = null;
    this.form.reset();
    this.object = null;
    this.objectChanged.emit(null);
  }

  approve(disapprove)
  {
    let action = 'approve';
    let status = 'approved';

    if (disapprove) {
      action = 'disapprove';
      status = 'disapproved';
    }

    const sta = this.confirmModalService.open({
      title: 'Leave Application Approval',
      body: 'Are you sure you want to <strong>' + action + '</strong> this application?',
      ctaPositive: 'Continue',
    },
    () => {
      this.service.setStatus(this.objectId, status).subscribe(
        (res) => {
          this.triggerReloadList.emit(true);
          this.toastr.success('Application ' + status);
        },
        (err) => {
          console.log('err ', err);
        }
      )
    });
  }

  ifCashAdvance(id) {
    this.isCashAdvance = false;
    console.log(this.suppData['leave-types'])
    for (let key in this.suppData['leave-types'])  {
      console.log(this.suppData['leave-types'][key])
      if (this.suppData['leave-types'][key].id == id) {
        this.isCashAdvance = true;
      }
    };
  }

  selectedType(e) {
    this.ifCashAdvance(e.target.value);
  }

  disableApprove() {
    if (!this.object) 
      return true;

    return false;
  }

  disableDisapprove() {
    if (!this.object) 
      return true;

    return false;
  }

  _preSubmit() {
    this.form.get('start').setValue(
      this.form.get('time_in_date').value.date.year + '-' +
      this.form.get('time_in_date').value.date.month + '-' +
      this.form.get('time_in_date').value.date.day + ' ' +
      this.form.get('time_in_time').value
    );


    this.form.get('end').setValue(
      this.form.get('time_out_date').value.date.year + '-' +
      this.form.get('time_out_date').value.date.month + '-' +
      this.form.get('time_out_date').value.date.day + ' ' +
      this.form.get('time_out_time').value
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.object && changes.object.currentValue) {
      this.formMode = 'update';
      this.objectId = changes.object.currentValue.id;

      this.form.get('leave_type_id').setValue(changes.object.currentValue.leave_type_id);
      this.form.get('employee_id').setValue(changes.object.currentValue.employee_id);
      this.form.get('purpose').setValue(changes.object.currentValue.purpose);

      let time_in_date;
      let time_in_time;
      if (changes.object.currentValue.start) {
        const bd = new Date(changes.object.currentValue.start.date);
        time_in_date = {
          date: {
            year: bd.getFullYear(),
            month: bd.getMonth() + 1,
            day:  bd.getDate()
          }
        };
        time_in_time = ('0' + bd.getHours()).slice(-2) + ':' + ('0' + bd.getMinutes()).slice(-2);

        this.form.get('time_in_date').setValue(time_in_date);
        this.form.get('time_in_time').setValue(time_in_time);
      }

      let time_out_date;
      let time_out_time;
      if (changes.object.currentValue.end) {
        const bd = new Date(changes.object.currentValue.end.date);
        time_out_date = {
          date: {
            year: bd.getFullYear(),
            month: bd.getMonth() + 1,
            day:  bd.getDate()
          }
        };
        time_out_time = ('0' + bd.getHours()).slice(-2) + ':' + ('0' + bd.getMinutes()).slice(-2);

        this.form.get('time_out_date').setValue(time_out_date);
        this.form.get('time_out_time').setValue(time_out_time);
      }
    } else {
      // this.formMode = 'c';
    }


    // if (this.formMode) {
    //   this.form.enable();
    // } else {
    //   this.form.disable();
    // }
  }
}
