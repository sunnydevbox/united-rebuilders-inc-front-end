import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotesFormComponent } from './notes-form.component';
import { FormsModule } from '@shared/forms.module';
import { FormModule as BaseFormModule } from '@shared/base/form/form.module';

/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { OverlayModule } from '@shared/overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */

@NgModule({
  declarations: [NotesFormComponent],
  imports: [
    CommonModule,
    FormsModule,
    BaseFormModule,
  ],
  entryComponents: [
    NotesFormComponent,
  ]
})
export class NotesFormModule { }
