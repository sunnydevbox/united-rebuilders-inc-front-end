import { Component, Output, EventEmitter } from '@angular/core';
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';
import { MODEL } from './model';
import { datePickerOptions1 } from '@admin/config';
import { IMyDateModel} from 'mydatepicker';
import { settings } from './settings';
import { PayrollLogService } from '@services/payroll-log.service';
import { formatDateFix, months } from '@admin/config';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent extends BaseFormComponent {

  public datePickerOptions;
  private startDate;
  private endDate;
  private payrollLogService;
  public payrolLogList = [];

  @Output() payrollLogChanged = new EventEmitter();

  @Output() objectChanged = new EventEmitter();

  _postConstruct() {
    this.form = this.formBuilder.group(MODEL);
    this.datePickerOptions = datePickerOptions1;
    // this.service = this.injector.get(PayrollLogService);
    this.payrollLogService = this.injector.get(PayrollLogService);
    this.service = this.injector.get(settings.service);

    this.getPayrollLogs()
  }

  formatDateFix(v) {
    return formatDateFix(v)
  }


  getPayrollLogs()
  {
    this.payrollLogService.index({
      search: 'status:DONE',
      filter: 'id;start;end;status',
      orderBy: 'start',
      sortedBy: 'desc'
    })
    .subscribe((res) => {

      res.data.forEach(function(element, key) {
        let start = new Date(formatDateFix(element.start.date));
        let end = new Date(formatDateFix(element.end.date));
        element.start = months[start.getUTCMonth()] + ' ' + start.getUTCDay() + ' ' + start.getUTCFullYear();
        element.end = months[end.getUTCMonth()] + ' ' + end.getUTCDay() + ' ' + end.getUTCFullYear();        
        res.data[key] = element;
      });

      this.payrolLogList = res.data;
    },
    (error) => {
      console.log('error', error);
    })
  }

  changePayrollLog(e) {
    this.payrollLogChanged.emit(e.target.value);
  }

  dateChanged(type, event: IMyDateModel) {
    if (type == 'start') {
      this.startDate = event.date.year + '-' + event.date.month + '-' + event.date.day;
    } else {
      this.endDate = event.date.year + '-' + event.date.month + '-' + event.date.day;
    }
    
    this.fetchList();
  }

  fetchList() {
    if (this.startDate && this.endDate) {
      this.objectChanged.emit(null);
      let dateRange = this.startDate + '|' + this.endDate;
      this.service.index({
        dateRange: dateRange,
        limit: 0,
        with: 'payroll_log;items;employee',
      }).subscribe(
        (res) => {
          console.log(res);
          this.objectChanged.emit(res.data);
        }
      )
    }
  }

}
