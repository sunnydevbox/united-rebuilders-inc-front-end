import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ListComponent as BaseListComponent } from '@shared/base/list/list.component' ;
import { settings } from './../../settings';
import { UserService } from '@services/user.service';
import { ModalService } from '@shared/modal/modal.service';
import { NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { CustomItemFormComponent } from './../custom-item-form/custom-item-form.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseListComponent {

  public userService;
  public currency;
  public popupFormModalService;

  @Output() object = new EventEmitter();

  _postConstruct() {
    this.popupFormModalService = this.injector.get(ModalService);

    this.currency = new Intl.NumberFormat('en-US', {
      // style: 'currency',
      currency: 'Php',
      minimumFractionDigits: 2
    })

    this.service = this.injector.get(settings.service);
    this.userService = this.injector.get(UserService);

    this.defaultParams = {
      orderBy: 'compensation_from',
      sortedBy: 'asc',
      limit: 0,
    };
  }

  edit(object) {
    this.object.emit(object);
    let modalOptions: NgbModalOptions = {
      size: 'lg',
    }


    let modalSettings = {
      data: {
        item: object,
      },
      template: CustomItemFormComponent,
      title: 'Update ',
    }
    
    let callback = (modal) => {
      modal.componentInstance.activeModal.dismiss('close');
    };

    this.triggerModal(modalOptions, modalSettings, callback);
  }

  add() {
    let modalOptions: NgbModalOptions = {
      size: 'lg',
    }


    let modalSettings = {
      data: {},
      template: CustomItemFormComponent,
      title: 'New SSS Record',
    }
    
    let callback = (modal) => {
      modal.componentInstance.activeModal.dismiss('close');
    };

    this.triggerModal(modalOptions, modalSettings, callback);
  }

  emitterHandler(m, callback) {
    let instance = m.componentInstance.componentRef.instance;

    if (!!instance.closeForm) {
      instance.closeForm.subscribe((res) => {
        m.componentInstance.activeModal.dismiss('close');
      });
    }
    if (!!instance.triggerReloadList) {
      instance.triggerReloadList.subscribe((res) => {
        m.componentInstance.activeModal.dismiss('close');
        this.reloadItems(this.defaultParams);
      });
    }
  }

  triggerModal(modalOptions, modalSettings = {}, callback) {
    this.popupFormModalService.setComponent(CustomItemFormComponent);

    let m = this.popupFormModalService.open(modalOptions, modalSettings);

    this.emitterHandler(m, callback);
  }
}
