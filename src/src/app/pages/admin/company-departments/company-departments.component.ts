import { Component, OnInit } from '@angular/core';

import { settings } from './settings';
@Component({
  selector: 'app-company-departments',
  templateUrl: './company-departments.component.html',
  styleUrls: ['./company-departments.component.css']
})
export class CompanyDepartmentsComponent implements OnInit {

  public settings; 
  public _triggerReloadList;
  public _object;

  constructor() { 
    this.settings = settings;
  }

  triggerReloadList(e) {
    if (e) {
      this._triggerReloadList = e;
    }
  }

  getObject(object) {
    this._object = object;
  }

  ngOnInit() {
  }

}
