import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FiltersComponent } from './filters.component';
import { FormModule } from './../form/form.module';

@NgModule({
  imports: [
    CommonModule,
    FormModule
  ],
  declarations: [
    FiltersComponent
  ]
})
export class FiltersModule { }
