import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class EmployeeDeductionsService extends AbstractService {
    endpoint = 'employee-deductions';


    attach(employeeId, deductionId) {
        return this.http.post(this.apiURL() + '/attach-deduction', {
            employee_id: employeeId,
            deduction_id: deductionId,
        })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
    }

    detach(employeeId, deductionId) {
        return this.http.post(this.apiURL() + '/detach-deduction', {
            employee_id: employeeId,
            deduction_id: deductionId,
        })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
    }


}
