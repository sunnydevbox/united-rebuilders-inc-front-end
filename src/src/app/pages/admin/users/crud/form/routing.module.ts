import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { Route } from './../../../../../core/route.service';

import { FormComponent } from './form.component';

const routes: Routes = [
    {
        path: '',
        data: { title: 'Users'  },
        component: FormComponent
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}
