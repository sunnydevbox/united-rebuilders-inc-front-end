import { Component, OnInit } from '@angular/core';

import { settings } from './settings';

@Component({
  selector: 'app-civil-status',
  templateUrl: './civil-status.component.html',
  styleUrls: ['./civil-status.component.css']
})
export class CivilStatusComponent implements OnInit {

  public settings; 
  public _triggerReloadList;
  public _object;

  constructor() { 
    this.settings = settings;
  }

  triggerReloadList(e) {
    if (e) {
      this._triggerReloadList = e;
    }
  }

  getObject(object) {
    this._object = object;
  }

  ngOnInit() {
  }

}
