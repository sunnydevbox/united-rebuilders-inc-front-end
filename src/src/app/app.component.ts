import { Component, AfterViewInit, ViewContainerRef } from '@angular/core';

import { ToastsManager } from 'ng2-toastr';

// import { ConfirmComponent } from './shared/confirm/confirm.component';

import { Router, NavigationStart, NavigationCancel, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {

  public pageLoading = false;
  private viewContainerRef: ViewContainerRef;

  constructor(
    private router: Router,
    public toastr: ToastsManager,
    viewContainerRef: ViewContainerRef
  ) {
    this.viewContainerRef = viewContainerRef;
    this.toastr.setRootViewContainerRef(viewContainerRef);

  
      console.log('Mode web');
  }


  ngAfterViewInit() {
        this.router.events
            .subscribe((event) => {
                if (event instanceof NavigationStart) {
                    this.pageLoading = true;
                } else if (
                    event instanceof NavigationEnd ||
                    event instanceof NavigationCancel
                ) {
                  this.pageLoading = false;
                }
            });
    }
}
