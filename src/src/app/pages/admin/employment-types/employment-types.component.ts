import { Component, OnInit } from '@angular/core';

import { settings } from './settings';

@Component({
  selector: 'app-employment-types',
  templateUrl: './employment-types.component.html',
  styleUrls: ['./employment-types.component.css']
})
export class EmploymentTypesComponent implements OnInit {

  public settings; 
  public _triggerReloadList;
  public _object;

  constructor() { 
    this.settings = settings;
  }

  triggerReloadList(e) {
    if (e) {
      this._triggerReloadList = e;
    }
  }

  getObject(object) {
    this._object = object;
  }

  ngOnInit() {
  }

}
