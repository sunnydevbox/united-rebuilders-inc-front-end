import { Component, Output, EventEmitter, Input, SimpleChanges, OnChanges } from '@angular/core';

import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';

import { MODEL } from './../../model';
import { settings } from './../../settings';
import { ERRORS } from '@admin/config';
import { forEach } from '@angular/router/src/utils/collection';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent extends BaseFormComponent implements OnChanges {

  @Output() triggerReloadList = new EventEmitter();
  @Input() object;

  days = [
    {
      value: 'first_day',
      name: 'First Day',
    }, 
    {
      value: 'last_day',
      name: 'Last Day',
    }
  ];

  _postConstruct() {
    this.service = this.injector.get(settings.service);
    this.form = this.formBuilder.group(MODEL);
    // this.form.controls.month_days = this.formBuilder.array([]);
    this.errors = ERRORS;
  }

  _onSaveSuccess(res) {
    super._onSaveSuccess(res);
    this.triggerReloadList.emit(true);
    this.form.reset();
  }

  _onUpdateSuccess(res) {
    super._onUpdateSuccess(res);
    this.triggerReloadList.emit(true);
    this.form.reset();
  }

  cancel() {
    this.triggerReloadList.emit(true);
    this.form.reset();
    this.formMode = 'create';
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.object && changes.object.currentValue) {
      this.formMode = 'update';
      this.form.get('name').setValue(changes.object.currentValue.name);
      this.objectId = changes.object.currentValue.id;

      this.form.get('month_days').setValue(changes.object.currentValue.month_days.join(','));
    }
  }
}
