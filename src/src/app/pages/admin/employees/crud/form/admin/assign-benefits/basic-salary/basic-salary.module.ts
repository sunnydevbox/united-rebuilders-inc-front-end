import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@shared/forms.module';

/*****************************************/
/**** IMPORT THE BASE MODULE for FORM ****/
/*****************************************/
/***/
/***/ import { FormModule as BaseFormModule } from '@shared/base/form/form.module';
/***/
/** INSTRUCTED FROM BASE FORM MODULE */
/***/
/***/ import { OverlayModule } from '@shared/overlay/overlay.module';
/***/

import { BasicSalaryComponent } from './basic-salary.component';
@NgModule({
  imports: [
    CommonModule,
    OverlayModule,
    FormsModule,
    BaseFormModule,
  ],
  declarations: [
    BasicSalaryComponent,
  ],
  exports: [
    BasicSalaryComponent,
  ]
})
export class BasicSalaryModule { }
