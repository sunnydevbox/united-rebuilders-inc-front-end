import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { ListComponent as BaseListComponent } from '@shared/base/list/list.component' ;

import { settings } from './../../settings';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseListComponent {

  @Output() object = new EventEmitter();

  _postConstruct() {
    this.service = this.injector.get(settings.service);
  }

  edit(object) {
    this.object.emit(object);
  }

}
