import { Component, OnInit, Input, Output, Injector, EventEmitter } from '@angular/core';
import { Router, RouterLink } from '@angular/router'; 
import { CrudComponent} from './../crud.component';

@Component({
  selector: 'crud-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent extends CrudComponent {

  @Output() refreshList: EventEmitter<any> = new EventEmitter();
  @Input() settings: any;
  @Input() action: any;
  @Input() title?: string = '';

  constructor(injector:Injector) {
    super(injector);
  }

  doRefreshList() {
    this.refreshList.emit();
  }

}
