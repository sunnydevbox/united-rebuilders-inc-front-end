import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LeaveApplicationsComponent } from './leave-applications.component';
import { RoutingModule } from './routing.module';
import { LeaveApplicationService } from '@services/leave-application.service';
import { FormModule } from './crud/form/form.module';
import { ListModule } from './crud/list/list.module';
import { FiltersModule } from './filters/filters.module';

import { SupplementDataService } from '@services/supplement-data.service';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    FormModule,
    ListModule,
    FiltersModule,
  ],
  declarations: [
    LeaveApplicationsComponent,
  ],
  exports: [
    LeaveApplicationsComponent,
  ],
  providers: [
    LeaveApplicationService,
    SupplementDataService,
  ]
})
export class LeaveApplicationsModule { }
