import { Component, OnInit, Input, Injector, Inject, OnChanges, SimpleChanges, ElementRef } from '@angular/core';

import { ToastsManager } from 'ng2-toastr';

import { settings } from './../../settings';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnChanges {

  @Input() listPermissions;
  @Input() roleAttachedPermissions;
  @Input() roleId = null;


  public toggleOverlay = false;
  public settings;
  private service;

  constructor(
    private injector: Injector,
    private elref: ElementRef,
    private toastr: ToastsManager
  ) {
    this.settings = settings;
    // this.settings.service;
    this.service = this.injector.get(this.settings.service);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (typeof changes.roleAttachedPermissions !== 'undefined') {
      // REPOPULATE THE CHECKBOXES
      const checkboxes = this.elref.nativeElement.querySelectorAll('.checkbox-permission');
      for (const key in checkboxes) {
        if (checkboxes.hasOwnProperty(key)) {
          checkboxes[key].checked = this.isAttached(checkboxes[key].value);
        }
      }
    }
  }

  isAttached(permission) {
    for (const key in this.roleAttachedPermissions) {
      if (this.roleAttachedPermissions.hasOwnProperty(key)) {
        if (this.roleAttachedPermissions[key].name === permission) {
          return true;
        }
      }
    }

    return false;
  }

  clickedCheckbox(e) {

    if (this.roleId) {
      this.toggleOverlay = true;
      if (e.target.checked) {
        // Add to role
        this.service.attachPermission(this.roleId, e.target.value)
        .subscribe(
          (res) => {},
          (err) => {
            this.toastr.error('There was a problem processing your request. Please try again.');
          },
          () => {
            this.toggleOverlay = false;
          }
        );
      } else {
        // remove from role
        this.service.revokePermission(this.roleId, e.target.value)
        .subscribe(
          (res) => {},
          (err) => {
            this.toastr.error('There was a problem processing your request. Please try again.');
          },
          () => {
            this.toggleOverlay = false;
          }
        );
      }
    }
  }
}
