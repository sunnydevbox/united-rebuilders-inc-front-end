import { Component, Output, EventEmitter, Input, SimpleChanges, OnChanges } from '@angular/core';

import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';

import { MODEL } from './../../model';
import { settings } from './../../settings';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent extends BaseFormComponent implements OnChanges {
  @Output() triggerReloadList = new EventEmitter();
  @Input() object;

  _postConstruct() {
    this.service = this.injector.get(settings.service);
    this.form = this.formBuilder.group(MODEL);
  }

  _onSaveSuccess(res) {
    super._onSaveSuccess(res);

    // Trigger Relaod list
    this.triggerReloadList.emit(true);

    // Reset the form
    this.form.reset();
  }

  _onUpdateSuccess(res) {
    super._onUpdateSuccess(res);
    // Trigger Relaod list
    this.triggerReloadList.emit(true);

    // Reset the form
    this.objectId = null;
    this.form.reset();

    // REset back to create mode
    this.formMode = 'create';
  }

  ngOnChanges(changes: SimpleChanges) {
    // console.log(' BASELIST ', changes);
    if (changes.object && changes.object.currentValue) {
      this.formMode = 'update';
      this.form.get('name').setValue(changes.object.currentValue.name);
      this.form.get('description').setValue(changes.object.currentValue.description);
      this.objectId = changes.object.currentValue.id;
    }
  }
}
