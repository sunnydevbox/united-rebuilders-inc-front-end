import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@shared/forms.module';

import { FormModule as BaseFormModule } from '@shared/base/form/form.module';

/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { OverlayModule } from '@shared/overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */

import { AdminComponent } from './admin.component';
import { ManagePasswordModule } from './manage-password/manage-password.module';

@NgModule({
  imports: [
    CommonModule,
    OverlayModule,
    NgbModule,
    FormsModule,
    BaseFormModule,
    ManagePasswordModule,
  ],
  declarations: [
    AdminComponent,
  ],
  exports: [
    AdminComponent,
  ],
  providers: [

  ]
})
export class AdminModule { }
