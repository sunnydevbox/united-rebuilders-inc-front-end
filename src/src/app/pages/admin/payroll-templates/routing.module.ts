import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { Route } from './../../../core/route.service';

import { PayrollTemplatesComponent } from './payroll-templates.component';

const routes: Routes = [
    {
        path: '',
        //component: ListComponent,
        data: { title: 'Roles'  },
        children:[
            {
              path: '',
              component: PayrollTemplatesComponent,
            },
          ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}