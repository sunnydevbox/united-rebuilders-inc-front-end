import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class CompanyDepartmentsService extends AbstractService {
    endpoint = 'company-departments';
}
