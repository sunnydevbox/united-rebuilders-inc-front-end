import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { GenerateReportComponent } from './generate-report.component';

const routes: Routes = [
    {
        path: '',
        component: GenerateReportComponent,
        data: { title: 'Roles'  },
        // children:[
        //     {
        //       path: '',
        //       component: LandingComponent,
        //     },
        //     {
        //       path: 'payslip/:id',
        //       loadChildren: '@admin/payroll/payslip/payslip.module#PayslipModule',
        //       data: {
        //         formMode: 'update',
        //       },
        //     },
        //     {
        //       path: 'create',
        //       loadChildren: '@admin/payroll/crud/form/form.module#FormModule',
        //       data: {
        //         formMode: 'create',
        //       },
        //     },
        //   ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}