import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-popupsearch',
  templateUrl: './popupsearch.component.html',
  styleUrls: ['./popupsearch.component.scss']
})
export class PopupsearchComponent implements OnInit {

  public toggle: Boolean = false;

  public template = '';

  constructor() { }

  ngOnInit() {
  }

}
