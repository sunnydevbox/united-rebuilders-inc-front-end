import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of'; 
import { Credentials, LoginContext } from './authentication.service';

export class MockAuthenticationService {

  private userData = null;

  credentials: Credentials | null = {
    username: 'test',
    token: '123',
    email: 'asd',
  };

  login(context: LoginContext): Observable<Credentials> {
    return of({
      username: context.username,
      token: '123456',
      email: 'test@test.test',
    });
  }

  logout(): Observable<boolean> {
    this.credentials = null;
    return of(true);
  }

  isAuthenticated(): boolean {
    return !!this.credentials;
  }
}
