import { EmploymentStatusService } from '@services/employment-status.service';

export const settings = {
    plural: 'Employment Status',
    singular: 'Employment Status',
    service: EmploymentStatusService,    
  
    crud: {
      create: {
        allow: true, // true/false
        url: '/app/system/employment-status'
      },
      read:{
  
      },
      update:{
        allow: true,
        url: '/app/system/employment-status/edit/'
      },
      delete:{
        allow: true
      },
      list: {
        allow: true,
        url: '/app/system/employment-status'
      }
    }
  };