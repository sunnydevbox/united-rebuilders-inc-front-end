import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TimelogsComponent } from './timelogs.component';
import { TimelogService } from '@services/timelog.service';

/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { DataTableModule } from 'angular5-data-table';
import { OverlayModule } from '@shared/overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */


import { ListModule as BaseListModule } from '@shared/base/list/list.module' ;
import { StatusBadgeModule } from '@shared/status-badge/status-badge.module';


@NgModule({
  imports: [
    CommonModule,
    BaseListModule,
    DataTableModule,
    OverlayModule,
    NgbModule,
    StatusBadgeModule,
  ],
  declarations: [
    TimelogsComponent,
  ],
  exports: [
    TimelogsComponent,
  ],
  providers: [
    TimelogService,
  ]
})
export class TimelogsModule { }
