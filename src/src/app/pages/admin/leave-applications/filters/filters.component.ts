import { Component, OnInit, Output, EventEmitter, OnDestroy, Input } from '@angular/core';

import { FiltersComponent as BaseFiltersComponent } from '@shared/base/filters/filters.component';

import {Observable} from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { MODEL } from './model';
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import { datePickerOptions1 } from '@admin/config';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css']
})
export class FiltersComponent extends BaseFiltersComponent implements OnDestroy {

  private formControleName: Subscription;
  public datePickerOptions1 = datePickerOptions1;

  @Output() filtersChanged = new EventEmitter();
  @Output() searchChangeEmitter = new EventEmitter();
  @Input() suppData;

  _postConstruct() {
    this.model = MODEL;
    this.form = this.formBuilder.group(this.model);

    // Use debounceTime to add delay to this field
    this.formControleName = this.form.get('name').valueChanges
      .debounceTime(1000)
      .subscribe((newValue) => {
        if (newValue && newValue.length >= 2) {
          this.startFilter();
        }
      });
  }

  fieldProcessor(field) {
    super.fieldProcessor(field);

    if (field === 'start') {
      this.query.push('start|' + this.form.get(field).value);
    }
  }

  dateChanged(field, event: IMyDateModel) {
    if (field === 'start') {
      console.log(event.formatted);
      this.form.get(field).setValue(event.formatted);
      this.query.push('start|' + this.form.get(field).value);
    }

    this.startFilter();
  }

  ngOnDestroy() {
    this.formControleName.unsubscribe();
  }
}
