import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PayrollComponent } from './payroll.component';
import { RoutingModule } from './routing.module';
import { MyDatePickerModule } from 'mydatepicker';
import { ListModule as BaseListModule } from '@shared/base/list/list.module' ;
import { settings } from './settings';
import { FilterModule } from './crud/filter/filter.module';
import { ListModule } from './crud/list/list.module';

@NgModule({
  declarations: [
    PayrollComponent
  ],
  imports: [
    CommonModule,
    MyDatePickerModule,
    RoutingModule,
    FilterModule,
    ListModule,
  ],
  providers: [
    settings.service,
  ]
})
export class PayrollModule { }
