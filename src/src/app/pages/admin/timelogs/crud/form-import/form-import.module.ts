import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@shared/forms.module';
import { FormModule as BaseFormModule } from '@shared/base/form/form.module';
/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { OverlayModule } from '@shared/overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */

import { FormImportComponent } from './form-import.component';
import { TimelogService } from '@services/timelog.service';
import { ProgressHttpModule } from "angular-progress-http";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BaseFormModule,
    OverlayModule,
    NgbModule,
    ProgressHttpModule,
  ],
  declarations: [
    FormImportComponent,
  ],
  exports: [
    FormImportComponent,
  ],
  providers: [
    TimelogService,
  ]
})
export class FormImportModule { }
