import { TaxStatusService } from '@services/tax-status.service';

export const settings = {
    plural: 'Tax Status',
    singular: 'Tax Status',
    service: TaxStatusService,

    crud: {
      create: {
        allow: true, // true/false
        url: '/app/system/tax-status'
      },
      read: {

      },
      update: {
        allow: true,
        url: '/app/system/tax-status/edit/'
      },
      delete: {
        allow: true
      },
      list: {
        allow: true,
        url: '/app/system/tax-status'
      }
    }
  };
