import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RouterModule } from '@angular/router'; 

import { AdminComponent } from './admin.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';

import { UserService } from '@services/user.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    AdminComponent,
    HeaderComponent,
    SidebarComponent,
  ],
  exports: [
    AdminComponent,
  ],
  providers: [
    UserService,
  ]
})
export class AdminModule { }
