import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class RoleService extends AbstractService {

    endpoint = 'roles';

    attachPermission(roleId, permissionName) {
        return this.http.post(this.apiURL() + '/' + roleId  + '/attach-permission', {
            name: permissionName
        })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
    }

    revokePermission(roleId, permissionName) {
        return this.http.post(this.apiURL() + '/' + roleId  + '/revoke-permission', {
            name: permissionName
        })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
    }

    getPermissions(roleId) {
        return this.http.get(this.apiURL() + '/' + roleId  + '/get-permissions')
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
    }

}
