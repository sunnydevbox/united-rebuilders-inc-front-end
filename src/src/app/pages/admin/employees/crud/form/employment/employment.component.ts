import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

// IMPORT THE BASE COMPONENT
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';

// Others..
// import { settings } from './../../settings';

import { IMyDpOptions, IMyDateModel } from 'mydatepicker';

import { ERRORS } from './../../../model';
import { MODEL } from './model';
import { EmployeeService } from '@services/employee.service';
import { formatDateFix } from '@admin/config';

@Component({
  selector: 'app-employment',
  templateUrl: './employment.component.html',
  styleUrls: ['./employment.component.css']
})
export class EmploymentComponent extends BaseFormComponent implements OnChanges {

  @Input() lists;
  @Input() object;

  dateNow = new Date();
  public datePickerOptions: IMyDpOptions = {
    dateFormat: 'mmm dd, yyyy',
    editableDateField: false,
    openSelectorOnInputClick: true,
    sunHighlight: true,
    minYear: this.dateNow.getFullYear() - 90,
    maxYear: this.dateNow.getFullYear(),
    height: '25px',
    selectionTxtFontSize: '11px',
    showTodayBtn: false,
    // showClearDateBtn: false,
  };

  public isDisabled = false;

  _postConstruct() {
    this.service = this.injector.get(EmployeeService);
    this.form = this.formBuilder.group(MODEL);
    this.errors = ERRORS;
    /// this.objectId = this.object.id;
    this.formMode = 'update';
  }

  formatDateFix(v) {
    return formatDateFix(v);
  }

  selectedShiftTemplate(items) {

    if (items.id) {
      items = [items];
    }

    items.forEach( (item, index) => {
      if (item.selected) {
        this.service.attachShiftTemplate(this.object.id, item.id).subscribe(
          (res) => {
            this.toastr.success('Shift Template added');
          },
          (err) => {
            this.toastr.danger('There was a problem assigning Shift Template');
          }
        );
      } else {
        this.service.detachShiftTemplate(this.object.id, item.id).subscribe(
          (res) => {
            this.toastr.success('Shift Template removed');
          },
          (err) => {
            this.toastr.danger('There was a problem removing Shift Template');
          }
        );
      }
    });
  }


  selectedPayrollTemplate(items) {

    if (items.id) {
      items = [items];
    }

    items.forEach( (item, index) => {
      if (item.selected) {
        this.service.attachPayrollTemplate(this.object.id, item.id).subscribe(
          (res) => {
            this.toastr.success('Payroll Template added');
          },
          (err) => {
            this.toastr.danger('There was a problem assigning Payroll Template');
          }
        );
      } else {
        this.service.detachPayrollTemplate(this.object.id, item.id).subscribe(
          (res) => {
            this.toastr.success('Payroll Template removed');
          },
          (err) => {
            this.toastr.danger('There was a problem removing Payroll Template');
          }
        );
      }
    });
  }

  disableFields(selectedId) {
    if (selectedId && typeof selectedId == 'object') {
      selectedId = selectedId.target.value;
    }
    this.isDisabled = (selectedId && selectedId != 'null') ? false : true;

    let model = MODEL;
    for(let field in model) {
      if (field != 'employee_template_id' && this.isDisabled) {
        this.form.get(field).disable();
      } else {
        this.form.get(field).enable();
      }
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.object && changes.object.currentValue) {
      this.objectId = changes.object.currentValue.id;

      this.form.get('department_id').setValue(changes.object.currentValue.department_id);
      this.form.get('job_position_id').setValue(changes.object.currentValue.job_position_id);
      this.form.get('employment_status_id').setValue(changes.object.currentValue.employment_status_id);
      this.form.get('employment_type_id').setValue(changes.object.currentValue.employment_type_id);
      this.form.get('shift_template_id').setValue(changes.object.currentValue.shift_template_id);
      this.form.get('hourly_rate').setValue(changes.object.currentValue.hourly_rate);
      this.form.get('monthly_rate').setValue(changes.object.currentValue.monthly_rate);
      this.form.get('sss_number').setValue(changes.object.currentValue.sss_number);
      this.form.get('tin_number').setValue(changes.object.currentValue.tin_number);
      
      let template_id = changes.object.currentValue.employee_template_id ? changes.object.currentValue.employee_template_id : null;
      
      this.form.get('employee_template_id').setValue(template_id);

      // DATE HIRED
      if (changes.object.currentValue.date_hired) {
        const bd = new Date(formatDateFix(changes.object.currentValue.date_hired));
        changes.object.currentValue.date_hired = {
          date: {
            year: bd.getFullYear(),
            month: bd.getMonth() + 1,
            day:  bd.getDate(),
          }
        };
      }
      this.form.get('date_hired').setValue(changes.object.currentValue.date_hired);


      // DATE DISMISSED
      if (changes.object.currentValue.date_dismissed) {
        const bd = new Date(formatDateFix(changes.object.currentValue.date_dismissed));
        changes.object.currentValue.date_dismissed = {
          date: {
            year: bd.getFullYear(),
            month: bd.getMonth() + 1,
            day:  bd.getDate(),
          }
        };
      }
      this.form.get('date_dismissed').setValue(changes.object.currentValue.date_dismissed);
      this.isDisabled = template_id ? false : true;
      this.disableFields(template_id)
    }
  }


  _preSubmit() {

    if (this.form.get('date_hired').value) {
      this.form.get('date_hired').setValue(this.form.get('date_hired').value.formatted);
    }

    if (this.form.get('date_dismissed').value) {
      this.form.get('date_dismissed').setValue(this.form.get('date_dismissed').value.formatted);
    }
  }

}
