import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SssTableComponent } from './sss-table.component';
import { RoutingModule } from './routing.module';
import { ListModule } from './crud/list/list.module';
import { settings } from './settings';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    ListModule,
  ],
  declarations: [
    SssTableComponent,
  ],
  exports: [
    SssTableComponent,
  ],
  providers: [
    settings.service,
  ]
})
export class SssTableModule { }
