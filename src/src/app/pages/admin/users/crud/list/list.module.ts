import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoutingModule } from './routing.module';
import { ListComponent } from './list.component';

import { settings } from './../../settings';

/*****************************************/
/**** IMPORT THE BASE MODULE for LIST ****/
/*****************************************/
/***/
/***/ import { ListModule as BaseListModule } from '@shared/base/list/list.module';
/***/
/** INSTRUCTED FROM BASE LIST MODULE */
/***/
/***/ import { DataTableModule } from 'angular5-data-table';
/***/ import { OverlayModule } from '@shared/overlay/overlay.module';
/***/
/*****************************************/

import { StatusBadgeModule } from '@shared/status-badge/status-badge.module';

@NgModule({
  imports: [
    CommonModule,
    OverlayModule,
    DataTableModule,
    RoutingModule,
    BaseListModule,
    StatusBadgeModule
  ],
  declarations: [
    ListComponent,
  ],
  providers: [
    settings.service
  ]
})
export class ListModule { }
