import { Component, Inject, Injector } from '@angular/core';
import { Router, UrlSegment, ActivatedRoute, PRIMARY_OUTLET } from '@angular/router';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/observable/throw';
// import 'rxjs/add/operator/startWith';
// import 'rxjs/add/observable/merge';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/debounceTime';
// import 'rxjs/add/operator/distinctUntilChanged';
// import 'rxjs/add/observable/fromEvent';
// import 'rxjs/add/observable/of';

import { CrudComponent } from './../crud.component';


import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import { ModalConfirmComponent } from './../../../modal/modal-confirm.component';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends CrudComponent implements ListComponentInterface {
  // public modalComponent = ModalConfirmComponent;
  // public modalOptions: any;
  // public modalActive;
  public service: any;
  public modalService: any;
  public rows: Array<any> = [];
  public page = {
    size: 20, // The number of elements in the page
    totalElements: 0, // The total number of elements
    totalPages: 0, // The total number of pages
    pageNumber: 0, // The current page number
  };

  public filterQuery = {
    'orderBy' : 'created_at',
    'sortedBy' : 'desc',
  }

  public queryState = {};

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    this.getList()
  }

  getList() {
    this.loadingStatus(true);
    this.service.index(this.filterQuery)
    .subscribe(
      (result: any) => {
          this.rows = result['data'];
          this.setPager(result['meta']['pagination']);
          this.loadingStatus(false);
      },
      (error: any) => {
        console.log('ERROR', error);
      },
    );
  }

  setPager(pageInfo: any) {
    this.page.size          = pageInfo['per_page'];
    this.page.totalElements = pageInfo['total'];
    this.page.totalPages    = pageInfo['total_pages'];
    this.page.pageNumber    = pageInfo['current_page'] - 1;
  }

  onDeleteRow(row: any): void {
    // this.modalActive = this.modalService.open(this.modalComponent, { size: 'sm', container: 'nb-layout' });
    // this.modalActive.componentInstance.modalHeader = 'Delete';
    // this.modalActive.componentInstance.modalContent = 'Are you sure you want to delete this record?'

    this.modalCallbacks(row);
  }

  modalCallbacks(obj: any) {}

  getEditLink(row: any) {
    return 'OVERRIDE THIS';
  }

  handlePager(e: any) {
    console.log(e)

    this.filterQuery['page'] = e.offset;
    this.filterQuery['take'] = e.limit;

    this.getList();
  }

  handleSort(e: any) {
    console.log(e.sorts)

    this.filterQuery['orderBy'] = e.sorts[0].prop;
    this.filterQuery['sortedBy'] = e.sorts[0].dir;

    this.getList();
  }


  refreshList() {
    this.getList();
  }

}

export interface ListComponentInterface {
  //modalComponent;
  service: any;
  rows: any;
  page: any;
  // Deinfe the logic to pull the list to be displayed in the table

  getEditLink: Function,
  refreshList: Function
}