import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BiofieldComponent } from './biofield.component';
import { FormsModule } from '@shared/forms.module';

/*****************************************/
/**** IMPORT THE BASE MODULE for FORM ****/
/*****************************************/
/***/
/***/ import { FormModule as BaseFormModule } from '@shared/base/form/form.module';
/***/
/** INSTRUCTED FROM BASE FORM MODULE */
/***/
/***/ import { OverlayModule } from '@shared/overlay/overlay.module';
/***/

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BaseFormModule,
    OverlayModule
  ],
  declarations: [
    BiofieldComponent,
  ],
  exports: [
    BiofieldComponent,
  ]
})
export class BiofieldModule { }
