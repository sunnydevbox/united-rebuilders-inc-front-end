import { Component, OnInit } from '@angular/core';

import { settings } from './settings';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  public settings;

  constructor() {
    this.settings = settings;
  }

  ngOnInit() {
    
  }

}
