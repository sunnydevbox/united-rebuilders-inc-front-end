import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';

// IMPORTT HE BASE COMPONENT
import { ListComponent as BaseListComponent } from '@shared/base/list/list.component';

// Others..
import { settings } from './../../settings';
import { SupplementDataService } from '@services/supplement-data.service';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseListComponent implements OnChanges {

  public settings;
  private suppdataService;
  public suppData = {
    departments: [],
    positions: [],
    status: [],
    employmentTypes: [],
  };

  _postConstruct() {
    this.settings = settings;
    this.service = this.injector.get(settings.service);
    this.suppdataService = this.injector.get(SupplementDataService);

    this.suppdataService.index({
      data: 'company-departments;job-positions;employment-status;employment-types;employee-templates'
    }).subscribe(
      (res) => {
        this.suppData.departments = res['company-departments'];
        this.suppData.positions = res['job-positions'];
        this.suppData.status = res['employment-status'];
        this.suppData.employmentTypes = res['employment-types'];
      }
    );

    this.defaultParams = {
      filter: 'id;employee_id_number;email;first_name;last_name;'
              + 'department_id;job_position_id;employment_status_id;'
              + 'employment_type_id;employee_template_id',
      with: 'department;job_position;status;employment_type;employee_template',
    };
  }

  filtersChanged(changed) {
    // this.filters = changed;
    this.defaultParams['query'] = changed.query;
    this.reloadItems(this.defaultParams);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.suppData && changes.suppData.currentValue) {
      console.log(changes.suppData.currentValue);
    }
  }
}
