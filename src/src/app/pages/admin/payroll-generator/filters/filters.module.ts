import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@shared/forms.module';
import { FiltersModule as BaseFiltersModule } from '@shared/base/filters/filters.module';
import { FiltersComponent } from './filters.component';
import { MyDatePickerModule } from 'mydatepicker';
import { PayrollTemplateService } from '@services/payroll-template.service';
import { SupplementDataService } from '@services/supplement-data.service'; 

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BaseFiltersModule,
    MyDatePickerModule,
  ],
  declarations: [
    FiltersComponent
  ],
  exports: [
    FiltersComponent,
  ],
  providers: [
    SupplementDataService,
    PayrollTemplateService,
  ]
})
export class FiltersModule { }
