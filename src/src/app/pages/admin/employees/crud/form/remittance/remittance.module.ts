import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RemittanceComponent } from './remittance.component';
import { ListModule } from './crud/list/list.module';
import { FormModule } from './crud/form/form.module';

import { EmployeeDeductionsService } from '@services/employee-deductions.service';
import { DeductionService } from '@services/deductions.service';
import { EmployeeService } from '@services/employee.service';

@NgModule({
  imports: [
    CommonModule,

    ListModule,
    FormModule,


  ],
  declarations: [
    RemittanceComponent
  ],
  exports: [
    RemittanceComponent,
  ],
  providers: [
    EmployeeDeductionsService,
    EmployeeService,
    DeductionService,
  ]
})
export class RemittanceModule { }
