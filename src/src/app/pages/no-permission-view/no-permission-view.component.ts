import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-permission-view',
  template: `
    <div>
      <h1>Access Denied</h1>
      <p>Sorry. You do not have enough permission to access this page.</p>
    </div>
  `,
  styleUrls: ['./no-permission-view.component.scss']
})
export class NoPermissionViewComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
