import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UpdateFormComponent} from './update-form.component';
import { AmazingTimePickerModule } from 'amazing-time-picker';
import { MyDatePickerModule } from 'mydatepicker';
import { FormsModule } from '@shared/forms.module';
import { FormModule as BaseFormModule } from '@shared/base/form/form.module';

/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { OverlayModule } from '@shared/overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */

@NgModule({
  imports: [
    CommonModule,
    AmazingTimePickerModule,
    MyDatePickerModule,
    FormsModule,
    BaseFormModule,
    OverlayModule,
    NgbModule,
  ],
  declarations: [
    UpdateFormComponent,
  ],
  exports: [
    UpdateFormComponent,
  ]
})
export class UpdateFormModule { }
