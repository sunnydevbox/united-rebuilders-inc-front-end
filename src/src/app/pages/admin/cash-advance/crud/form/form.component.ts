import { Component } from '@angular/core';
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';
import { MODEL } from './../../model';
import { settings } from './../../settings';
import { datePickerOptions1 } from '@admin/config';
import { EmployeeService } from '@services/employee.service';
import { formatDateFix } from '@admin/config';
import { ModalService } from '@shared/modal/modal.service';
import { NotesFormComponent } from './notes-form/notes-form.component';
import { NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { CashAdvanceLogService } from '@services/cash-advance-log.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent extends BaseFormComponent {

  public datePickerOptions;
  public isDisabled = false;
  public employeeService;
  public employees = [];
  public object = null;
  public numberOfPeriods:any = 'N/A';
  public durationStartObject;
  public durationEndObject;
  public statusChanging = false;
  public logs = [];
  private popupFormModalService;
  private cashAdvanceLogService;

  _postConstruct() {
    this.cashAdvanceLogService = this.injector.get(CashAdvanceLogService);
    this.popupFormModalService = this.injector.get(ModalService);
    this.employeeService = this.injector.get(EmployeeService);
    this.datePickerOptions = datePickerOptions1;
    this.service = this.injector.get(settings.service);
    this.form = this.formBuilder.group(MODEL);

    this.listErrors = {
      'validation.gte': 'Total Amount must be greater than or equal to Payable per payday',
    };
  }
  
  ngOnInit() {
    this.formMode = this.route.snapshot.data['formMode'];
  
    if (this.formMode == 'create') {
      this.getEmployees();
    } else {
      this.objectId = this.route.snapshot.params['id'];
      this.getCashAdvanceDetails(this.objectId);
    }
  }

  getCashAdvanceDetails(objectId) {
    this.toggleOverlay = true;
    this.service.show(objectId, {
      with: 'employee'
    }).subscribe(
      (res) => {
        this.object = res.data;
        this.toggleOverlay = false;

        this.form.get('employee_id').setValue(this.object.employee_id);
        this.form.get('total_amount').setValue(this.object.total_amount);
        this.form.get('payable_per_period').setValue(this.object.payable_per_period);
        this.form.get('notes').setValue(this.object.notes);
          
        if (this.object.duration_start) {
          const bd = new Date(formatDateFix(this.object.duration_start.date));
          this.object.duration_start = {
            date: {
              year: bd.getUTCFullYear(),
              month: bd.getUTCMonth() + 1,
              day:  bd.getUTCDate(),
            }
          };
          this.form.get('duration_start').setValue(this.object.duration_start);
        }

        if (this.object.duration_end) {
          const bd = new Date(formatDateFix(this.object.duration_end.date));
          this.object.duration_end = {
            date: {
              year: bd.getUTCFullYear(),
              month: bd.getUTCMonth() + 1,
              day:  bd.getUTCDate(),
            }
          };
          this.form.get('duration_end').setValue(this.object.duration_end);
        }

        this.countPaydays();
      },
      (err) => {
        this.toggleOverlay = false;
      }
    );

    this.cashAdvanceLogService.index({
      search: 'cash_advance_id:' + this.objectId,
      sortedBy: 'desc',
      orderBy: 'created_at',
      limit: '0'
    }).subscribe(
      (res) => {
        this.logs = res.data;
      }
    );
  }

  getEmployees() {
    this.toggleOverlay = true;
    this.employeeService.index({
      filter: 'id;first_name;last_name',
      limit: 0,
    }).subscribe(
      (res) => {
        this.employees = res.data;
        this.toggleOverlay = false;
      },
      (err) => {
        this.toggleOverlay = false;
      }
    );
  }

  countPaydays() {
    let total_amount = this.form.get('total_amount').value;
    let payable = this.form.get('payable_per_period').value
    
    this.numberOfPeriods = 'N/A';
    if (total_amount && payable) {
      if (total_amount >= payable) {
        this.numberOfPeriods = (total_amount / payable).toFixed(2);
        this.numberOfPeriods += ' payday' + ((this.numberOfPeriods > 1) ? 's': '');
      }
    }
    
  }

  formatDateFix(v) {
    return formatDateFix(v);
  }

  _preSubmit() {

    this.durationStartObject = this.form.get('duration_start').value;
    this.durationEndObject = this.form.get('duration_end').value;

    this.form.get('duration_start').setValue(
      formatDateFix(this.form.get('duration_start').value.date.year + '-' +
          this.form.get('duration_start').value.date.month + '-' +
          this.form.get('duration_start').value.date.day + ' 00:00:00'
      )
    );

    this.form.get('duration_end').setValue(
      formatDateFix(this.form.get('duration_end').value.date.year + '-' +
        this.form.get('duration_end').value.date.month + '-' +
        this.form.get('duration_end').value.date.day + ' 23:59:59'
      )
    );

  }

  _onSaveSuccess(res) {
    super._onSaveSuccess(res);

    this.numberOfPeriods = 'N/A';

    this.listUrl = settings.crud.list.url;

    // Reset the form
    this.form.reset();
    this.redirectToList();
  }

  _onSaveError(err) {

    super._onSaveError(err);
    this.formSubmitting = false;
    this.toggleOverlay = false;
  }

  _onUpdateSuccess(res) {
    this.skipRedirect(true);  
    super._onUpdateSuccess(res);
  }

  _onSaveComplete() {
    super._onSaveComplete();

    this.form.get('duration_start').setValue(this.durationStartObject);
    this.form.get('duration_end').setValue(this.durationEndObject);
  }

  approve() {
    this.statusChanging = true;

    let modalOptions: NgbModalOptions = {
      size: 'sm',
    }

    let modalSettings = {
      data: {},
      template: NotesFormComponent,
      title: 'Add Notes',
    }

    let callback = (modal, notes) => {
      return this.service.setStatus({
        id: this.objectId,
        status: 'approved',
        notes: notes,
      }).subscribe(
        (res) => {  
          modal.componentInstance.activeModal.dismiss('close');
          this.router.navigate([settings.crud.list.url]);
        },  
        (err) => {
          console.log(err)
        },
        () => {
          this.statusChanging = false;
        }
      )
    };

    this.triggerModal(modalOptions, modalSettings, callback);
  }

  triggerModal(modalOptions, modalSettings = {}, callback) {
    this.popupFormModalService.setComponent(NotesFormComponent);

    let m = this.popupFormModalService.open(modalOptions, modalSettings);

    this.emitterHandler(m, callback);
  }

  emitterHandler(m, callback) {
    let instance = m.componentInstance.componentRef.instance;

    if (!!instance.notesChanged) {
      instance.notesChanged.subscribe((res) => {
        callback(m, res);
        
        // this.getPayslip(this.objectId);
        // m.componentInstance.activeModal.dismiss('close');
      });

      if (!!instance.cancelModalChanged) {
        instance.cancelModalChanged.subscribe((res) => {
          this.statusChanging = false;
          m.componentInstance.activeModal.dismiss('close');
        });
      }
    }
  }

  disapprove() {
    this.statusChanging = true;

    let modalOptions: NgbModalOptions = {
      size: 'sm',
    }

    let modalSettings = {
      data: {},
      template: NotesFormComponent,
      title: 'Add Notes',
    }

    let callback = (modal, notes) => {
      return this.service.setStatus({
        id: this.objectId,
        status: 'disapproved',
        notes: notes,
      }).subscribe(
        (res) => {  
          modal.componentInstance.activeModal.dismiss('close');
          this.router.navigate([settings.crud.list.url]);
        },  
        (err) => {
          console.log(err)
        },
        () => {
          this.statusChanging = false;
        }
      )
    };

    this.triggerModal(modalOptions, modalSettings, callback);
  }

  close() {
    this.statusChanging = true;

    let modalOptions: NgbModalOptions = {
      size: 'sm',
    }

    let modalSettings = {
      data: {},
      template: NotesFormComponent,
      title: 'Add Notes',
    }

    let callback = (modal, notes) => {
      return this.service.setStatus({
        id: this.objectId,
        status: 'closed',
        notes: notes,
      }).subscribe(
        (res) => {  
          modal.componentInstance.activeModal.dismiss('close');
          this.router.navigate([settings.crud.list.url]);
        },  
        (err) => {
          console.log(err)
        },
        () => {
          this.statusChanging = false;
        }
      )
    };

    this.triggerModal(modalOptions, modalSettings, callback);
  }
}
