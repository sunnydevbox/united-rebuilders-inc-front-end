import { Component, OnInit, Injector } from '@angular/core';
import { Router, UrlSegment, ActivatedRoute, PRIMARY_OUTLET } from '@angular/router';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';


@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.scss']
})
export class CrudComponent implements OnInit, BaseCrudInterace {
  
  public objectId: any;
  public settings: any;
  public router: Router;
  public activatedRoute: ActivatedRoute;

  public displayedColumns: any = [];
  public service: any;

  public loading: Boolean = false;

  constructor(private injector: Injector) {
    this.activatedRoute = injector.get(ActivatedRoute);
    this.router = injector.get(Router);
  }

  ngOnInit() {}

  ngOnDestroy() {
    console.log('component destroyed')
    //this.activatedRoute.unsubscribe();
  }

  getLoadingStatus() {
      return this.loading;
  }

  loadingStatus(status?: any) {
    if (status) {
      this.loading = status;
    } else {
      this.loading = !this.loading;
    }
  }

  setSettings(settings: any) {
    this.settings = settings;
  }

  /** DEPRECATE */
  _updateModel(data: any, formService: any, formModel: any) {
      var keys = Object.keys(data);
      for(let i=0; i<keys.length; i++) {
          let key = keys[i];
          //let input = formService.findById(key, formModel) as DynamicInputModel;
      }
  }

}

export interface BaseCrudInterace {
  //jsScripts: Array<any>;
}