import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';

import { environment } from './../../environments/environment';

@Injectable()
export class HttpStatusInterceptor implements HttpInterceptor {

  private router: Router;

  constructor(injector: Injector) {
    this.router = injector.get(Router);
  }

  intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req).do(evt => {
      // console.log(req)
      // console.log(next)
      // console.log(evt, evt instanceof HttpResponse)
      if (evt instanceof HttpResponse) {
        // console.log('---> status:', evt.status);
        // console.log('---> filter:', req.params.get('filter'));
      }
    }, (error: any) => {
      if (error instanceof HttpErrorResponse) {

        if (error.status === 401 || (error.status === 500
          && error.error.message === 'Token has expired'
        )) {

          // CLEAR STORAGE
          sessionStorage.removeItem('credentials');
          localStorage.removeItem('credentials');

          if (this.router.url !== '/login') {
            // window.location.href = environment.BASE_URL + 'login' + '?' + new Date().getMilliseconds();
            // window.location.href = environment.BASE_URL ;//+ 'login' + '?' + new Date().getMilliseconds();
            // this.router.navigateByUrl('login');

            // window.location.hash = 'login';
            window.location.href = '/login';
            // window.location.reload();
          }

          return Observable.throw(next);
          // return next.handle(req);
        } else {
          return Observable.throw(error);
        }

      }
    });

  }
}
