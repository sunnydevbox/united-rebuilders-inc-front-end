import { Component, OnInit, Input, SimpleChanges, OnChanges } from '@angular/core';

// IMPORTT HE BASE COMPONENT
import { ListComponent as BaseListComponent } from '@shared/base/list/list.component';

// Others..
import { settings } from './../../settings';
import { SupplementDataService } from '@services/supplement-data.service';
import { formatDateFix } from '@admin/config';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseListComponent implements OnChanges {

  public settings;
  private suppdataService;
  public suppData = {
    departments: [],
    positions: [],
    status: [],
    employmentTypes: [],
  };
  public id;

  @Input() object;


  _postConstruct() {
    this.loadListOnInit = false;
    this.settings = settings;
    this.service = this.injector.get(settings.service);
    this.suppdataService = this.injector.get(SupplementDataService);
    this.defaultParams = {
      filter: 'id;employee_id;name;department;status;payroll_log_id;total_gross;total_net;total_deduction;total_adjustments',
      with: 'payroll_log',
      
    };
  }

  formatDateFix(v) {
    return formatDateFix(v);
  }

  getPayslip(action, object) {
    const sta = this.confirmModalService.open({
      title: 'Payslip',
      body: 'Your payslip will be generate and it will automatically open in a new window shortly.',
      ctaPositive: 'Continue',
    });

    sta.result.then((result) => {
      if (result) {
        this.service.getPayslip(action, object.id).subscribe(
          (res) => {
            if (action == 'view') {
              this.toastr.success('Your payslip will now open.');
              const fileUrl = URL.createObjectURL(res);
              window.open(fileUrl, '_blank');
            } else {
              // var link = document.createElement('a');
              // link.href = 'the/url';
              // link.download = 'file.pdf';
              // link.dispatchEvent(new MouseEvent('click'));
              // saveas
            }
          },
          (err) => {
            this.toastr.danger('There was a problem generating your payslip.');
          }
        );
      
      }
    }, (reason) => {
      console.log(reason);
    });


    
  }

  filtersChanged(changed) {
    // this.filters = changed;
    this.defaultParams['query'] = changed.query;
    //this.reloadItems(this.defaultParams);
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.suppData && changes.suppData.currentValue) {
      console.log(changes.suppData.currentValue);
    }

    if (changes.object && changes.object.currentValue) {
      this.id = changes.object.currentValue.id;
      this.defaultParams['search'] = 'employee_id:' + this.id
    }
  }
}
