import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';

// IMPORT THE BASE COMPONENT
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';
import { EmployeeService } from '@services/employee.service';

@Component({
  selector: 'app-assign-benefits',
  templateUrl: './assign-benefits.component.html',
  styleUrls: ['./assign-benefits.component.css']
})
export class AssignBenefitsComponent extends BaseFormComponent {

  @Input() lists;
  @Input() object;
  @Output() objectChanged = new EventEmitter();

  _postConstruct() {
    this.service = this.injector.get(EmployeeService);
  }

  assignSelection(items) {

    if (items.id) {
      items = [items];
    }

    items.forEach( (item, index) => {
      if (item.selected) {
        this.service.attachBenefit(this.object.id, item.id).subscribe(
          (res) => {
            this.toastr.success('Benefit added');
          },
          (err) => {
            this.toastr.danger('There was a problem assigning Benefit');
          }
        );
      } else {
        this.service.detachBenefit(this.object.id, item.id).subscribe(
          (res) => {
            this.toastr.success('Benefit removed');
          },
          (err) => {
            this.toastr.danger('There was a problem removing Benefit');
          }
        );
      }
    });
  }
}
