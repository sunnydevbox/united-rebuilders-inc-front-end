import { Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit {

  @Input() title;
  @Input() body;
  @Input() ctaPositive: any = 'Yes';
  @Input() ctaNegative: any = 'Cancel';
  @Input() footerEnabled: boolean = false;
  @ViewChild('componentContainer', { read: ViewContainerRef }) entry: ViewContainerRef;
  
  public componentRef: any;
  
  constructor(
    private resolver: ComponentFactoryResolver,
    public activeModal: NgbActiveModal
  ) { }

  createComponent(component, data) {
    this.entry.clear();
    const factory = this.resolver.resolveComponentFactory(component);
    let comp = this.componentRef = this.entry.createComponent(factory);
    

    if (data) {
      for (var prop in data) {
        this.componentRef.instance[prop] = data[prop];
      }
    }
  }

  close = (): void => {
    // do cleanup stuff..
    this.entry.clear();
  }
  
  destroyComponent() {
    this.componentRef.destroy();
}

  ngOnInit() {
  }

}
