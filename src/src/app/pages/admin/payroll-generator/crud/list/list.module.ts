import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { DataTableModule } from 'angular5-data-table';
import { OverlayModule } from '@shared/overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */


import { ListModule as BaseListModule } from '@shared/base/list/list.module' ;
import { ListComponent } from './list.component';
import { StatusBadgeModule } from '@shared/status-badge/status-badge.module';
import { PayrollLogService } from '@services/payroll-log.service';

@NgModule({
  imports: [
    CommonModule,
    BaseListModule,
    DataTableModule,
    OverlayModule,
    NgbModule,
    StatusBadgeModule,
  ],
  declarations: [
    ListComponent,
  ],
  exports: [
    ListComponent
  ],
  providers: [
    PayrollLogService,
  ]
})
export class ListModule { }
