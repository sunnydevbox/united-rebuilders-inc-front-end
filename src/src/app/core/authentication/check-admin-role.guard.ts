import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { Logger } from '../logger.service';
import { UserService } from '@services/user.service';

const log = new Logger('CheckRoleGuard');

@Injectable()
export class CheckAdminRoleGuard implements CanActivate {
  
  constructor(
    private router:Router,
    private userService: UserService
  ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    
      console.log('is Admin? ', this.userService.checkUserRole('Admin'));
    if (this.userService.checkUserRole('Admin')) {
      return true;
    } else {
      log.debug('You are not ADMIN, redirecting to "me"...');
      this.router.navigate(['/me'], { replaceUrl: true });
      return false;
    }
  }
}
