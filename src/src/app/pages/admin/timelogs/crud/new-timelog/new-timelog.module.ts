import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewTimelogComponent } from './new-timelog.component';
import { settings } from './settings'
import { FormsModule } from '@shared/forms.module';
import { FormModule as BaseFormModule } from '@shared/base/form/form.module';
import { MyDatePickerModule } from 'mydatepicker';
import { EmployeeService } from '@services/employee.service';
/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { OverlayModule } from '@shared/overlay/overlay.module';
import { NgbModule, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BaseFormModule,
    OverlayModule,
    NgbModule,
    MyDatePickerModule,
  ],
  declarations: [
    NewTimelogComponent,
  ],
  exports: [
    NewTimelogComponent,
  ],
  providers: [
    settings.service,
    EmployeeService,
  ]
})
export class NewTimelogModule { }
