import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class SSSService extends AbstractService {
    endpoint = 'sss-deduction';
}
