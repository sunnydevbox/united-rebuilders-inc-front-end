import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PayslipComponent } from './payslip.component';


const routes: Routes = [
    {
        path: '',
        component: PayslipComponent,
        data: { title: 'Roles'  },
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}