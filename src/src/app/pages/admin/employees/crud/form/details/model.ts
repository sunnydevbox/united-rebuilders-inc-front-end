import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
  'id' : [null],

  'type': ['personal'],

  'employee_id_number' : [null, Validators.compose([Validators.minLength(2), Validators.maxLength(30)])],
  'first_name' : [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(30)])],
  'last_name' : [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(30)])],
  'email' : [null, Validators.compose([Validators.pattern('[^ @]*@[^ @]*')])],
  'title_id' : [null, Validators.compose([])],


  'gender' : [null, Validators.compose([])],
  'birthdate' : [null, Validators.compose([])],
  'civil_status_id' : [null, Validators.compose([])],
  'nationality' : [null, Validators.compose([])],
  'religion' : [null, Validators.compose([])],

  'landline' : [null, Validators.compose([])],
  'mobile' : [null, Validators.compose([])],

  'address_1' : [null, Validators.compose([])],
  'address_2' : [null, Validators.compose([])],
  'city' : [null, Validators.compose([])],
  'province' : [null, Validators.compose([])],
  'zipcode' : [null, Validators.compose([])],
  'country' : [null, Validators.compose([])],

};

export const ERRORS = [
  { name: 'required', text: 'This field is required', rules: ['touched'] },
  { name: 'minlength', text: 'Minimum number of characters ', rules: ['touched'] },
  { name: 'maxlength', text: 'Maximum number of characters ', rules: ['touched'] }
];
