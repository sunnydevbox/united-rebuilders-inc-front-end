import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

import { ListComponent as BaseListComponent } from '@shared/base/list/list.component' ;

// import { settings } from './../../settings';
import { EmployeeLogsService } from '@services/employee-logs.service';


@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css']
})
export class LogsComponent extends BaseListComponent implements OnChanges {

  public objectId;
  @Input() object;
  // @Output() object = new EventEmitter();

  _postConstruct() {
    // this.service = this.injector.get(settings.service);
    this.service = this.injector.get(EmployeeLogsService);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.object && changes.object.currentValue) {
      this.defaultParams = {
        search: changes.object.currentValue.id,
        searchFilters: 'employee_id',
        orderBy: 'created_at',
        sortedBy: 'desc',
        // with: 'actorable',
      };
    }
  }

}
