import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@shared/forms.module';
import { FiltersModule as BaseFiltersModule } from '@shared/base/filters/filters.module';

import { FiltersComponent } from './filters.component';
import { MyDatePickerModule } from 'mydatepicker';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BaseFiltersModule,
    MyDatePickerModule,
  ],
  declarations: [
    FiltersComponent
  ],
  exports: [
    FiltersComponent,
  ]
})
export class FiltersModule { }
