import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PayslipsComponent } from './payslips.component';
import { ListModule } from './crud/list/list.module';

@NgModule({
  imports: [
    CommonModule,
    ListModule,
  ],
  declarations: [
    PayslipsComponent,
  ],
  exports: [
    PayslipsComponent,
  ]
})
export class PayslipsModule { }
