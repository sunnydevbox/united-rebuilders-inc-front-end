import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { DataTableModule } from 'angular5-data-table';
import { OverlayModule } from '@shared/overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */
import { ListModule as BaseListModule } from '@shared/base/list/list.module' ;
import { ListComponent } from './list.component';
import { TimelogService } from '@services/timelog.service';
import { StatusBadgeModule } from '@shared/status-badge/status-badge.module';
import { UserService } from '@services/user.service';
import { EmployeeService } from '@services/employee.service';
import { RouterModule } from '@angular/router';
import { FiltersModule } from './../filters/filters.module';
import { ShiftTemplatePeriodService } from '@services/shift-template-period.service';
import { UpdateFormModule } from './../update-form/update-form.module';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    OverlayModule,
    NgbModule,
    BaseListModule,
    StatusBadgeModule,
    RouterModule,
    FiltersModule,
    UpdateFormModule,
  ],
  declarations: [
    ListComponent,
  ],
  exports: [
    ListComponent,
  ],
  providers: [
    TimelogService,
    UserService,
    EmployeeService,
    ShiftTemplatePeriodService,
  ]
})
export class ListModule { }
