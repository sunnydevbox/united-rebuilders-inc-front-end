import { Component, OnInit } from '@angular/core';

import { settings } from './../settings';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  public settings;

  public _object = null;
  public _triggerReloadList = null;
  public filters = null;

  constructor() {
    this.settings = settings;
  }

  getObject(object) {
    this._object = object;
  }

  filtersChanged(changed) {
    this.filters = changed;
  }

  triggerReloadList(event) {
    this._triggerReloadList = true;
    console.log('_triggerReloadList', this._triggerReloadList);
  }

  ngOnInit() {
  }

}
