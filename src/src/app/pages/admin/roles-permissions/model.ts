
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
    'name' : [null, Validators.compose([Validators.required, Validators.minLength(2), Validators.maxLength(20)])],
  }

export const ERRORS = [
  { name: 'required', text: 'This field is required', rules: ['touched', 'dirty'] },
];