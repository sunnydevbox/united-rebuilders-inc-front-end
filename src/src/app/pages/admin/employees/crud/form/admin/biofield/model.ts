import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
  'type': ['biofield-id'],
  'bio_id' : [null, Validators.compose([Validators.minLength(1), Validators.maxLength(30)])],
};


