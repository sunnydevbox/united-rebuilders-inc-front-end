import { Component, OnInit, Injector, Output, EventEmitter } from '@angular/core';

import { Router, ActivatedRoute } from '@angular/router';

import { ConfirmService } from './../../confirm/confirm.service';
import { ToastsManager } from 'ng2-toastr';
import { ErrorMessageService } from '@services/errors-message.service';

import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  // VARIABLES REQUIRED FOR THIS COMPONENT
  private _skipRedirect = false;
  public confirmModalService;
  public toastr;
  private vcr;
  public toggleOverlay: Boolean = false;
  public formBuilder: FormBuilder;
  public service;
  public form: FormGroup;
  public formSubmitting: Boolean = false;
  public resetFormOnSuccess: Boolean = true;
  public errors; // List of error formats
  public route;
  public router;
  public listUrl: any = false;
  public formMode = 'create';
  public objectId;
  public errorMessageService;
  public listErrors = {};

  /*
   * This is a flag for a form saved.
   * This must be reset by the component using this.
  */
  // @Input()
  @Output() formWasSaved = new EventEmitter<Boolean>();

  constructor(
    public injector: Injector
  ) {
      this.formBuilder = injector.get(FormBuilder);
      this.confirmModalService = injector.get(ConfirmService);
      this.toastr = injector.get(ToastsManager);
      this.route = this.injector.get(ActivatedRoute);
      this.router = this.injector.get(Router);
      this.errorMessageService = this.injector.get(ErrorMessageService);
      this._postConstruct();
  }

  // OVERRIDE THIS.
  // PUT IN HERE any classes that need
  // instantiation such as services
  _postConstruct() {
    // INITIALIZE THE FORM/S HERE
    // ex:
    // this.form1 = this.formBuilder.group(MODEL);
    // this.errors = ERRORS;
  }

  submit() {
    // Overlay
    this.toggleOverlay = true;
    this.formSubmitting = true;

    this._preSubmit();

    if (this.formMode === 'create') {
      return this._submitSave();
    } else {
      return this._submitUpdate();
    }
  }

  _preSubmit() {}

  _submitUpdate() {
    return this.service.update(this.objectId, this.form.value).subscribe(
      (res: any) => {
        this._onUpdateSuccess(res);
      },
      (err: any) => {
        this._onSaveError(err);
      },
      () => {
        this._onSaveComplete();
      }
    );
  }

  _submitSave() {
    this.service.store(this.form.value).subscribe(
      (res: any) => {
        if (typeof res === 'string') {
          res = JSON.parse(res);
        }

        this._onSaveSuccess(res);
      },
      (err: any) => {
        this._onSaveError(err);
      },
      () => {
        this._onSaveComplete();
      }
    );
  }

  _onUpdateSuccess(res) {
    /** SAMPLE IMPLEMENTATION **/
    /*****************************
      this.form.reset();
      this.toastr.success('Unit saved', null);
      this.cancel();
    *****************************/

   this.toastr.success('Entry updated', null);

   this.formWasSaved.emit(true);
   this.toggleOverlay = false;
   this.formSubmitting = false;

   // this.resetForm();
    this.skipRedirect(true);

   // redirect to list
   this.redirectToList();
  }

  _onSaveSuccess(res) {
    /** SAMPLE IMPLEMENTATION **/
    /*****************************
      this.form.reset();
      this.toastr.success('Unit saved', null);
      this.cancel();
    *****************************/

    this.toastr.success('Entry saved', null);

    this.formWasSaved.emit(true);
    this.toggleOverlay = false;
    this.formSubmitting = false;

    this.resetForm();

    // redirect to list
    this.redirectToList();
  }

  _onSaveComplete() {
    /** SAMPLE IMPLEMENTATION **/
    this.formWasSaved.emit(false);
    this.toggleOverlay = false;
    this.formSubmitting = false;
  }

  _onSaveError(err) {

    /** SAMPLE IMPLEMENTATION **/
    /*****************************
      console.log('HERE ', err.error.message);
      let message = 'Error<br/>';
      for (const prop in err.error.message) {
        message += err.error.message[prop] + '<br>';
          //console.log(prop, err.error.message[prop]);
      }
      this.toastr.warning(message, null, {enableHTML: true});
    *****************************/

    let message = 'There was a problem saving your entry. Please try again.<br/><br/>';

    if (err.error.message) {
      if (typeof err.error.message === 'object') {
        for (const field in err.error.message) {
          if (err.error.message.hasOwnProperty(field)) {


            message += ((this.listErrors[err.error.message[field]]) ? this.listErrors[err.error.message[field]] : err.error.message[field]) + '<br/>';
          }
        }
      } else if (typeof err.error.message === 'string') {
        message += (this.listErrors[err.error.message]) ? this.listErrors[err.error.message] : err.error.message;
      }
    }

    this.toastr.warning(message, null, {enableHTML: true});

    this.formWasSaved.emit(false);
    this.toggleOverlay = false;
    this.formSubmitting = false;
  }

  redirectToList() {
    if (this._skipRedirect) {
      return true;
    }

    if (typeof this.listUrl === 'string') {
      this.router.navigate([this.listUrl]);
    }

    // RESET SKIP REDIRECT FLAG
    this._skipRedirect = false;
  }

  skipRedirect(skip: any) {
    if (typeof skip === 'boolean') {
      this._skipRedirect = skip;
    } else if (typeof skip === 'undefined') {
      this._skipRedirect = true;
    }

    return this;
  }

  resetForm() {
    if (this.resetFormOnSuccess) {
      this.form.reset();
    }
  }


  cancel() {
    /** SAMPLE IMPLEMENTATION **/
    /*****************************
      this.form.reset();
      this.formMode = 'create';
      this.formModeChange.emit('create');
    *****************************/
  }

  ngOnInit() {}

}
