import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GenerateReportComponent } from './generate-report.component';
import { RoutingModule } from './routing.module';
import { FilterModule } from './../crud/filter/filter.module';
import { MyDatePickerModule } from 'mydatepicker';
import { settings } from './../settings';
/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { DataTableModule } from 'angular5-data-table';
import { OverlayModule } from '@shared/overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */


import { ListModule as BaseListModule } from '@shared/base/list/list.module' ;

@NgModule({
  declarations: [
    GenerateReportComponent
  ],
  imports: [
    CommonModule,
    RoutingModule,
    FilterModule,
    MyDatePickerModule,
    BaseListModule,
  ],
  exports: [
    GenerateReportComponent,
  ],
  providers: [
    settings.service,
  ]
})
export class GenerateReportModule { }
