import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SssTableComponent } from './sss-table.component';

describe('SssTableComponent', () => {
  let component: SssTableComponent;
  let fixture: ComponentFixture<SssTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SssTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SssTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
