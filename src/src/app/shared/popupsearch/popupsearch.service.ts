import { Injectable } from '@angular/core';

import { NgbModal, ModalDismissReasons, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { PopupsearchComponent } from './popupsearch.component';

@Injectable()
export class PopupsearchService {

  closeResponse: boolean;
  closeResult: string;
  isOpen: Boolean = false;

  constructor(
    public modalService: NgbModal
  ) {}

  open(modalSettings) {
    const modalRef = this.modalService.open(PopupsearchComponent);

    modalRef.componentInstance.title = modalSettings.title;
    modalRef.componentInstance.body  = modalSettings.body;

    modalRef.result.then((result) => {
      this.closeResponse = result;
      // this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResponse = null;
      // this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });

    return modalRef;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
