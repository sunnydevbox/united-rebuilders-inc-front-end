import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class HolidayService extends AbstractService {
    endpoint = 'holidays';


    monthList(start, end) {
        return this.http.post(this.apiURL() + '/month-list', {
            start: start,
            end: end,
        })
        .map((res) => {
            return res;
        })
        .catch(this.handleError)
    }
}
