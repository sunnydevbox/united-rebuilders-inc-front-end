import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/*****************************************/
/**** IMPORT THE BASE MODULE for FORM ****/
/*****************************************/
/***/
/***/ import { FormModule as BaseFormModule } from '@shared/base/form/form.module';
/***/
/** INSTRUCTED FROM BASE FORM MODULE */
/***/
/***/ import { OverlayModule } from '@shared/overlay/overlay.module';
/***/

import { DeleteEmployeeComponent } from './delete-employee.component';

@NgModule({
  imports: [
    CommonModule,
    BaseFormModule,
    OverlayModule,
  ],
  declarations: [
    DeleteEmployeeComponent,
  ],
  exports: [
    DeleteEmployeeComponent,
  ]
})
export class DeleteEmployeeModule { }
