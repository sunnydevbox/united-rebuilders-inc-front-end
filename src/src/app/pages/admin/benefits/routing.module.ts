import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { Route } from './../../../core/route.service';

import { BenefitsComponent } from './benefits.component';
// import { ListComponent } from './crud/list/list.component';
// import { FormComponent } from './crud/form/form.component';
// import { CivilStatusComponent } from './civil-status.component';

const routes: Routes = [
    {
        path: '',
        data: { title: 'Civil Status'  },
        children: [
            {
              path: '',
              component: BenefitsComponent,
            },
          ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}
