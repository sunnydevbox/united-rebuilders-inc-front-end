import { Component, OnInit } from '@angular/core';
import { UserService } from '@services/user.service';
import { AuthenticationService } from './../../authentication/authentication.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-admin-theme',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  public user: any;
  public userSubscription: Subscription;

  constructor(
    private userService: UserService,
    private authService: AuthenticationService
  ) {
    this.userSubscription = this.userService.user().subscribe(user => {
      this.user = user;
    });
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

}
