import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { JobPositionsComponent } from './job-positions.component';
import { RoutingModule } from './routing.module';

import { ListModule } from './crud/list/list.module';
import { FormModule } from './crud/form/form.module';
import { settings } from './settings';


@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    FormModule,
    ListModule
  ],
  declarations: [
    JobPositionsComponent
  ],
  exports: [
    JobPositionsComponent,
  ],
  providers: [
    settings.service
  ],
})
export class JobPositionsModule { }
