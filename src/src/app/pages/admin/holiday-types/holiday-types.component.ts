import { Component, OnInit } from '@angular/core';

import { settings } from './settings';

@Component({
  selector: 'app-holiday-types',
  templateUrl: './holiday-types.component.html',
  styleUrls: ['./holiday-types.component.css']
})
export class HolidayTypesComponent implements OnInit {

  public settings;
  public _triggerReloadList;
  public _object;

  constructor() {
    this.settings = settings;
  }

  triggerReloadList(e) {
    if (e) {
      this._triggerReloadList = e;
    }
  }

  getObject(object) {
    this._object = object;
  }

  ngOnInit() {
  }

}
