import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

// IMPORT THE BASE COMPONENT
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';

// IMPORT THE MODEL DATA
import { MODEL } from './model';
import { ERRORS } from './../../../model';
import { EmployeeService } from '@services/employee.service';

import { settings } from './../../../settings';
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';



@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent extends BaseFormComponent implements OnChanges {

  public formMode = 'create';
  public settings;
  @Input() object;
  @Input() lists;

  public employeeService;

  public saveAndContinue = false;

  dateNow = new Date();
  public datePickerOptions: IMyDpOptions = {
    dateFormat: 'mmm dd, yyyy',
    editableDateField: false,
    openSelectorOnInputClick: true,
    sunHighlight: true,
    minYear: this.dateNow.getFullYear() - 90,
    maxYear: this.dateNow.getFullYear() + 2,
    height: '25px',
    selectionTxtFontSize: '11px',
    showTodayBtn: false,
    // showClearDateBtn: false,
  };

  _postConstruct() {
    this.settings = settings;

    this.listUrl = settings.crud.list.url;

    this.service = this.injector.get(settings.service);
    this.employeeService = this.injector.get(EmployeeService);

    this.form = this.formBuilder.group(MODEL);
    this.errors = ERRORS;
  }


  _preSubmit() {
    if (this.form.get('birthdate').value) {
      this.form.get('birthdate').setValue(this.form.get('birthdate').value.formatted);
    }
  }


  submitContinue() {
    this.saveAndContinue = true;
    this.skipRedirect(true);
    this.submit();

  }

  _onSaveSuccess(res) {
    super._onSaveSuccess(res);

    if (this.saveAndContinue) {
      // RESET saveAndContinue
      this.saveAndContinue = false;
      this.router.navigate([settings.crud.update.url + '/' + res.data.id]);
    }
  }

  _onUpdateSuccess(res) {
    super._onUpdateSuccess(res);
    this.populateForm(res.data);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.formMode = this.route.snapshot.data.formMode;

    if (changes.object && changes.object.currentValue) {
      this.populateForm(changes.object.currentValue);
    }
  }


  populateForm(object) {
    if (this.formMode === 'update') {
      this.objectId = object.id;
    }

    // POPULATE THE FORM
    this.form.get('id').setValue(object.id);
    this.form.get('employee_id_number').setValue(object.employee_id_number);
    this.form.get('first_name').setValue(object.first_name);
    this.form.get('last_name').setValue(object.last_name);
    this.form.get('email').setValue(object.email);

    this.form.get('title_id').setValue(object.title_id);
    this.form.get('gender').setValue(object.gender);
    this.form.get('civil_status_id').setValue(object.civil_status_id);
    this.form.get('nationality').setValue(object.nationality);
    this.form.get('religion').setValue(object.religion);
    this.form.get('landline').setValue(object.landline);
    this.form.get('mobile').setValue(object.mobile);

    this.form.get('address_1').setValue(object.address_1);
    this.form.get('address_2').setValue(object.address_2);
    this.form.get('city').setValue(object.city);
    this.form.get('province').setValue(object.province);
    this.form.get('zipcode').setValue(object.zipcode);
    this.form.get('country').setValue(object.country);
    // this.form.get('notes').setValue(object.notes);


    if (object.birthdate) {
      const bd = new Date(object.birthdate);
      // object.birthdate = new
      // console.log(object.birthdate)
      object.birthdate = {
        date: {
          year: bd.getFullYear(),
          month: bd.getMonth() + 1,
          day:  bd.getDate(),
        }
      };
    }


    // let birthdate;
    // console.log(this.form.get('birthdate').value)
    // if (this.form.get('birthdate').value.jsdate) {
    //   birthdate = this.form.get('birthdate').value.jsdate
    // } else if (this.form.get('birthdate').value.date) {
    //   birthdate = new Date(
    //                 this.form.get('birthdate').value.date.year + '-' +
    //                 this.form.get('birthdate').value.date.month + '-' +
    //                 this.form.get('birthdate').value.date.day
    //               );
    // } else {
    //   birthdate = new Date`(this.form.get('birthdate').value)
    // }

    this.form.get('birthdate').setValue(object.birthdate);
  }

}
