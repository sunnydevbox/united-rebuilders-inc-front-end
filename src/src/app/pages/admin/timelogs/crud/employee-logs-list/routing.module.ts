import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { EmployeeLogsListComponent } from './employee-logs-list.component';

const routes: Routes = [
    {
        path: '',
        data: { title: 'Timelog'  },
        component: EmployeeLogsListComponent,
        // ß
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {}
