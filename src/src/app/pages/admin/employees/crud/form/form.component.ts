import { Component, OnInit } from '@angular/core';

// IMPORT THE BASE COMPONENT
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';

// Others..
import { settings } from './../../settings';
import { SupplementDataService } from '@services/supplement-data.service';
import { UserService } from '@services/user.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent extends BaseFormComponent implements OnInit {

  public object;

  private supplementDataService;
  public lists = [];
  public userService;

  _postConstruct() {
    this.service = this.injector.get(settings.service);
    this.supplementDataService = this.injector.get(SupplementDataService);
    this.userService = this.injector.get(UserService);

    this.supplementDataService.index({
      data: 'civil-status;company-departments;tax-status;'
            + 'deductions;benefits;job-positions;titles;employment-status;'
            + 'employment-types;shift-templates;payroll-templates;employee-templates'
    })
    .subscribe(
      (res) => {
        this.lists['civilStatus']         = res['civil-status'];
        this.lists['companyDepartments']  = res['company-departments'];
        this.lists['deductions']          = res['deductions'];
        this.lists['jobPositions']        = res['job-positions'];
        this.lists['titles']              = res['titles'];
        this.lists['taxStatus']           = res['tax-status'];
        this.lists['employmentStatus']    = res['employment-status'];
        this.lists['employmentTypes']     = res['employment-types'];
        this.lists['shiftTemplates']      = res['shift-templates'];
        this.lists['payrollTemplates']    = res['payroll-templates'];
        this.lists['benefits']            = res['benefits'];
        this.lists['employee-templates']  = res['employee-templates'];
      }
    );
  }

  ngOnInit() {
    this.formMode = this.route.snapshot.data.formMode;


    if (this.formMode === 'update') {
      // LOAD the record

      this.toggleOverlay = true;

      this.objectId = this.router.parseUrl(this.router.url).root.children.primary.segments[3];

      this.service.show(this.objectId, {
        with: 'deductions;shift_templates;payroll_templates;benefits;employee_template'
      }).subscribe(
        (response) => {

          // THis will be passed to the child components
          this.object = response.data;
        },

        (error) => {

        },

        () => {
          this.toggleOverlay = false;
        },

      );

    }

  }
}
