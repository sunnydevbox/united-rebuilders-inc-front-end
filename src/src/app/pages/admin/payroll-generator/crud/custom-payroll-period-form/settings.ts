import { PayrollLogService } from '@services/payroll-log.service';

export const settings = {
    plural: 'Payroll Generator',
    singular: 'Payroll Generator',
    service: PayrollLogService,
  
    crud: {
      create: {
        allow: true, // true/false
        url: '/admin/roles/create'
      },
      read:{
  
      },
      update:{
        allow: true,
        url: '/admin/roles/edit/'
      },
      delete:{
        allow: true
      },
      list: {
        allow: true,
        url: '/admin/roles'
      }
    }
  };