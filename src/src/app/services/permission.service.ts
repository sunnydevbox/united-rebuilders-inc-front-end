import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class PermissionService extends AbstractService {
    endpoint = 'permissions';
}
