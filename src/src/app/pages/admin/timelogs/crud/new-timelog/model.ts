
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
    // 'name' : [null, Validators.compose([Validators.required, Validators.minLength(2)])],
    'employee_id': [null],
    'notes': [null],
    'time_in': [null, Validators.compose([])],
    'time_in_time': [null, Validators.compose([Validators.required])],
    'time_in_date': [null, Validators.compose([Validators.required])],
    'time_out': [null],
    'time_out_time': [null, Validators.compose([Validators.required])],
    'time_out_date': [null, Validators.compose([Validators.required])],
};
