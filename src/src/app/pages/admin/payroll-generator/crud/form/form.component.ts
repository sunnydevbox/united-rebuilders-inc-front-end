import { Component, Output, EventEmitter, Input, SimpleChanges, OnChanges } from '@angular/core';
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';
import { MODEL } from './model';
import { settings } from './settings';
import { months, range } from '@admin/config';
import { SupplementDataService } from '@services/supplement-data.service';
import { $ } from 'protractor';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent extends BaseFormComponent implements OnChanges {
  @Output() triggerReloadList = new EventEmitter();
  @Output() objectChanged = new EventEmitter();
  @Input() object;
  @Input() payrollTemplateId = null;

  public months = months;
  public range = range;
  private supplementDataService;
  public lists = {
    'shift-templates': []
  };

  _postConstruct() {
    this.service = this.injector.get(settings.service);
    this.form = this.formBuilder.group(MODEL);
    this.supplementDataService = this.injector.get(SupplementDataService);

    this.supplementDataService.index({data: 'shift-templates'}).subscribe(
      (res) => {
        this.lists['shift-templates'] = res['shift-templates'];
      }
    );
  }

  _onSaveSuccess(res) {
    super._onSaveSuccess(res);

    // Trigger Relaod list
    this.triggerReloadList.emit(true);

    // Reset the form
    this.form.reset();
    this.form.get('payroll_template_id').setValue(this.payrollTemplateId);
  }

  _onUpdateSuccess(res) {
    super._onUpdateSuccess(res);
    // Trigger Relaod list
    this.triggerReloadList.emit(true);

    // Reset the form
    this.objectId = null;
    this.form.reset();

    // REset back to create mode
    this.formMode = 'create';

    this.objectChanged.emit(null);
  }


  dateObj() {
    return new Date();
  }

  cancel() {
    this.form.reset();
    this.form.get('payroll_template_id').setValue(this.payrollTemplateId);
    this.objectChanged.emit(null);
    this.object = null;
  }

  triggerListReload(e) {
    this.triggerReloadList.emit(e);
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.payrollTemplateId && changes.payrollTemplateId.currentValue) {
      this.form.get('payroll_template_id').setValue(changes.payrollTemplateId.currentValue);
      
    }

    if (changes.object && changes.object.currentValue) {
      let customPayrollBtn: HTMLElement = document.getElementById('custom-payroll-form-btn') as HTMLElement;
      let customPayrolLogFormCollapse: HTMLElement = document.getElementById('customPayrolLogForm') as HTMLElement;

      if (!customPayrolLogFormCollapse.classList.contains('show')) {
        customPayrollBtn.click();
      }
    }
  }
}
