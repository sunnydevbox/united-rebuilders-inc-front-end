import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CashAdvanceComponent } from './cash-advance.component';
import { RoutingModule } from './routing.module';
import { settings } from './settings';
import { ListModule } from './crud/list/list.module';
import { FormModule } from './crud/form/form.module';

@NgModule({
  declarations: [CashAdvanceComponent],
  imports: [
    CommonModule,
    RoutingModule,
    ListModule,
    FormModule,
  ],
  providers: [
    settings.service,
  ]
})
export class CashAdvanceModule { }
