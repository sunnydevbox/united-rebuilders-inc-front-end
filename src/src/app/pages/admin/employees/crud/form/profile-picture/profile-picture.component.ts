import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

// IMPORT THE BASE COMPONENT
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';

import { ERRORS } from './../../../model';
import { MODEL } from './model';
import { EmployeeService } from '@services/employee.service';


@Component({
  selector: 'app-profile-picture',
  templateUrl: './profile-picture.component.html',
  styleUrls: ['./profile-picture.component.css']
})
export class ProfilePictureComponent extends BaseFormComponent implements OnChanges {

  @Input() object;

  public profilePictureUrl = './assets/images/profile-placeholder.png';

  _postConstruct() {
    this.formMode = 'update';
    this.form = this.formBuilder.group(MODEL);
    this.errors = ERRORS;

    this.service = this.injector.get(EmployeeService);
  }

  onFileChanged(event) {
    const file = event.target.files[0];
    console.log(file);
    const formdata: FormData = new FormData();
    formdata.append('profile_picture', file);

    // //this.form.get('profile_picture').setValue(file);
    // uploadData.append('profile_pciture', file, file.name);
    formdata.append('type', 'profile-picture');
    formdata.append('id', this.objectId);
    console.log(formdata);

    this.service.upload(formdata)
    .subscribe(
      (res) => {
        console.log(res);
        this.profilePictureUrl = res.data.url;
      },
      (err) => {
        console.log('err ', err);
      }
    );
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.object && changes.object.currentValue) {
      this.objectId = changes.object.currentValue.id;

      if (changes.object.currentValue.profile_picture) {
        this.profilePictureUrl = changes.object.currentValue.profile_picture.url;
      }
    }
  }

}
