import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

// import { Route } from './../../../../core/route.service';

import { TaxComponent } from './tax.component';

// import { ListComponent } from './crud/list/list.component';
// import { FormComponent } from './crud/form/form.component';
// import { RolesComponent } from './roles.component';

const routes: Routes = [
    {
        path: '',
        data: { title: 'Roles'  },
        children: [
            {
              path: '',
              component: TaxComponent
              ,
            },
            // {
            //   path: 'edit/:id',
            //   component: FormComponent,
            //   data: { title: 'Edit Role'  },
            // },
          ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}
