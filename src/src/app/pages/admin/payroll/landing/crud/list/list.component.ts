import { Component, OnInit, Output, EventEmitter, OnChanges, SimpleChanges, Input } from '@angular/core';

import { ListComponent as BaseListComponent } from '@shared/base/list/list.component' ;

import { settings } from './../../../../payroll/settings';
import { UserService } from '@services/user.service';
import { distanceInWords } from 'date-fns';
import { formatDateFix, minutesToHoursMinutes } from '@admin/config';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseListComponent implements OnChanges {

  public userService;
  public settings;
  public isRecalculating = false;

  @Output() object = new EventEmitter();
  @Input() filters;
  @Input() payrollTemplateId = null;


  _postConstruct() {
    // this.loadListOnInit = false;
    this.settings = settings;
    this.service = this.injector.get(settings.service);
    this.userService = this.injector.get(UserService);
    this.defaultParams = {
      with: 'payroll_log.payroll_template;employee',
      orderBy: 'payroll_log_id',
      sortedBy: 'desc'
    };

    if (this.route.snapshot.queryParamMap.params['payroll']) {
      this.defaultParams['search'] = this.route.snapshot.queryParamMap.params['payroll']
      this.defaultParams['searchFields'] = 'payroll_log_id:=';
    }

    // console.log(
    //   this.route.snapshot.queryParamMap.params,
    //   this.router.url,
    //   this.router.parseUrl(this.router.url).root.children
    // );

  }
  callBackForChangineRowColors(item) {
    return (item.status == 'BELOW40') ? '#ed9a9a' : null;
  }

  formatDateFix(v) {
    return formatDateFix(v);
  }

  edit(object) {
    this.object.emit(object);
  }

  recalculate(item) {
    const sta = this.confirmModalService.open({
      title: 'Recalculate Payroll',
      body: 'This employee\'s payroll will be recalculated. '
            + 'Any changes made to the timelog prior to this action will be accounted for.',
      ctaPositive: 'Continue',
    });

    sta.result.then((result) => {
      if (result) {
        this.isRecalculating = true;
        this.service.recalculate(item.id).subscribe(
          (res) => {
            this.isRecalculating = false;
            this.toastr.success('Payroll recalculated');
            this.reloadItems(this.defaultParams);
          },
          (err) => {
            this.isRecalculating = false;
          }
        );
      }
    }, (reason) => {
      console.log(reason);
    });

    
  }

  triggerGenerate(object) {

    const sta = this.confirmModalService.open({
      title: 'Processing Payrolls',
      body: 'Payrolls are processed in the background. Please check back for the progress.'
            + '<br/> <strong class="text-danger">IMPORTANT:</strong> Ensure that timelogs are verified and approved before proceeding.',
      ctaPositive: 'Continue',
    });

    sta.result.then((result) => {
      if (result) {
        this.service.triggerGenerate(object.id).subscribe(
          (res) => {
            console.log(res);
          }
        )
      }
    }, (reason) => {
      console.log(reason);
    });
  }

  minutesToHoursMinutes(minutes) {
    return minutesToHoursMinutes(minutes);
  }

  ngOnChanges(changes: SimpleChanges) {
    super.ngOnChanges(changes);

    // if (changes.payrollTemplateId && changes.payrollTemplateId.currentValue) {
    //   this.payrollTemplateId = changes.payrollTemplateId.currentValue;
    //   this.reloadItems({
    //     search: this.payrollTemplateId
    //   });
    // }
  }
}
