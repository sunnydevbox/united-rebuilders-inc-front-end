import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class ShiftTemplatePeriodService extends AbstractService {
    endpoint = 'shift-template-periods';

    perEmployee(employeeId) {
        return this.http.get(this.apiURL() + '/employee/' + employeeId)
        .map((result: any) => {
            return result;
        })
        .catch(this.handleError);
    }
}
