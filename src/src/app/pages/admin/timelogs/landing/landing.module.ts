import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LandingComponent } from './landing.component';
import { RoutingModule } from './routing.module';
import { ListModule } from './../crud/list/list.module';
import { FormModule } from './../crud/form/form.module';
import { FormImportModule } from './../crud/form-import/form-import.module';
import { FiltersModule } from './../filters/filters.module';
import { NewTimelogModule } from './../crud/new-timelog/new-timelog.module';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    ListModule,
    FormModule,
    FormImportModule,
    FiltersModule,
    NewTimelogModule,
  ],
  declarations: [
    LandingComponent
  ],
  exports: [
    LandingComponent,
  ]
})
export class LandingModule { }
