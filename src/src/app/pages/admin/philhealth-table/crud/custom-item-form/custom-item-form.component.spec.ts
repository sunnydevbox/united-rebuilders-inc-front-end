import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomItemFormComponent } from './custom-item-form.component';

describe('CustomItemFormComponent', () => {
  let component: CustomItemFormComponent;
  let fixture: ComponentFixture<CustomItemFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomItemFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomItemFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
