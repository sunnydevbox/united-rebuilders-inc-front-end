import { Component, Output, EventEmitter, Input, SimpleChanges, OnChanges } from '@angular/core';
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';
import { MODEL } from './model';
import { settings } from './../../settings';
import { months, range } from '@admin/config';
import { SupplementDataService } from '@services/supplement-data.service';
import { timingSafeEqual } from 'crypto';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent extends BaseFormComponent implements OnChanges {
  @Output() triggerReloadList = new EventEmitter();
  @Output() objectChanged = new EventEmitter();
  @Input() object;
  @Input() payrollTemplateId = null;

  public payslip;
  public months = months;
  public range = range;
  private supplementDataService;
  public lists = {
    'shift-templates': []
  };

  _postConstruct() {
    this.service = this.injector.get(settings.service);
    this.form = this.formBuilder.group(MODEL);
    this.supplementDataService = this.injector.get(SupplementDataService);

    this.supplementDataService.index({data: 'shift-templates'}).subscribe(
      (res) => {
        this.lists['shift-templates'] = res['shift-templates'];
      }
    );


          
    this.formMode = this.route.snapshot.data.formMode;
    if (this.formMode == 'update') {
      this.toggleOverlay = true;
      
      this.objectId = this.router.parseUrl(this.router.url).root.children.primary.segments[5];
      this.getPayslip(this.objectId);
    }
  }

  getPayslip(payrollId) {
    this.service.show(payrollId, {
      with: 'items;payroll_log;employee'
    }).subscribe(
      (res) => {
        this.payslip = res.data;
      },
      (error) => {
        this.toggleOverlay = false;
        this.toastr.error('Invalid Payslip');
        
      },
      () => {
        this.toggleOverlay = false;
      }
    )
  }

  _onSaveSuccess(res) {
    super._onSaveSuccess(res);

    // Trigger Relaod list
    this.triggerReloadList.emit(true);

    // Reset the form
    this.form.reset();
    this.form.get('payroll_template_id').setValue(this.payrollTemplateId);
  }

  _onUpdateSuccess(res) {
    super._onUpdateSuccess(res);
    // Trigger Relaod list
    this.triggerReloadList.emit(true);

    // Reset the form
    this.objectId = null;
    this.form.reset();

    // REset back to create mode
    this.formMode = 'create';

    this.objectChanged.emit(null);
  }


  dateObj() {
    return new Date();
  }

  cancel() {
    this.form.reset();
    this.form.get('payroll_template_id').setValue(this.payrollTemplateId);
    this.objectChanged.emit(null);
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.payrollTemplateId && changes.payrollTemplateId.currentValue) {
      this.form.get('payroll_template_id').setValue(changes.payrollTemplateId.currentValue);
      
    }

    if (changes.object && changes.object.currentValue) {
      // console.log(changes.object.currentValue);
    }
  }
}
