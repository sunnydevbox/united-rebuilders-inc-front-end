import { Injectable } from '@angular/core';

import { AbstractService } from './AbstractService';
import { environment } from './../../environments/environment';

import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

@Injectable()
export class UserService extends AbstractService {

    endpoint = 'users';
    userData = null;
    credentialsKey = 'credentials';
    storageType = 'session'; // TEMPORARILY STATIC

    user() {
        this.userData = new BehaviorSubject(this.getStoredCredentials());
        return this.userData;
    }

    getList(e?: any) {
        return this.index(e);
    }

    clearCurrentUser() {
        window.localStorage.removeItem('currentUser');
    }

    setCurrentUser(user: any) {
        window.localStorage.setItem('currentUser', user);
    }

    getCurrentUser() {
        return window.localStorage.getItem('currentUser');
    }

    getUserByToken() {
        const token = JSON.parse(window.localStorage.getItem('credentials')).token;
        return this.http.post(this.apiURL() + '/by-token', {token: token})
        .map((response: any) => {
            return response.data;
            // login successful if there's a jwt token in the response
        },
    (error: any) => {
        return error;
    });
    }

    register(data: any) {
        return this.http.post(this.apiURL() + '/register', data)
            .map((response: any) => {
                // console.log(response)
                return response;
            },
            (err: any) => {
                // console.log(err);
                return err;
            });
    }

    updateDetails(id: any, params: any) {
        return this.http.put(this.apiURL() + '/details/' + id, params)
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            }
        );
    }

    updateEmail(id: any, params: any) {
        return this.http.put(this.apiURL() + '/email/' + id, params)
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            }
        );
    }

    updatePassword(id: any, params: any) {
        return this.http.put(this.apiURL() + '/password/' + id, params)
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            }
        );
    }

    setRoles(userId: any, roles: Array<any> []) {
        return this.http.post(this.apiURL() + '/' + userId + '/assign-roles', roles)
        .map(
            (response: any) => {
                return response;
            },
            (error: any) => {
                console.log(error);
                return error;
            }
        );
    }

    removeRole(userId: any, roles: Array<any> []) {
        return this.http.post(this.apiURL() + '/' + userId + '/remove-role', roles)
        .map(
            (response: any) => {
                return response;
            },
            (error: any) => {
                console.log(error);
                return error;
            }
        );
    }


    authenticate(data: any) {

        // REMEMBER ME
        window.localStorage.removeItem('rememberMe');
        window.localStorage.setItem('rememberMe', data.remember);

        return this.http.post(
            this.apiURL() + '/auth',
            JSON.stringify(data)
        ).map((response: any) => {

            if (this._validateUserData(response.data)) {
                const creds = {
                    token: response.data.token,
                    id: response.data.id,
                    first_name: response.data.first_name,
                    last_name: response.data.last_name,
                    email: response.data.email,
                    roles: response.data.roles,
                    permissions: response.data.permissions,
                    remember: data.remember,
                };
                this.updateStoredCredentials(creds);

                return this.getStoredCredentials();
            }

            return {
                status: false,
                error: {
                    error: 'system_error'
                }
            };
        }).catch(this.handleError);
    }

    _validateUserData(data) {
        // ID, EMAIL, First/last name, roles and permission must exist

        console.log(data);
        if (
            typeof data.id === 'undefined' ||
            typeof data.token === 'undefined' ||
            typeof data.email === 'undefined' ||
            typeof data.first_name === 'undefined' ||
            typeof data.last_name === 'undefined' ||
            typeof data.roles === 'undefined' ||
            typeof data.permissions === 'undefined'
        ) {
            // log.error('Invalid response. Missing data');
            return false;
        }

        // log.error('Validated response');
        return true;
    }

    updateStoredCredentials(data?: Object) {
        const storedCredentials = this.getStoredCredentials();
        let  newCredentials;

        if (data === null) {
            newCredentials = JSON.stringify({});
        } else {
            newCredentials = JSON.stringify(Object.assign(storedCredentials, data));
        }

        this.storageType = (window.localStorage.getItem('rememberMe')) ? 'local' : 'session';

        switch (this.storageType) {
            case 'local':
                window.localStorage.setItem(this.credentialsKey, newCredentials);
                break;
            case 'session':
                sessionStorage.setItem(this.credentialsKey, newCredentials);
                break;
        }
    }

    getStoredCredentials() {
        let storedCredentials: any = {};

        this.storageType = (window.localStorage.getItem('rememberMe')) ? 'local' : 'session';

        switch (this.storageType) {
            case 'session':
                storedCredentials = sessionStorage.getItem(this.credentialsKey);
            break;

            case 'local':
                storedCredentials = window.localStorage.getItem(this.credentialsKey);
            break;
        }

        if (!storedCredentials) {
            storedCredentials = '{}';
        }

        return JSON.parse(storedCredentials);
    }

    isLoggedIn() {
        const s = this.updateStoredCredentials();
        return Object.keys(this.getStoredCredentials()).length === 0 ? false : true;
    }

    redirectToLogin() {

        window.location.href = '/login';
    }

    checkPermission(permissions: any = []) {
        if (!this.isLoggedIn()) {
            this.redirectToLogin();
        }

        const credentials = this.getStoredCredentials();

        // ADMIN has access to ALL
        if (this.checkRole(['admin'])) {
            return true;
        }

        let status = false;

        if (!credentials.permissions) {
            return false;
        }

        const Permissions = (typeof credentials.permissions.data !== 'undefined') ? credentials.permissions.data : credentials.permissions;
        if (Permissions && Permissions.length) {
            Permissions.forEach((userPermission: any) => {
                permissions.filter(function(permission) {
                    if (userPermission.name === permission) {
                        status = true;
                    }
                });
            });
        }

        return status;
    }

    checkRole(roles: any = []) {
        if (!this.isLoggedIn()) {
            this.redirectToLogin();
        }

        const credentials = this.getStoredCredentials();
        let status = false;

        if (!credentials.roles) {
            return false;
        }

        const Roles = (typeof credentials.roles.data !== 'undefined') ? credentials.roles.data : credentials.roles;

        if (Roles && Roles.length) {
            Roles.forEach((userRole: any) => {
                roles.filter(function(role: string) {
                    if (userRole.name === role) {
                        status = true;
                    }
                });
            });
        }

        return status;
    }

    checkUserRole(roleToCheck: string) {

        const credentialsKey = 'credentials';
        let savedCredentials;
        savedCredentials = sessionStorage.getItem(credentialsKey) ?
            sessionStorage.getItem(credentialsKey) : window.localStorage.getItem(credentialsKey);

        const credentials = JSON.parse(savedCredentials);

        let status = false;
        if (credentials.roles) {
            credentials.roles.forEach((role: any) => {
                // console.log(role.name + '==' + roleToCheck)
                if (role.name === roleToCheck) {
                    status = true;
                }
            });
        }

        return status;
    }

    logout(): Observable<boolean> {
        this.updateStoredCredentials(null);
        return of(true);
      }

    verifyEmail(token: string) {
        return this.http.get(this.apiURL() + '/verify-email/' + token)
            .map((result: any) => {
                return result;
            },
            (error: any) => {
                return error;
            });
    }


    setStatus(id, status) {

        return this.http.put(this.apiURL() + '/' + id, {'status': status})
            .map((result: any) => {
                return result;
            })
            .do(data => {
                // console.log('createProduct: ' + JSON.stringify(data))
            })
            .catch(this.handleError)
            ;
    }
}
