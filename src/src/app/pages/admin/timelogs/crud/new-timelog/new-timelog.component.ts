import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';
import { MODEL } from './model';
import { settings } from './settings';
import { datePickerOptions1 } from '@admin/config';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';

// import {NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';
import { EmployeeService } from '@services/employee.service';


const states = ['Alabama', 'Alaska', 'American Samoa', 'Arizona', 'Arkansas', 'California', 'Colorado',
  'Connecticut', 'Delaware', 'District Of Columbia', 'Federated States Of Micronesia', 'Florida', 'Georgia',
  'Guam', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine',
  'Marshall Islands', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana',
  'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota',
  'Northern Mariana Islands', 'Ohio', 'Oklahoma', 'Oregon', 'Palau', 'Pennsylvania', 'Puerto Rico', 'Rhode Island',
  'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virgin Islands', 'Virginia',
  'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];

@Component({
  selector: 'app-new-timelog',
  templateUrl: './new-timelog.component.html',
  styleUrls: ['./new-timelog.component.css']
})
export class NewTimelogComponent extends BaseFormComponent  implements OnChanges {

  public datePickerOptions = datePickerOptions1;
  public employeeService;
  searching = false;
  searchFailed = false;

  @Input() employee;

  _postConstruct() {
    this.form = this.formBuilder.group(MODEL);
    this.service = this.injector.get(settings.service)
    this.employeeService = this.injector.get(EmployeeService);
  }

  _preSubmit() {
    this.form.get('time_in').setValue(
      this.form.get('time_in_date').value.date.year + '-' +
      this.form.get('time_in_date').value.date.month + '-' +
      this.form.get('time_in_date').value.date.day + ' ' +
      this.form.get('time_in_time').value
    );


    this.form.get('time_out').setValue(
      this.form.get('time_out_date').value.date.year + '-' +
      this.form.get('time_out_date').value.date.month + '-' +
      this.form.get('time_out_date').value.date.day + ' ' +
      this.form.get('time_out_time').value
    );
  }

  cancle() {
    super.cancel();
  }

  resetForm() {
    super.resetForm();

    this.form.get('employee_id').setValue(this.employee.id);
  }

  formatter(result) {
    return result.first_name + ' ' + result.last_name
  }

  search = (text$: Observable<string>) => 
    // text$.pipe(
    //   debounceTime(300),
    //   distinctUntilChanged(),
    //   tap(() => this.searching = true),
    //   switchMap(term => this.employeeService.searchName(term))
    // );

    text$
      .debounceTime(200)
      .distinctUntilChanged()
      .do((text) => console.log(text))
      .switchMap(term =>
        this.employeeService.searchName(term)
      );

  ngOnChanges(changes: SimpleChanges) {
    if (changes.employee && changes.employee.currentValue) {
      this.form.get('employee_id').setValue(changes.employee.currentValue.id);
    }
  }
}
