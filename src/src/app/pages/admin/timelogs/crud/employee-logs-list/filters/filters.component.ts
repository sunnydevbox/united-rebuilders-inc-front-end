import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';

import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';
import { FiltersComponent as BaseFiltersComponent } from '@shared/base/filters/filters.component';


import {Observable} from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';

import { MODEL } from './model';
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import { datePickerOptions1 } from '@admin/config';

@Component({
  selector: 'app-filters',
  templateUrl: './filters.component.html',
  styleUrls: ['./filters.component.css']
})
export class FiltersComponent extends BaseFiltersComponent implements OnInit, OnDestroy {

  private formControleName: Subscription;
  public datePickerOptions1;

  @Output() filtersChanged = new EventEmitter();
  @Output() searchChangeEmitter = new EventEmitter();
  // This is an output component that tells the rest of the world that the user has entered a valid text

  _postConstruct() {
    this.datePickerOptions1 = datePickerOptions1;
    this.model = MODEL;
    this.form = this.formBuilder.group(MODEL);
  }

  fieldProcessor(field) {
    if (field === 'time_in' && this.form.get(field).value && typeof this.form.get(field).value == 'object') {
      // console.log(typeof this.form.get(field).value, this.form.get(field).value)
      this.form.get(field).setValue(this.form.get(field).value.formatted);
    }

    if (field === 'time_out' && this.form.get(field).value && typeof this.form.get(field).value == 'object') {
      // console.log(typeof this.form.get(field).value, this.form.get(field).value)
      this.form.get(field).setValue(this.form.get(field).value.formatted);
    }

    super.fieldProcessor(field);    
  }

  checkboxChanged(e) {
    this.form.get(e.target.id).setValue((e.target.checked) ? 1 : 0);
    this.startFilter();
  }

  dateChanged(field, event: IMyDateModel) {
    if (field === 'time_in') {
      this.form.get(field).setValue(event.formatted);
    }

    if (field === 'time_out') {
      this.form.get(field).setValue(event.formatted);
    }

    this.startFilter();
  }

  ngOnDestroy() {
    // this.formControleName.unsubscribe();
  }
}
