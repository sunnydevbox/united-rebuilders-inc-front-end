import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NoPermissionViewComponent } from './no-permission-view.component';

describe('NoPermissionViewComponent', () => {
  let component: NoPermissionViewComponent;
  let fixture: ComponentFixture<NoPermissionViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NoPermissionViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NoPermissionViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
