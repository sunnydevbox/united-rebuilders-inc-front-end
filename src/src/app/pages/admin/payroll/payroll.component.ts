import { Component, OnInit } from '@angular/core';
import { settings } from './settings';

@Component({
  selector: 'app-payroll',
  templateUrl: './payroll.component.html',
  styleUrls: ['./payroll.component.css']
})
export class PayrollComponent implements OnInit {
  public settings;

  constructor() { 
    this.settings = settings;

  }

  ngOnInit() {
  }

}
