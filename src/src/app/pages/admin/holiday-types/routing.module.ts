import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { Route } from './../../../core/route.service';

import { ListComponent } from './crud/list/list.component';
import { FormComponent } from './crud/form/form.component';
import { HolidayTypesComponent } from './holiday-types.component';

const routes: Routes = [
    {
        path: '',
        data: { title: 'Holiday Types'  },
        children: [
            {
              path: '',
              component: HolidayTypesComponent,
            },
            {
              path: 'edit/:id',
              component: FormComponent,
              data: { title: 'Edit Employment Status'  },
            },
          ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}
