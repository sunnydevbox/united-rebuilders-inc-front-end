import { LeaveTypeService } from '@services/leave-type.service';

export const settings = {
    plural: 'Leave Applications',
    singular: 'Leave Application',
    service: LeaveTypeService,

    crud: {
      create: {
        allow: true, // true/false
        url: '/admin/settings/system/leave-types/create'
      },
      read: {

      },
      update: {
        allow: true,
        url: '/admin/settings/system/leave-types/edit/'
      },
      delete: {
        allow: true
      },
      list: {
        allow: true,
        url: '/admin/settings/system/leave-types'
      }
    }
  };
