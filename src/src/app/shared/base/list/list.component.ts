import { Component, OnInit, OnChanges, Injector, Input, Output, EventEmitter, SimpleChanges, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataTable, DataTableResource } from 'angular5-data-table';
import { ConfirmService } from './../../confirm/confirm.service';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { NgbTooltip } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit, OnChanges {

  // VARIABLES REQUIRED FOR THIS COMPONENT
  public confirmModalService;
  private vcr;
  public toastr;
  public toggleOverlay: Boolean = false;
  public loadListOnInit: Boolean = true;


  @ViewChild(DataTable) dataTable;
  @Input() triggerListReload: Boolean = false;
  @Output() triggerListReloadChange = new EventEmitter();
  @Output() selectedObject = new EventEmitter();

  public route;
  public router;

  // MAIN SERVICE FOR THE LIST
  public service;

  public defaultParams = {};
  config = {
    confirmDelete: {
      title: 'Confirm Delete',
      body: 'Are you sure you want to delete this?'
    }
  };

  items = [];
  itemCount = 0;

  constructor(
    public injector: Injector
  ) {
      this.confirmModalService = injector.get(ConfirmService);
      this.toastr = injector.get(ToastsManager);
      this.route = this.injector.get(ActivatedRoute);
      this.router = this.injector.get(Router);
      this._postConstruct();
  }

  // OVERRIDE THIS.
  // PUT IN HERE any classes that need
  // instantiation such as services
  _postConstruct() {}

  reloadItems(params) {
    
    if (!this.service) {
      return null;
    }

    // this.toggleOverlay = true;
    params = Object.assign(this.defaultParams, params);
    params = this.service.paramsToQueryString(params);

    // this.filmResource.query(params).then(films => this.films = films);
    this.service.index(params).subscribe(
        (res: any) => {
        this._onSuccess(res);
      },
      (err: any) => {
        this._onError(err);
      },
      () => {
        this._onComplete();
      }
    );

    // console.log(params)
    // this.itemResource.query(params).then(items => this.items = items);
  }

  _onSuccess(res) {
    this.items = (typeof res['data'] !== 'undefined') ? res.data : res;
    if (typeof res['meta'] !== 'undefined') {
      this.itemCount = res.meta.pagination.total;
    }
  }

  _onError(err) {
    this.toggleOverlay = false;
    this.toastr.warning('There was a problem updating the list', null, {enableHTML: true});
  }

  _onComplete() {
    this.toggleOverlay = false;
    this.triggerListReloadChange.emit(false);
  }

  _onDeleteSuccess(response) {
    // trIGGER ToASTeR
    this.toastr.success('Item successfully deleted', null, {enableHTML: true});

    // Reload THe list
    this.reloadItems({
      page: 1
    });
  }

  _onDeleteError(err) {
    this.toastr.warning('We encountered a problem. Please try again');
  }

  _onDeleteComplete() {
    this.toggleOverlay = false;
  }

  // special properties:
  rowClick(rowEvent) {
    // console.log('Clicked: ' ,  rowEvent.row.item);
    // console.log(this.dataTable);
  }

  rowDoubleClick(rowEvent) {
      // alert('Double clicked: ' + rowEvent.row.item.name);
  }

  rowTooltip(item) {
    return ''; // item.jobTitle;
  }

  /**
   * Callback for Delete Button
   */
  delete(item) {

    const sta = this.confirmModalService.open({
      title: this.config.confirmDelete.title,
      body: this.config.confirmDelete.body,
    });

    sta.result.then((result) => {
      if (result) {
        this.toggleOverlay = true;

        // CONTINUE DELETING
        this.service.destroy(item.id).subscribe(
          (response: any) => {

            this._onDeleteSuccess(response);
          },
          (error) => {
            this._onDeleteError(error);
          },
          () => {
            this._onDeleteComplete();
          }
        );
      }
    }, (reason) => {
      console.log(reason);
    });
  }

  /**
   * Callback for Edit Button
   */
  edit(objectId) {
    // this.formModeChange.emit(objectId);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.triggerListReload && changes.triggerListReload.currentValue) {
      this.reloadItems(this.defaultParams);
    }
  }

  ngOnInit() {
    // This prevents this error ->
    // ERROR Error: ExpressionChangedAfterItHasBeenCheckedError: Expression has changed after it was checked.
    if (this.loadListOnInit) {
      this.reloadItems({});
    }
  }
}
