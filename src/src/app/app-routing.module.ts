import { HomeComponent } from './components/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NoContentComponent } from './pages/no-content/no-content.component';
import { Route } from './core/theme/admin/admin-routing.service';
// const routes: Routes = [
const routes: Routes = Route.withShell([
    {
        path: '**',
        component: NoContentComponent,
    }
]);

@NgModule({
    imports: [RouterModule.forRoot(routes, {useHash: false, enableTracing: false })],
    exports: [RouterModule]
})
export class AppRoutingModule { }
