import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShiftTemplatePeriodsComponent } from './shift-template-periods.component';

describe('ShiftTemplatePeriodsComponent', () => {
  let component: ShiftTemplatePeriodsComponent;
  let fixture: ComponentFixture<ShiftTemplatePeriodsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShiftTemplatePeriodsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShiftTemplatePeriodsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
