import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class TaxTableService extends AbstractService {
    endpoint = 'table-tax';
}
