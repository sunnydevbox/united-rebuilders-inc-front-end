import { PayrollTemplateService } from '@services/payroll-template.service';

export const settings = {
    plural: 'Payroll Generator',
    singular: 'Payroll Generator',
    service: PayrollTemplateService,
  
    crud: {
      create: {
        allow: true, // true/false
        url: '/admin/roles/create'
      },
      read:{
        url: '/app/settings/system/payroll-generator/'
      },
      update:{
        allow: true,
        url: '/admin/roles/edit/'
      },
      delete:{
        allow: true
      },
      list: {
        allow: true,
        url: '/admin/roles'
      }
    }
  };