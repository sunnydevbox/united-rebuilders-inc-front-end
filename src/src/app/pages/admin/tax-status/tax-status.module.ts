import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TaxStatusComponent } from './tax-status.component';
import { RoutingModule } from './routing.module';

import { ListModule } from './crud/list/list.module';
import { FormModule } from './crud/form/form.module';
import { settings } from './settings';


@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    ListModule,
    FormModule
  ],
  declarations: [
    TaxStatusComponent,
  ],
  exports: [
    TaxStatusComponent,
  ],
  providers: [
    settings.service
  ]
})
export class TaxStatusModule { }
