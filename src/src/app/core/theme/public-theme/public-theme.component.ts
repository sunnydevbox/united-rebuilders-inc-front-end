import { Component, OnInit } from '@angular/core';

import { environment } from './../../../../environments/environment';

import { UserService } from '@services/user.service';

import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-public-theme',
  templateUrl: './public-theme.component.html',
  styleUrls: ['./public-theme.component.scss']
})
export class PublicThemeComponent implements OnInit {

  environment = environment;
  userSubscription: Subscription;
  user: null;
  isLoggedIn: Boolean = false;

  constructor(private userService: UserService) { }

  ngOnInit() {
    // this.user = this.userService.getCurrentUser();
    console.log('pub them core');
    this.isLoggedIn = this.userService.isLoggedIn();
    this.userSubscription = this.userService.user().subscribe(user => {
      this.user = user;
    });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
  }

}
