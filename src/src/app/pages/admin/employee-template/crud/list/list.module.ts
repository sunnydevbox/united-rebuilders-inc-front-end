import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { DataTableModule } from 'angular5-data-table';
import { OverlayModule } from '@shared/overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */


import { ListModule as BaseListModule } from '@shared/base/list/list.module' ;
import { ListComponent } from './list.component';
import {  RoutingModule } from './routing.module';

@NgModule({
  imports: [
    CommonModule,
    BaseListModule,
    DataTableModule,
    OverlayModule,
    NgbModule,
    RoutingModule,
  ],
  declarations: [
    ListComponent,
  ],
  exports: [
    ListComponent
  ]
})
export class ListModule { }
