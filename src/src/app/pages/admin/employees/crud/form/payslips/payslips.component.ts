import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { settings } from './settings';

@Component({
  selector: 'app-payslips',
  templateUrl: './payslips.component.html',
  styleUrls: ['./payslips.component.css']
})
export class PayslipsComponent implements OnChanges {

  public settings;

  @Input() object;

  constructor() { 
    this.settings = settings;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.object && changes.object.currentValue) {}
  }

}
