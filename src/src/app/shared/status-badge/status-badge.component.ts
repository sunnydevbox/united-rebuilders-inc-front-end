import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'status-badge',
  templateUrl: './status-badge.component.html',
  styleUrls: ['./status-badge.component.scss']
})
export class StatusBadgeComponent implements OnInit, OnChanges {

  @Input() status;
  @Input() class;
  @Input() customLabel;
  public className;
  public label;

  constructor() { }

  ngOnInit() {
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.status && changes.status.currentValue && typeof changes.status.currentValue !== 'undefined') {

      /*
        const STATE_UNAPPROVED = 'unapproved';
        const STATE_PARKED = 'parked';
        const STATE_PLACED = 'placed';
        const STATE_COSTED = 'costed';
        const STATE_RECEIPTED = 'receipted';
        const STATE_COMPLETE = 'complete';
        const STATE_DELETED = 'deleted';
      */
      switch (changes.status.currentValue.toLowerCase()) {

        case 'approved':
        
        case 'receipted':
        case 'complete':
        case 'active':

        case 'employed':
        case 'regular':
        case 'contractual':
        case 'probationary':
        case 'ojt':
        case 'computed':
        case 'applicant':
        case 'done':
        case 'yes':
          this.className = 'badge-success';
          break;

        case 'invalid':
        case 'deleted':
        case 'suspended':
        case 'no':
        case 'below40':
          this.className = 'badge-danger';
          break;

        case 'unapproved':
        case 'disapproved':
          this.className = 'badge-danger';
          changes.status.currentValue = 'disapproved';
          break;

        case 'placed':
          this.className = 'badge-warning text-success';
          break;

        case 'costed':
          this.className = 'badge-warning';
          changes.status.currentValue = 'approved';
          break;

        case 'parked':
        case 'inactive':
        case 'dismissed': // dismissed from employment
        default:
          this.className = 'badge-warning';
          break;
      }

      this.label = changes.status.currentValue.toUpperCase();

      if (changes.customLabel && changes.customLabel.currentValue.length) {
        this.label = changes.customLabel.currentValue;
      }
    }
  }

}
