import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { settings } from './settings';
import { SupplementDataService } from '@services/supplement-data.service';

@Component({
  selector: 'app-leave-applications',
  templateUrl: './leave-applications.component.html',
  styleUrls: ['./leave-applications.component.css']
})
export class LeaveApplicationsComponent implements OnInit {

  public settings;
  public suppData = {
    'leave-types': [],
    'shift-templates': [],
    'application-statuses': [],
  };
  public filters;
  public _object;

  @Output() _triggerReloadList = new EventEmitter();

  constructor(
    private suppService: SupplementDataService
  ) {
    this.settings = settings;
    suppService.index({
      data: 'leave-types;shift-templates;application-statuses'
    }).subscribe(
      (res) => {
        this.suppData = {
          'leave-types': res['leave-types'],
          'shift-templates': res['shift-templates'],
          'application-statuses': res['application-statuses']
        };
      }
    );
  }

  getObject(object) {
    this._object = object;
  }
  

  filtersChanged(changed) {
    console.log('changed', changed);
    this.filters = changed;
  }


  triggerReloadList(e) {
    if (e) {
      this._triggerReloadList = e;
    }
  }

  ngOnInit() { }

}
