import { PhilhealthTableService } from '@services/philhealth-table.service';

export const settings = {
    plural: 'Philhealth Table',
    singular: 'Philhealth Table',
    service: PhilhealthTableService,

    crud: {
      create: {
        allow: true, // true/false
        url: '/admin/settings/system/philhealth-table/create'
      },
      read: {

      },
      update: {
        allow: true,
        url: '/admin/settings/system/philhealth-table/edit/'
      },
      delete: {
        allow: true
      },
      list: {
        allow: true,
        url: '/admin/settings/system/philhealth-table'
      }
    }
  };
