import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class CashAdvanceLogService extends AbstractService {
    endpoint = 'cash-advance-logs';
}
