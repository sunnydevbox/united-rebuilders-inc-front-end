import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';


@Injectable()
export class ActivityLogService extends AbstractService {

  endpoint = 'activity-logs';

}
