import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';

export const MODEL = {
  'id' : [null],
  'type': ['employment'],

  'employment_type_id' : [null, Validators.compose([])],
  'employment_status_id' : [null, Validators.compose([])],
  'department_id' : [null, Validators.compose([])],
  'job_position_id' : [null, Validators.compose([])],
  'date_hired' : [null, Validators.compose([])],
  'date_dismissed' : [null, Validators.compose([])],
  'shift_template_id': [null, Validators.compose([])],
  'hourly_rate': [0, Validators.compose([])],
  'monthly_rate': [0, Validators.compose([])],
  'employee_template_id': [null, Validators.compose([Validators.required])],

  'sss_number': [null, Validators.compose([])],
  'tin_number': [null, Validators.compose([])],

};
