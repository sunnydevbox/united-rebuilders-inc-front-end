import { PayrollService } from '@services/payroll.service';

export const settings = {
    plural: 'Payslips',
    singular: 'Payslip',
    service: PayrollService,
    uriPrefix: 'Payslips',

    crud: {
      create: {
        allow: true, // true/false
        url: '/app/Payslips/add'
      },
      read: {

      },
      update: {
        allow: true,
        url: '/app/Payslips/edit/'
      },
      delete: {
        allow: true
      },
      list: {
        allow: true,
        url: '/app/Payslips'
      }
    }
  };
