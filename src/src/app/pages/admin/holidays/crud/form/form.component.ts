import { Component, Output, EventEmitter, Input, SimpleChanges, OnChanges } from '@angular/core';
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';
import { MODEL } from './../../model';
import { settings } from './../../settings';
import { ERRORS, datePickerOptions1 } from '@admin/config';
import { SupplementDataService } from '@services/supplement-data.service';
import { formatDateFix } from '@admin/config';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent extends BaseFormComponent implements OnChanges {

  @Output() triggerReloadList = new EventEmitter();
  @Input() object;

  private supplementDataService;
  public lists;
  public datePickerOptions = datePickerOptions1;

  _postConstruct() {
    this.supplementDataService = this.injector.get(SupplementDataService);
    this.service = this.injector.get(settings.service);
    this.form = this.formBuilder.group(MODEL);
    this.errors = ERRORS;

    this.supplementDataService.index({
      data: 'holiday-types'
    }).subscribe(
      (res) => {
        this.lists = res;
      }
    );
  }

  _preSubmit() {
    if (this.form.get('start_date').value) {
      this.form.get('start_date').setValue(this.form.get('start_date').value.formatted);
    }
  }

  _onSaveSuccess(res) {
    super._onSaveSuccess(res);

    // Trigger Relaod list
    this.triggerReloadList.emit(true);

    // Reset the form
    this.form.reset();
  }

  _onUpdateSuccess(res) {
    super._onUpdateSuccess(res);
    // Trigger Relaod list
    this.triggerReloadList.emit(true);

    // Reset the form
    this.objectId = null;
    this.form.reset();

    // REset back to create mode
    this.formMode = 'create';
  }

  cancel() {
    this.form.reset();
    this.objectId = null;
    this.formMode = 'create';
  }

  ngOnChanges(changes: SimpleChanges) {
    // console.log(' BASELIST ', changes);
    if (changes.object && changes.object.currentValue) {
      console.log(changes.object.currentValue);
      this.formMode = 'update';
      this.form.get('name').setValue(changes.object.currentValue.name);
      this.form.get('description').setValue(changes.object.currentValue.description);
      this.form.get('holiday_type_id').setValue(changes.object.currentValue.holiday_type_id);
      this.form.get('name').setValue(changes.object.currentValue.name);
      this.objectId = changes.object.currentValue.id;
      let start_date;
      if (changes.object.currentValue.start_date) {
        const bd = new Date(formatDateFix(changes.object.currentValue.start_date.date));
        start_date = {
          date: {
            year: bd.getFullYear(),
            month: bd.getMonth() + 1,
            day:  bd.getDate()
          }
        };

        this.form.get('start_date').setValue(start_date);
      }




    }
  }
}
