import { Component, OnInit, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';

// IMPORT THE BASE COMPONENT
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';

import { ERRORS, responseErrors } from '@admin/config';
import { MODEL } from './model';

import { EmployeeService } from '@services/employee.service';

@Component({
  selector: 'app-employee-id',
  templateUrl: './employee-id.component.html',
  styleUrls: ['./employee-id.component.css']
})
export class EmployeeIdComponent extends BaseFormComponent   implements OnChanges {

  @Input() object;
  @Output() objectChanged = new EventEmitter();


  _postConstruct() {
    this.formMode = 'update';
    this.service = this.injector.get(EmployeeService);
    this.errors = ERRORS;
    this.form = this.formBuilder.group(MODEL);

    this.listErrors = responseErrors;
  }

  _onUpdateSuccess(res) {
    super._onUpdateSuccess(res);

    // Update the parent Object
    this.object.employee_id_number = res.data.employee_id_number;

    this.form.get('employee_id_number').setValue(res.data.employee_id_number);
  }

  ngOnChanges(changes: SimpleChanges) {

    if (changes.object && changes.object.currentValue) {
      this.objectId = this.object.id;
      this.form.get('employee_id_number').setValue(changes.object.currentValue.employee_id_number);
    }

  }
}
