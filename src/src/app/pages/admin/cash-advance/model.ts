
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
  'employee_id': [null, Validators.compose([Validators.required])],
  'total_amount': [null, Validators.compose([Validators.required])],
  'payable_per_period': [null, Validators.compose([Validators.required])],
  'duration_start': [null, Validators.compose([Validators.required])],
  'duration_end': [null, Validators.compose([Validators.required])],
  'notes': [null, Validators.compose([Validators.required])],
}