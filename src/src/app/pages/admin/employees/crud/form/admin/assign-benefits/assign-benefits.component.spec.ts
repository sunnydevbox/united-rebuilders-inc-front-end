import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignBenefitsComponent } from './assign-benefits.component';

describe('AssignBenefitsComponent', () => {
  let component: AssignBenefitsComponent;
  let fixture: ComponentFixture<AssignBenefitsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignBenefitsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignBenefitsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
