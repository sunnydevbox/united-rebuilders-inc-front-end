import { JobPositionService } from '@services/job-position.service';

export const settings = {
    plural: 'Job Positions',
    singular: 'job position',
    service: JobPositionService,    
  
    crud: {
      create: {
        allow: true, // true/false
        url: '/app/system/job-positions'
      },
      read:{
  
      },
      update:{
        allow: true,
        url: '/app/system/job-positions/edit/'
      },
      delete:{
        allow: true
      },
      list: {
        allow: true,
        url: '/app/system/job-positions'
      }
    }
  };