
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
  'id' : [null],
  'compensation_from': [null],
  'compensation_to': [null],
  'monthly_salary_credit': [null],
  'social_security_er': [null],
  'social_security_ee': [null],
  'social_security_total': [null],
  'ec_er': [null],
  'total_contribution_er': [null],
  'total_contribution_ee': [null],
  'total_contribution_er_ee': [null],
  'total_contribution_se_vm_ofw': [null],
};
