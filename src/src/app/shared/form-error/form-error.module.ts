import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxErrorsModule } from '@ultimate/ngxerrors';
import { FormErrorComponent } from './form-error.component';
@NgModule({
  imports: [
    CommonModule,
    NgxErrorsModule
  ],
  declarations: [
    FormErrorComponent,
  ],
  exports: [
    FormErrorComponent,
  ]
})
export class FormErrorModule { }
