import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { Route } from './../../../core/route.service';

import { settings } from './settings';
import { UsersComponent } from './users.component';

import { CheckPermissionGuard } from './../../../core/guards/check-permissions.guard';

const routes: Routes = [
    {
        path: '',
        component: UsersComponent,
        data: { title: settings.plural  },
        children: [
          {
            path: '',
            loadChildren: '@admin/' + settings.uriPrefix + '/crud/list/list.module#ListModule',
            data: {
              permissions: [
                'user.browse'
              ]
            }
          },
          {
            path: 'add',
            loadChildren: '@admin/' + settings.uriPrefix + '/crud/form/form.module#FormModule',
            data: {
              formMode: 'create',
              permissions: [
                'user.add'
              ]
            }
          },
          {
            path: 'edit/:id',
            loadChildren: '@admin/' + settings.uriPrefix + '/crud/form/form.module#FormModule',
            data: {
              formMode: 'update',
              permissions: [
                'user.edit'
              ]
            }
          }
        ],
        canActivate: [
          CheckPermissionGuard,
        ],
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}
