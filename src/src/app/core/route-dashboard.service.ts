import { Routes } from '@angular/router';

import { DashboardComponent } from './theme/dashboard/dashboard.component';
import { AuthenticationGuard } from './authentication/authentication.guard';

import { CheckStaffRoleGuard } from './authentication/check-staff-role.guard';
/**
 * Provides helper methods to create routes.
 */
export class Route {

  /**
   * Creates routes using the shell component and authentication.
   * @param routes The routes to add.
   * @return {Routes} The new routes using shell as the base.
   */
  static withShell(routes: Routes, theme?: string): Routes {
    
      return [{
        path: '',
        component: DashboardComponent,
        children: routes,
        canActivate: [AuthenticationGuard, CheckStaffRoleGuard],
        // Reuse ShellComponent instance when navigating between child views
        data: { reuse: true }
      }];
  }

}
