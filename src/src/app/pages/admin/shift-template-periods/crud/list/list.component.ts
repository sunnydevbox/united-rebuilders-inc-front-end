import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { ListComponent as BaseListComponent } from '@shared/base/list/list.component' ;

import { settings } from './../../settings';
import { UserService } from '@services/user.service';
import { constants } from 'os';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseListComponent {

  public userService;
  public selectedDaysList = [];

  @Output() object = new EventEmitter();

  _postConstruct() {
    this.service = this.injector.get(settings.service);
    this.userService = this.injector.get(UserService);
    this.defaultParams = {
      with: 'shiftTemplate'
    };
  }

  selectedDays(item) {
    const days = [];
    const restDays = [];

    if (item.day_m) {
      days.push('M');
    } else {
      restDays.push('M');
    }

    if (item.day_t) {
      days.push('T');
    } else {
      restDays.push('T');
    }

    if (item.day_w) {
      days.push('W');
    } else {
      restDays.push('W');
    }

    if (item.day_th) {
      days.push('Th');
    } else {
      restDays.push('Th');
    }

    if (item.day_f) {
      days.push('F');
    } else {
      restDays.push('F');
    }

    if (item.day_s) {
      days.push('S');
    } else {
      restDays.push('S');
    }

    if (item.day_sun) {
      days.push('Sun');
    } else {
      restDays.push('Sun');
    }

    this.selectedDaysList = [
      days.join(', '),
      restDays.join(', '),
    ]
  }

  restDays(item) {
    const days = [];

    if (item.day_m) {
      days.push('M');
    }

    if (item.day_t) {
      days.push('T');
    }

    if (item.day_w) {
      days.push('W');
    }

    if (item.day_th) {
      days.push('Th');
    }

    if (item.day_f) {
      days.push('F');
    }
    if (item.day_s) {
      days.push('S');
    }

    if (item.day_sun) {
      days.push('Sun');
    }

    return days.join(', ');
  }

  edit(object) {
    this.object.emit(object);
  }

}
