import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutingModule } from './routing.module';
import { TaxComponent} from './tax.component';
import { ListModule } from './crud/list/list.module';
import { TaxTableService } from '@services/tax-table.service';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    ListModule,
  ],
  declarations: [
    TaxComponent,
  ],
  exports: [
    TaxComponent,
  ],
  providers: [
    TaxTableService,
  ]
})
export class TaxModule { }
