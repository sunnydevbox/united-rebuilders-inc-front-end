import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { Router } from '@angular/router';

import { AuthenticationService } from './../../core/authentication/authentication.service';
import { UserService } from '@services/user.service';
import { environment } from './../../../environments/environment';

@Injectable()
export class GuestOnlyGuard implements CanActivate {
  constructor(
    private authService: AuthenticationService,
    private userService: UserService,
    private router: Router
  ) {

  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {
    const isAuthenticated = this.authService.isAuthenticated();
    console.log(' IS AUTHENTIOCATED', isAuthenticated);
      // IF AUTHENTICATED THEN redirect to app page
    if (isAuthenticated) {
      if (environment.admin.ENABLED) {

        this.router.navigate([environment.admin.REDIRECT_URL]);
        // window.location.hash = '/' + environment.admin.REDIRECT_URL;
        // // window.location.href = '/#/login';//environment.admin.REDIRECT_URL;
        // console.log(window.location)
       // window.location.reload();
      }
      return false;
    } else {
      return true;
    }
  }
}
