import { Component, OnInit } from '@angular/core';

import { settings } from './settings';

@Component({
  selector: 'app-tax-status',
  templateUrl: './tax-status.component.html',
  styleUrls: ['./tax-status.component.css']
})
export class TaxStatusComponent implements OnInit {

  public settings; 
  public _triggerReloadList;
  public _object;

  constructor() { 
    this.settings = settings;
  }

  triggerReloadList(e) {
    if (e) {
      this._triggerReloadList = e;
    }
  }

  getObject(object) {
    this._object = object;
  }

  ngOnInit() {
  }
}
