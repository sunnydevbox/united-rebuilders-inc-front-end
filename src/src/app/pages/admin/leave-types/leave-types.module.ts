import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LeaveTypesComponent } from './leave-types.component';
import { RoutingModule } from './routing.module';
import { LeaveTypeService } from '@services/leave-type.service';
import { FormModule } from './crud/form/form.module';
import { ListModule } from './crud/list/list.module';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    FormModule,
    ListModule,
  ],
  declarations: [
    LeaveTypesComponent,
  ],
  exports: [
    LeaveTypesComponent,
  ],
  providers: [
    LeaveTypeService,
  ]
})
export class LeaveTypesModule { }
