import { Component, OnInit, ViewEncapsulation , OnDestroy, Renderer2, ElementRef } from '@angular/core';

import { ListComponent as BaseListComponent } from '@shared/base/list/list.component' ;
import { TimelogService } from '@services/timelog.service';
import { minutesToHoursMinutes } from '@admin/config';
import { UserService } from '@services/user.service';
import { EmployeeService } from '@services/employee.service';
import { ShiftTemplatePeriodService } from '@services/shift-template-period.service';
import { ERRORS, datePickerOptions1 } from '@admin/config';
import { MODEL } from '../update-form/model';
import { formatDateFix } from '@admin/config';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ListComponent extends BaseListComponent implements OnInit, OnDestroy {
  
  private employeeId = null;
  private employeeService;
  private shiftTemplatePeriodService;
  public datePickerOptions1;
  public shiftTemplatePeriodList;
  public employee;
  public userService;

  public renderer;
  public elementRef;

  _postConstruct() {
    this.userService = this.injector.get(UserService);
    this.datePickerOptions1 = datePickerOptions1;
    this.employeeService = this.injector.get(EmployeeService);
    this.service = this.injector.get(TimelogService);
    this.employeeId = this.route.snapshot.params.id;
    this.shiftTemplatePeriodService = this.injector.get(ShiftTemplatePeriodService);

    this.renderer = this.injector.get(Renderer2);
    this.elementRef = this.injector.get(ElementRef);


    // this.renderer.listen('.select-column-header', 'click', (e) => {
    //   console.log('e ', e);
    // });

    this.renderer.listen

    this.employeeService.show(this.employeeId, {
      with: 'shift_templates'
    }).subscribe(
      (res) => {
        this.employee = res.data;
      },
      (err) => {
        
      }
    )

    this.shiftTemplatePeriodService.perEmployee(this.employeeId).subscribe(
      (res) => {
        this.shiftTemplatePeriodList = res.data;
      }
    )

    this.defaultParams = {
      filter: 'id;status;time_in;shift_template_period_id;ot_application_id;'
              + 'time_elapsed;tardy_minutes;overtime_minutes;billable_minutes;is_rest_day;'
              + 'time_out;employee_id;is_approved;is_payroll_generated;is_auto_approve',
      with: 'employee.shift_templates;shiftTemplatePeriod.shift_template;leave_application',
      orderBy: 'time_in',
      sortedBy: 'desc',
      search: this.employeeId,
      searchFields: 'employee_id',
    };
  }

  formatDateFix(v) {
    return formatDateFix(v);
  }

  triggerReloadList(e) {
    this.triggerListReloadChange.emit(true);
  }

  filtersChanged(e) {
    this.defaultParams['query'] = e.query;
    this.reloadItems({});
  }

  updateObject(object) {
    object = object.data;

    this.items.forEach((v,k) => {
      if (v.id == object.id) {
        this.items[k] = object;
        return;
      }
    });

  }

  headerClick(e) {
    console.log(e, this.dataTable);
  }

  setStatus(action, id) {
    if (action == 'delete') {

      this.service.destroy(id).subscribe(
        (res) => {
          this.toastr.success('Timelog delete');
        }
      )
    } else if (action == 'approved') {
      this.service.setStatus(id, action).subscribe(
        (res) => {
          this.updateObject(res);
          this.toastr.success('Time log approved');
        },
        (err) => {
          console.log(err);
          this.toastr.danger('There was a problem updating the status');
        }
      )
    }
  }

  minutesToHoursMinutes(minutes) {
    return minutesToHoursMinutes(minutes);
  }

  ngOnDestroy() {
    // this.service.unsubscribe();
  }
}
