import { Component, Output, EventEmitter, Input, SimpleChanges, OnChanges } from '@angular/core';

import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';

import { MODEL } from './../../model';
import { settings } from './../../settings';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent extends BaseFormComponent {
  public object;
  public settings;

  @Output() triggerReloadList = new EventEmitter();
  @Input() selectedObject;

  _postConstruct() {
    this.settings = settings;
    this.service = this.injector.get(settings.service);
    this.form = this.formBuilder.group(MODEL);
    
    this.formMode = this.route.snapshot.data.formMode;
    if (this.formMode == 'update') {
      this.toggleOverlay = true;
      
      this.objectId = this.router.parseUrl(this.router.url).root.children.primary.segments[5];
      this.service.show(this.objectId).subscribe(
        (res) => {
          this.form.controls.name.setValue(res.data.name);
          this.form.controls.description.setValue(res.data.description);
    
          for (let field in res.data) {
            if (field == 'tax_period') {
              this.form.get('tax_period').setValue((!res.data[field] || res.data[field] == 0) ? 0 : res.data[field]);
            } 
            // Checkboxes
            else if ([
                'deduction_philhealth','deduction_sss','deduction_tardiness','deduction_bir','deduction_absences','deduction_pag_ibig', 'deduction_cash_advance',
                'benefit_basic_pay','benefit_overtime_pay','benefit_holiday_pay','benefit_night_differential_pay'
              ].indexOf(field) >= 0
            ) {
              this.clickedCheckbox({
                name: field,
                value: res.data[field] ? 1 : 0,
              })
            } else {
              let f = this.form.get(field);
              if (f) {
               this.form.get(field).setValue(res.data[field]);
              }
            }
          }
        },
        (error) => {
          console.log(error)
        },
        () => {
          this.toggleOverlay = false;
        }
      )
    }
    
  }


  clickedCheckbox(e) {
    if (e.target) {
      console.log(e.target.checked);
      this.form.get(e.target.name).setValue(e.target.checked ? 1 : 0);
    } else {
      //console.log(this.form.get(e.name));
      this.form.get(e.name).setValue(e.value ? 1 : 0);
    }
  }

  setCheckboxState(field) {
    if (!this.selectedObject) {
      return false;
    }

    if (this.selectedObject[field]) {
      return true;
    }

    return false;
  }

  _preSubmit() {
    if (this.form.get('tax_period').value == 0) {
      this.form.get('tax_period').setValue(null);
    }
  }

  _onSaveSuccess(res) {
    super._onSaveSuccess(res);
    
    // Reset the form
    this.form.reset();
    this.router.navigate([this.settings.crud.update.url + res.data.id]);
  }
}
