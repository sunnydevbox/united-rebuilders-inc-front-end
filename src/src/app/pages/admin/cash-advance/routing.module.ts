import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { Route } from './../../../core/route.service';

import { ListComponent } from './crud/list/list.component';
import { FormComponent } from './crud/form/form.component';
import { CashAdvanceComponent } from './cash-advance.component';

const routes: Routes = [
    {
        path: '',
        component: CashAdvanceComponent,
        data: { title: 'Cash Advances'  },
        children:[
            {
              path: '',
              component: ListComponent,
            },
            {
              path: 'add',
              component: FormComponent,
              data: { formMode: 'create' }
            },
            {
              path: 'edit/:id',
              component: FormComponent,
              data: { formMode: 'update' }
            },
          ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}