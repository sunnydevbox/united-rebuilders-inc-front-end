import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';

// IMPORT THE BASE COMPONENT
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';

// Others..
// import { settings } from './../../settings';

import { IMyDpOptions, IMyDateModel } from 'mydatepicker';

import { ERRORS } from '@admin/config';
import { MODEL } from './model';
import { EmployeeService } from '@services/employee.service';

@Component({
  selector: 'app-manage-password',
  templateUrl: './manage-password.component.html',
  styleUrls: ['./manage-password.component.css']
})
export class ManagePasswordComponent extends BaseFormComponent implements OnChanges {

  @Input() object;

  _postConstruct() {
    this.service = this.injector.get(EmployeeService);
    this.form = this.formBuilder.group(MODEL);
    this.errors = ERRORS;
    this.formMode = 'update';
  }

  _onUpdateSuccess(res) {
    super._onUpdateSuccess(res);

    this.resetForm();
  }


  ngOnChanges(changes: SimpleChanges) {
    if (changes.object && changes.object.currentValue) {
      this.objectId = changes.object.currentValue.id;
    }
  }

}
