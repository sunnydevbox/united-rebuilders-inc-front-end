import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayrollTemplatesComponent } from './payroll-templates.component';

describe('PayrollTemplatesComponent', () => {
  let component: PayrollTemplatesComponent;
  let fixture: ComponentFixture<PayrollTemplatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayrollTemplatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayrollTemplatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
