import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoutingModule } from './routing.module';
import { UsersComponent } from './users.component';

import { CheckPermissionGuard } from './../../../core/guards/check-permissions.guard';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule
  ],
  declarations: [
    UsersComponent,
  ],
  providers: [
    CheckPermissionGuard,
  ]
})
export class UsersModule { }
