import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoutingModule } from './routing.module';
import { HolidaysComponent } from './holidays.component';

import { CalendarListModule } from './crud/calendar-list/calendar-list.module';
import { FormModule } from './crud/form/form.module';
import { ListModule } from './crud/list/list.module';
import { FilterModule } from './crud/filter/filter.module';
import { HolidayService } from '@services/holiday.service';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    CalendarListModule,
    FormModule,
    ListModule,
    FilterModule,
  ],
  declarations: [
    HolidaysComponent,
  ],
  exports: [
    HolidaysComponent,
  ],
  providers: [
    HolidayService,
  ]
})
export class HolidaysModule { }
