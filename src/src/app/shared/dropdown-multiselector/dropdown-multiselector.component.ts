import { Component, OnInit, Input, Output, OnChanges, SimpleChanges, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-dropdown-multiselector',
  templateUrl: './dropdown-multiselector.component.html',
  styleUrls: ['./dropdown-multiselector.component.css']
})
export class DropdownMultiselectorComponent implements OnInit, OnChanges {

  @Input() dropdownList = [];
  @Input() selectedItems = [];
  @Input() customSettings = [];
  @Output() selectedItem = new EventEmitter();
  @Input() disabled = false;
  
  public dropdownSettings = {
    singleSelection: false,
    idField: 'item_id',
    textField: 'item_text',
    selectAllText: 'Select All',
    unSelectAllText: 'UnSelect All',
    itemsShowLimit: 3,
    allowSearchFilter: true
  };

  constructor() { }

  ngOnInit() {
  }

  onItemSelect(item: any) {
    this.selectedItem.emit({
      id: item.item_id,
      selected: true,
    });
  }

  onItemDeSelect(item: any) {
    this.selectedItem.emit({
      id: item.item_id,
      selected: false,
    });
  }

  onSelectAll(items: any) {
    items.forEach((row, index) => {
      items[index] = {
        id: row.item_id,
        selected: true,
      };
    });
    this.selectedItem.emit(items);
  }

  onDeSelectAll(e) {
    let items = [];
    this.dropdownList.forEach((row, index) => {
      items[index] = {
        id: row.item_id,
        selected: false,
      };
    });
    this.selectedItem.emit(items);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.customSettings && changes.customSettings.currentValue) {
      // this.dropdownSettings = {...this.dropdownSettings, ...this.customSettings};
    }

    if (changes.dropdownList && changes.dropdownList.currentValue) {
      // ADD A little formatter
      changes.dropdownList.currentValue.forEach((row, index) => {
        if (!row.item_id) {
          row.item_id = row.id;
          changes.dropdownList.currentValue[index] = {...row};
        }

        if (!row.item_text) {
          row.item_text = row.name;
          changes.dropdownList.currentValue[index] = {...row};
        }
      });
    }

    if (changes.selectedItems && changes.selectedItems.currentValue) {

      // ADD A little formatter
      changes.selectedItems.currentValue.forEach((row, index) => {
        if (!row.item_id) {
          row.item_id = row.id;
          changes.selectedItems.currentValue[index] = {...row};
        }

        if (!row.item_text) {
          row.item_text = row.name;
          changes.selectedItems.currentValue[index] = {...row};
        }
      });

      
    }
  }

}
