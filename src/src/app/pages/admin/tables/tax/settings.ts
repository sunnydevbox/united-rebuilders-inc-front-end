import { TaxTableService } from '@services/tax-table.service';

export const settings = {
    plural: 'Tax Table',
    singular: 'Tax Table',
    service: TaxTableService,

    crud: {
      create: {
        allow: true, // true/false
        url: '/admin/settings/system/sss-table/create'
      },
      read: {

      },
      update: {
        allow: true,
        url: '/admin/settings/system/sss-table/edit/'
      },
      delete: {
        allow: true
      },
      list: {
        allow: true,
        url: '/admin/settings/system/sss-table'
      }
    }
  };
