import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';

export const MODEL = {
  'id' : [null],
  'type': ['password'],
  'password': [null, Validators.compose([Validators.minLength(8)])],
  'retype_password': [null, Validators.compose([Validators.minLength(8)])],

};
