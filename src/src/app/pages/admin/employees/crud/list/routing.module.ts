import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { Route } from './../../../../../core/route.service';

import { ListComponent } from './list.component';
// import { ListComponent, EditComponent, CreateComponent } from './crud';
// import { UnitsComponent } from './units.component';

const routes: Routes = [
    {
        path: '',
        data: { title: 'View Units'  },
        component: ListComponent
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}
