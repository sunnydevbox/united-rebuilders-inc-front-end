import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { Route } from './../../../core/route.service';

import { ShiftTemplatePeriodsComponent } from './shift-template-periods.component';

const routes: Routes = [
    {
        path: '',
        data: { title: 'Roles'  },
        children: [
            {
              path: '',
              component: ShiftTemplatePeriodsComponent,
            },
          ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}
