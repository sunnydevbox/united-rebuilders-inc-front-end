import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';

import { environment } from './../../environments/environment';
import { UserService } from '@services/user.service';
// import { AuthenticationService } from './../core/authentication/authentication.service';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
  private userService: UserService;
  private router: Router;

  constructor(private injector: Injector) {
    setTimeout(() => {
      this.userService = injector.get(UserService);
      this.router = injector.get(Router);
    });
  }


  intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const authReq = this.authenticateRequest(req);

    return next.handle(authReq)
    .do(
      evt => {
        // console.log(evt)
      },
      (error: any) => {
        if (error instanceof HttpErrorResponse) {
          if (error.status === 401) {
            // this.router.navigate([environment.BASE_URL + environment.LOGIN_URL]);
          }
        }

      },
    );
  }

  authenticateRequest(req: any) {

    let headers = req.headers.set('Accept', environment.API_HEADER_ACCEPT);

    // PATCH
    if (!this.userService) {
      this.userService = this.injector.get(UserService);
    }
      const user = this.userService.getStoredCredentials();
      if (typeof user.token !== 'undefined') {
        headers = headers.set('Authorization', 'Bearer ' + user.token);
      }

      let contentType = 'application/json';

      let authReq;

      // FOR UPLOAD
      if (req.reportProgress) {
        contentType = 'application/x-www-form-urlencoded';
        headers = headers.set('enctype', 'multipart/form-data');
        // headers = headers.set('enctype', contentType);
        authReq = req.clone({
          headers: headers
        });
      } else {
        authReq = req.clone({
          headers: headers.set('Content-type', contentType)
        });
      }

    return authReq;

  }
}
