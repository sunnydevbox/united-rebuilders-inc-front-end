import { Component, OnInit } from '@angular/core';

// IMPORT THE BASE COMPONENT
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';

// IMPORT THE MODEL DATA
import { MODEL, ERRORS } from './../../model';

// Others..
import { settings } from './../../settings';
import { SupplementDataService  } from '@services/supplement-data.service';
import { UserService } from '@services/user.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent extends BaseFormComponent implements OnInit {

  public formMode = 'create';
  public settings;
  public object;

  private supplementDataService;
  public lists = [];
  public userService;


  _postConstruct() {
    this.settings = settings;

    this.listUrl = settings.crud.list.url;

    this.service = this.injector.get(settings.service);
    this.userService = this.injector.get(UserService);

    this.form = this.formBuilder.group(MODEL);
    this.errors = ERRORS;

    this.supplementDataService = this.injector.get(SupplementDataService);

    this.supplementDataService.index({
      data: 'roles',
    }).subscribe(
      (res) => {
        this.lists['roles'] = res.roles;
      }
    );

  }

  /** OVERRIDE **/
  submit() {
    // Overlay
    this.toggleOverlay = true;
    this.formSubmitting = true;

    if (this.formMode === 'update') {
      this.service.update(this.objectId, this.form.value).subscribe(
        (res: any) => {
          this._onSaveSuccess(res);
        },
        (err: any) => {
          this._onSaveError(err);
        },
        () => {
          this._onSaveComplete();
        }
      );
    } else {

      this.service.register(this.form.value).subscribe(
        (res: any) => {
          this._onSaveSuccess(res);
        },
        (err: any) => {
          this._onSaveError(err);
        },
        () => {
          this._onSaveComplete();
        }
      );
    }
  }

  ngOnInit() {
    this.formMode = this.route.snapshot.data.formMode;

    if (this.formMode === 'update') {
      // LOAD the record

      this.toggleOverlay = true;

      this.objectId = this.router.parseUrl(this.router.url).root.children.primary.segments[5];

      this.service.show(this.objectId).subscribe(
        (response) => {
          this.object = response.data;

          // POPULATE THE FORM
          this.form.controls.id.setValue(response.data.id);
          this.form.controls.first_name.setValue(response.data.first_name);
          this.form.controls.last_name.setValue(response.data.last_name);
          this.form.controls.email.setValue(response.data.email);
        },

        (error) => {

        },

        () => {
          this.toggleOverlay = false;
        },

      );

    }

  }
}
