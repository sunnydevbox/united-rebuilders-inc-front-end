import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@shared/forms.module';

/*****************************************/
/**** IMPORT THE BASE MODULE for FORM ****/
/*****************************************/
/***/
/***/ import { FormModule as BaseFormModule } from '@shared/base/form/form.module';
/***/
/** INSTRUCTED FROM BASE FORM MODULE */
/***/
/***/ import { OverlayModule } from '@shared/overlay/overlay.module';
/***/

/*****************************************/
/***/ // import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { MyDatePickerModule } from 'mydatepicker';
import { DetailsComponent } from './details.component';

@NgModule({
  imports: [
    CommonModule,

    BaseFormModule,
    OverlayModule,
    FormsModule,
    // NgbModule,

    MyDatePickerModule,
  ],
  declarations: [
    DetailsComponent,
  ],
  exports: [
    DetailsComponent,
  ]
})
export class DetailsModule { }
