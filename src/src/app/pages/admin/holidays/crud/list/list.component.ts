import { Component, OnInit, Output, EventEmitter, Input, SimpleChanges, OnChanges } from '@angular/core';
import { ListComponent as BaseListComponent } from '@shared/base/list/list.component' ;
import { settings } from './../../settings';
import { UserService } from '@services/user.service';
import { formatDateFix } from '@admin/config';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseListComponent implements OnChanges {

  public userService;

  @Output() objectChanged = new EventEmitter();
  @Input() selectedYear;

  _postConstruct() {
    this.service = this.injector.get(settings.service);
    this.userService = this.injector.get(UserService);
  }

  formatDateFix(v) {
    return formatDateFix(v);
  }

  edit(object) {
    this.objectChanged.emit(object);
  }


  ngOnChanges(changes: SimpleChanges) {
    super.ngOnChanges(changes);
    
    if (changes.selectedYear && changes.selectedYear.currentValue) {
      let start = this.selectedYear + '-' + 1 + '-' + 1;
      let end = this.selectedYear + '-' + 12 + '-' + 31;

      this.defaultParams = {
        orderBy: 'start_date',
        sortedBy: 'desc',
        with: 'holiday_type',
        dateRange: start + '|' + end
      }

      this.reloadItems(this.defaultParams);
    }
  }
}
