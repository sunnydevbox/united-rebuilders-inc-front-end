import { TimelogService } from '@services/timelog.service';

export const settings = {
    plural: 'Timelogs',
    singular: 'Timelog',
    service: TimelogService,

    crud: {
      create: {
        allow: true, // true/false
        url: '/app/system/timelogs'
      },
      read: {

      },
      update: {
        allow: true,
        url: '/app/system/timelogs/edit/'
      },
      delete: {
        allow: true
      },
      list: {
        allow: true,
        url: '/app/system/timelogs'
      }
    }
  };
