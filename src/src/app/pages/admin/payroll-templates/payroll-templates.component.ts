import { Component, OnInit } from '@angular/core';
import { settings } from './settings';

@Component({
  selector: 'app-payroll-templates',
  templateUrl: './payroll-templates.component.html',
  styleUrls: ['./payroll-templates.component.css']
})
export class PayrollTemplatesComponent implements OnInit {
  public settings;
  public _triggerReloadList;
  public _object;

  constructor() {
    this.settings = settings;
  }

  triggerReloadList(e) {
    if (e) {
      this._triggerReloadList = e;
    }
  }

  getObject(object) {
    this._object = object;
  }

  ngOnInit() {
  }

}
