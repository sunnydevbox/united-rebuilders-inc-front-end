import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomPayrollPeriodFormComponent } from './custom-payroll-period-form.component';
import { MyDatePickerModule } from 'mydatepicker';
import { FormModule as BaseFormModule } from '@shared/base/form/form.module';
import { FormsModule } from '@shared/forms.module';

/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { OverlayModule } from '@shared/overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */
import { settings } from './settings';

@NgModule({
  imports: [
    CommonModule,
    MyDatePickerModule,
    FormsModule,
    BaseFormModule,
    OverlayModule,
    NgbModule,
  ],
  declarations: [
    CustomPayrollPeriodFormComponent
  ],
  exports: [
    CustomPayrollPeriodFormComponent
  ],
  providers: [
    settings.service
  ],
})
export class CustomPayrollPeriodFormModule { }
