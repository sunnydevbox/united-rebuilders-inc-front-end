import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class HolidayTypeService extends AbstractService {
    endpoint = 'holiday-types';
}
