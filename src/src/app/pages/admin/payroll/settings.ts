import { PayrollService } from '@services/payroll.service';

export const settings = {
    plural: 'Payrolls',
    singular: 'Payroll',
    service: PayrollService,
  
    crud: {
      create: {
        allow: true, // true/false
        url: '/app/settings/system/payrolls/create'
      },
      read:{
        url: '/app/settings/system/payrolls/'
      },
      update:{
        allow: true,
        url: '/app/settings/system/payrolls/edit/'
      },
      delete:{
        allow: true
      },
      list: {
        allow: true,
        url: '/app/settings/system/payrolls/'
      },
      generate: {
        url: '/app/settings/system/payrolls/generate-report'
      }
    }
  };