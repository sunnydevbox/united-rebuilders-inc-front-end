import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class LeaveTypeService extends AbstractService {
    endpoint = 'leave-types';
}
