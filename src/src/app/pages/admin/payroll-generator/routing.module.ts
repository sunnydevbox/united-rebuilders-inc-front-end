import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { Route } from './../../../core/route.service';

import { PayrollGeneratorComponent } from './payroll-generator.component';

const routes: Routes = [
    {
        path: '',
        //component: ListComponent,
        data: { title: 'Roles'  },
        children:[
            {
              path: '',
              component: PayrollGeneratorComponent,
            }
          ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}