import { TitleService } from '@services/title.service';

export const settings = {
    plural: 'Titles',
    singular: 'Title',
    service: TitleService,    
  
    crud: {
      create: {
        allow: true, // true/false
        url: '/app/system/titles'
      },
      read:{
  
      },
      update:{
        allow: true,
        url: '/app/system/titles/edit/'
      },
      delete:{
        allow: true
      },
      list: {
        allow: true,
        url: '/app/system/titles'
      }
    }
  };