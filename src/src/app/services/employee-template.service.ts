import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class EmployeeTemplateService extends AbstractService {
    endpoint = 'employee-templates';
}
