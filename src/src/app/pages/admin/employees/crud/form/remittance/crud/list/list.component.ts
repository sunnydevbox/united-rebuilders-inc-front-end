import { Component, OnInit, Output, EventEmitter, SimpleChanges, Input, OnChanges } from '@angular/core';

import { ListComponent as BaseListComponent } from '@shared/base/list/list.component' ;

// import { settings } from './../../settings';
import { EmployeeDeductionsService } from '@services/employee-deductions.service';
import { UserService } from '@services/user.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseListComponent implements OnChanges {

  public userService;
  public objectId;
  @Input() object;
  // @Output() object = new EventEmitter();

  _postConstruct() {
    // this.service = this.injector.get(settings.service);
    this.service = this.injector.get(EmployeeDeductionsService);
    this.userService = this.injector.get(UserService);
  }


  // _onSuccess(res) {
  //   this.items = (typeof res['data'] !== 'undefined') ? res.data[0].deductions : res[0].deductions;
  //   if (typeof res['meta'] !== 'undefined') {
  //     this.itemCount = res.meta.pagination.total;
  //   }
  // }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.object && changes.object.currentValue) {
      this.objectId = changes.object.currentValue.id;

      this.defaultParams = {
        search        : this.objectId,
        searchFields  : 'employee_id',
        with          : 'deduction',
      };
    }
  }

  // edit(object) {
  //   this.object.emit(object);
  // }

}
