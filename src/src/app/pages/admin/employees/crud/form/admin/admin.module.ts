import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormModule as BaseFormModule } from '@shared/base/form/form.module';

/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { OverlayModule } from '@shared/overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */

import { AdminComponent } from './admin.component';

import { ManagePasswordModule  } from './manage-password/manage-password.module';
import { DeleteEmployeeModule  } from './delete-employee/delete-employee.module';
import { EmployeeIdModule  } from './employee-id/employee-id.module';
import { AssignDeductionsModule } from './assign-deductions/assign-deductions.module';
import { BiofieldModule } from './biofield/biofield.module';
import { AssignBenefitsModule } from './assign-benefits/assign-benefits.module';

@NgModule({
  imports: [
    CommonModule,
    OverlayModule,
    NgbModule,
    BaseFormModule,
    ManagePasswordModule,
    DeleteEmployeeModule,
    EmployeeIdModule,
    AssignDeductionsModule,
    BiofieldModule,
    AssignBenefitsModule,
  ],
  declarations: [
    AdminComponent,
  ],
  exports: [
    AdminComponent,
  ],
  providers: [

  ]
})
export class AdminModule { }
