import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class DeductionService extends AbstractService {
    endpoint = 'deductions';
}
