import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ShiftTemplatesComponent } from './shift-templates.component';
import { ShiftTemplateService } from '@services/shift-template.service';
import { RoutingModule } from './routing.module';
import { ListModule } from './crud/list/list.module';
import { FormModule } from './crud/form/form.module';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    ListModule,
    FormModule,
  ],
  declarations: [
    ShiftTemplatesComponent,
  ],
  exports: [
    ShiftTemplatesComponent,
  ],
  providers: [
    ShiftTemplateService,
  ],
})
export class ShiftTemplatesModule { }
