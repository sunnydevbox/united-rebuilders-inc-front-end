import { DeductionService } from '@services/deductions.service';

export const settings = {
    plural: 'Deductions',
    singular: 'Deduction',
    service: DeductionService,    
  
    crud: {
      create: {
        allow: true, // true/false
        url: '/app/system/deductions'
      },
      read:{
  
      },
      update:{
        allow: true,
        url: '/app/system/deductions/edit/'
      },
      delete:{
        allow: true
      },
      list: {
        allow: true,
        url: '/app/system/deductions'
      }
    }
  };