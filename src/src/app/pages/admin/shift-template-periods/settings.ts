import { ShiftTemplatePeriodService } from '@services/shift-template-period.service';

export const settings = {
    plural: 'Shift Template Periods',
    singular: 'Shift Template Period',
    service: ShiftTemplatePeriodService,

    crud: {
      create: {
        allow: true, // true/false
        url: '/admin/settings/system/shift-template-periods/create'
      },
      read: {

      },
      update: {
        allow: true,
        url: '/admin/settings/system/shift-template-periods/edit/'
      },
      delete: {
        allow: true
      },
      list: {
        allow: true,
        url: '/admin/settings/system/shift-template-periods'
      }
    }
  };
