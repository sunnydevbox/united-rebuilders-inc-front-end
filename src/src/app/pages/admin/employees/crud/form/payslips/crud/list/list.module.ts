import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoutingModule } from './routing.module';
import { ListComponent } from './list.component';

import { settings } from './../../settings';

/*****************************************/
/**** IMPORT THE BASE MODULE for LIST ****/
/*****************************************/
/***/
/***/ import { ListModule as BaseListModule } from '@shared/base/list/list.module';
/***/
/** INSTRUCTED FROM BASE LIST MODULE */
/***/
/***/ import { DataTableModule } from 'angular5-data-table';
/***/ import { OverlayModule } from '@shared/overlay/overlay.module';
/***/
/*****************************************/
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { StatusBadgeModule } from '@shared/status-badge/status-badge.module';

import { SupplementDataService } from '@services/supplement-data.service';


@NgModule({
  imports: [
    CommonModule,
    OverlayModule,
    DataTableModule,
    RoutingModule,
    BaseListModule,
    StatusBadgeModule,
    NgbModule,
  ],
  declarations: [
    ListComponent,
  ],
  exports: [
    ListComponent,
  ],
  providers: [
    settings.service,
    SupplementDataService,
  ]
})
export class ListModule { }
