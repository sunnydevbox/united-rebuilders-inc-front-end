import { Component, OnInit } from '@angular/core';

import { settings } from './settings';

@Component({
  selector: 'app-shift-template-periods',
  templateUrl: './shift-template-periods.component.html',
  styleUrls: ['./shift-template-periods.component.css']
})
export class ShiftTemplatePeriodsComponent implements OnInit {

  public settings;
  public _triggerReloadList;
  public _object;

  constructor() {
    this.settings = settings;
  }

  triggerReloadList(e) {
    if (e) {
      this._triggerReloadList = e;
    }
  }

  getObject(object) {
    this._object = object;
  }

  ngOnInit() {

  }
}
