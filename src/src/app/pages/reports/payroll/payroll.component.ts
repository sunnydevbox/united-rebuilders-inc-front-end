import { Component, OnInit } from '@angular/core';
import { ListComponent as BaseListComponent } from '@shared/base/list/list.component' ;
import { settings } from './settings';

@Component({
  selector: 'app-payroll',
  templateUrl: './payroll.component.html',
  styleUrls: ['./payroll.component.css']
})
export class PayrollComponent extends BaseListComponent {

  public payrollLogId;

  _postConstruct() { 
    this.loadListOnInit = false;
    this.service = this.injector.get(settings.service)
    
    this.items = [];
  }

  payrollLogChanged(payrollLogId) {
    this.payrollLogId = payrollLogId;
  }

  objectChanged(e) {
    this.items = e;
  }
}
