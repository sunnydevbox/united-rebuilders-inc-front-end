import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list.component';

/*****************************************/
/**** IMPORT THE BASE MODULE for LIST ****/
/*****************************************/
/***/
/***/ import { ListModule as BaseListModule } from '@shared/base/list/list.module';
/***/
/** INSTRUCTED FROM BASE LIST MODULE */
/***/
/***/ import { DataTableModule } from 'angular5-data-table';
/***/ import { OverlayModule } from '@shared/overlay/overlay.module';
/***/
/*****************************************/

import { PayrollService } from '@services/payroll.service';

@NgModule({
  declarations: [
    ListComponent
  ],
  exports: [
    ListComponent,
  ],
  imports: [
    CommonModule,
    BaseListModule,
    OverlayModule,
  ],
  providers: [
    PayrollService,
  ]
})
export class ListModule { }
