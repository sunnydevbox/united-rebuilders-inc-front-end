import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { TimelogsComponent } from './timelogs.component';

const routes: Routes = [
    {
        path: '',
        data: { title: 'Timelog'  },
        component: TimelogsComponent,
        children: [
            {
              path: '',
              loadChildren: './landing/landing.module#LandingModule',
            },
            {
              path: ':id',
              loadChildren: './crud/employee-logs-list/employee-logs-list.module#EmployeeLogsListModule',
            },
          ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {}
