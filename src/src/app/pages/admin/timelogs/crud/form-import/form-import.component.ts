import { Component, EventEmitter, Output } from '@angular/core';
import { RequestOptionsArgs, Headers } from '@angular/http';

import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';
import { ERRORS, responseErrors } from '@admin/config';
import { MODEL } from './model';
import { TimelogService } from '@services/timelog.service';
import { ProgressHttp } from 'angular-progress-http';

@Component({
  selector: 'app-form-import',
  templateUrl: './form-import.component.html',
  styleUrls: ['./form-import.component.css']
})
export class FormImportComponent extends BaseFormComponent {

  private http: ProgressHttp;
  public uploadProgress = null;
  private options: RequestOptionsArgs;

  @Output() triggerReload = new EventEmitter();

  _postConstruct() {
    this.errors = ERRORS;
    this.form = this.formBuilder.group(MODEL);
    this.service = this.injector.get(TimelogService);
    this.http = this.injector.get(ProgressHttp);    
  }

  onFileChanged(event) {
    const file = event.target.files[0];
    const formdata: FormData = new FormData();
    formdata.append('dtr_csv', file);
    this.uploadProgress = 'Uploading...';
    
    this.service.upload2('/import', formdata).subscribe(
      (result) => {
        this.toastr.success('DTR Imported')
        this.form.reset();

        this.triggerReload.emit(true);
      },
      (err) => {
        this.toastr.error(responseErrors[err.error.message]);
        this.form.reset();
      },
      () => {
        this.uploadProgress = null;
      }
    );
  }

}
