import { Component, OnInit } from '@angular/core';
import { settings } from './settings';

@Component({
  selector: 'app-philhealth-table',
  templateUrl: './philhealth-table.component.html',
  styleUrls: ['./philhealth-table.component.css']
})
export class PhilhealthTableComponent implements OnInit {
  public settings;

  constructor() { 
    this.settings = settings;
  }

  ngOnInit() {
  }

}
