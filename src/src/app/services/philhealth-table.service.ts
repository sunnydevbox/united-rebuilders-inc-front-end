import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class PhilhealthTableService extends AbstractService {
    endpoint = 'philhealth';
}
