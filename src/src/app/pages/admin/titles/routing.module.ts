import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { Route } from './../../../core/route.service';

import { ListComponent } from './crud/list/list.component';
import { FormComponent } from './crud/form/form.component';
import { TitlesComponent } from './titles.component';

const routes: Routes = [
    {
        path: '',
        //component: ListComponent,
        data: { title: 'Title'  },
        children:[
            {
              path: '',
              component: TitlesComponent,
            },
            {
              path: 'edit/:id',
              component: FormComponent,
              data: { title: 'Edit Title'  },
            },
          ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}