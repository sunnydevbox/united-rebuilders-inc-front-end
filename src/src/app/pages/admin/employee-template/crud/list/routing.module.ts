import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { ListComponent } from './list.component';

const routes: Routes = [
  {
      path: '',
      data: { title: 'Roles'  },
      children: [
          {
            path: '', 
            component: ListComponent
            ,
          },
        ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}
