import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class EmploymentTypeService extends AbstractService {
    endpoint = 'employment-types';
}
