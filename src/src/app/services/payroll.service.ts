import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';
import { HttpHeaders } from '@angular/common/http';

@Injectable()
export class PayrollService extends AbstractService {
    endpoint = 'payrolls';


    getPayslip(action, payrollId) {

        let headers = new HttpHeaders();
        headers = headers.set('Accept', 'application/pdf');

        return this.http.post(this.apiURL() + '/get-payslip', {
            payroll_id: payrollId,
            action: action
        }, {responseType:'arraybuffer'})
            .map((result: any) => {
                console.log('result', result)
                return new Blob([result], { type: 'application/pdf', });
            }, (err: any) => {
                console.log('error' ,err)
                return err;
            })
            .catch(this.handleError);
    }

    recalculate(payrollId) {

        return this.http.post(this.apiURL() + '/recalculate', {
            payroll_id: payrollId
        })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
    }

    openPDF(payrollId) {
        return this.http.post(this.apiURL() + '/recalculate', {
            payroll_id: payrollId
        })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
    }
}
