import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@shared/forms.module';

import { FormModule as BaseFormModule } from '@shared/base/form/form.module';

/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { OverlayModule } from '@shared/overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */

import { RoleService } from '@services/role.service';
import { RolesComponent } from './roles.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BaseFormModule,
    OverlayModule,
    NgbModule,
  ],
  declarations: [
    RolesComponent,
  ],
  exports: [
    RolesComponent,
  ],
  providers: [
    RoleService,
  ]
})
export class RolesModule { }
