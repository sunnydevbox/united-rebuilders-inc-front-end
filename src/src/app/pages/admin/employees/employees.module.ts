import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EmployeesComponent } from './employees.component';
import { RoutingModule } from './routing.module';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
  ],
  declarations: [
    EmployeesComponent,
  ],
  exports: [
    EmployeesComponent
  ],
  providers: [
  ]
})
export class EmployeesModule { }
