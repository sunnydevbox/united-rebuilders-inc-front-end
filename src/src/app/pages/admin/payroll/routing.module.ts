import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { Route } from './../../../core/route.service';
import { PayrollComponent } from './payroll.component';
import { LandingComponent } from './landing/landing.component';

const routes: Routes = [
    {
        path: '',
        component: PayrollComponent,
        data: { title: 'Roles'  },
        children:[
            {
              path: '',
              component: LandingComponent,
            },
            {
              path: 'payslip/:id',
              loadChildren: '@admin/payroll/payslip/payslip.module#PayslipModule',
              data: {
                formMode: 'update',
              },
            },
            {
              path: 'create',
              loadChildren: '@admin/payroll/crud/form/form.module#FormModule',
              data: {
                formMode: 'create',
              },
            },
            {
              path: 'generate-report',
              loadChildren: '@admin/payroll/generate-report/generate-report.module#GenerateReportModule',
            },
          ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}