import { FormBuilder, FormArray, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
  'type': ['employee-id'],
  'employee_id_number' : [null, Validators.compose([Validators.minLength(2), Validators.maxLength(30)])],
};


