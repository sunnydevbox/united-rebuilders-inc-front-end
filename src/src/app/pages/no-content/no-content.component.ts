import { Component } from '@angular/core';

@Component({
  selector: 'no-content',
  template: `
    <div>
      <h1>Page not found</h1>
      <p>Sorry. We couldn't find the page you are looking for.</p>
    </div>
  `
})
export class NoContentComponent {

}
