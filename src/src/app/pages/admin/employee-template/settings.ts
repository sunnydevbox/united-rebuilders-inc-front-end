import { EmployeeTemplateService } from '@services/employee-template.service';

export const settings = {
    plural: 'Employee Templates',
    singular: 'Employee Template',
    service: EmployeeTemplateService,
    uriPrefix: 'employee-template',

    crud: {
      create: {
        allow: true, // true/false
        url: '/app/settings/system/employee-templates/create'
      },
      read: {

      },
      update: {
        allow: true,
        url: '/app/settings/system/employee-templates/edit/'
      },
      delete: {
        allow: true
      },
      list: {
        allow: true,
        url: '/app/settings/system/employee-templates'
      }
    }
  };
