import { EmploymentTypeService } from '@services/employment-types.service';

export const settings = {
    plural: 'Employment Types',
    singular: 'Employment Type',
    service: EmploymentTypeService,    
  
    crud: {
      create: {
        allow: true, // true/false
        url: '/app/system/employment-types'
      },
      read:{
  
      },
      update:{
        allow: true,
        url: '/app/system/employment-types/edit/'
      },
      delete:{
        allow: true
      },
      list: {
        allow: true,
        url: '/app/system/employment-types'
      }
    }
  };