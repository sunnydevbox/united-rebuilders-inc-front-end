import { HolidayTypeService } from '@services/holiday-type.service';

export const settings = {
    plural: 'Holiday Types',
    singular: 'Holiday Type',
    service: HolidayTypeService,
    crud: {
      create: {
        allow: true, // true/false
        url: '/app/system/holiday-types'
      },
      read: { },
      update: {
        allow: true,
        url: '/app/system/holiday-types/edit/'
      },
      delete: {
        allow: true
      },
      list: {
        allow: true,
        url: '/app/system/holiday-types'
      }
    }
  };
