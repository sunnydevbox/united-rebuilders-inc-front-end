import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownMultiselectorComponent } from './dropdown-multiselector.component';

describe('DropdownMultiselectorComponent', () => {
  let component: DropdownMultiselectorComponent;
  let fixture: ComponentFixture<DropdownMultiselectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DropdownMultiselectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DropdownMultiselectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
