import { Component, OnInit } from '@angular/core';

// IMPORT THE BASE COMPONENT
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';
import { MODEL } from './model';

@Component({
  selector: 'app-basic-salary',
  templateUrl: './basic-salary.component.html',
  styleUrls: ['./basic-salary.component.css']
})
export class BasicSalaryComponent extends BaseFormComponent {

 
  _postConstruct() {
    this.form = this.formBuilder.group(MODEL);
  }
}
