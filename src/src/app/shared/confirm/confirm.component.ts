import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'ngbd-modal-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})
export class ConfirmComponent {
  @Input() title;
  @Input() body;
  @Input() ctaPositive: any = 'Yes';
  @Input() ctaNegative: any = 'Cancel';

  constructor(public activeModal: NgbActiveModal) { }

}
