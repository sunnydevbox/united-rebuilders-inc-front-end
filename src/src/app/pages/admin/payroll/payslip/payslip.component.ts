import { Component } from '@angular/core';
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';
import { settings } from './../settings';
import { PayrollItemService } from '@services/payroll-item.service';
import { ModalService } from '@shared/modal/modal.service';
import { CustomItemFormComponent } from './custom-item-form/custom-item-form.component';
import { NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-payslip',
  templateUrl: './payslip.component.html',
  styleUrls: ['./payslip.component.css']
})
export class PayslipComponent extends BaseFormComponent {

  public payslip;
  private payrollItemService;
  private popupFormModalService;

  _postConstruct() {
    this.popupFormModalService = this.injector.get(ModalService);
    this.service = this.injector.get(settings.service);
    this.payrollItemService = this.injector.get(PayrollItemService);
    this.formMode = this.route.snapshot.data.formMode;
    
    if (this.formMode == 'update') {
      this.toggleOverlay = true;
      
      this.objectId = this.router.parseUrl(this.router.url).root.children.primary.segments[5];
      this.getPayslip(this.objectId);
    }
  }
  
  openPDF(payrollId) {
    this.service.getPayslip('view', payrollId).subscribe(
      (res) => {
        

        let pdf = URL.createObjectURL(res);
        console.log('res', pdf)

        window.open(pdf, "_blank");
      },
      (error) => {
        console.log('error', error)
      }
    );

    return null;
  }

  getPayslip(payrollId) {
    this.service.show(payrollId, {
      with: 'items;payroll_log;employee.job_position',
      groupitems: true,
    }).subscribe(
      (res) => {
        this.payslip = res.data;
      },
      (error) => {
        this.toggleOverlay = false;
        this.toastr.error('Invalid Payslip');
      },
      () => {
        this.toggleOverlay = false;
      }
    )
  }


  openAdjustmentForm(item) {
    
    let modalOptions: NgbModalOptions = {
      size: 'lg',
    }

    let title = 'Add adjustment';
    if (item) {
      title = 'Edit ' + item.name;
    }

    let modalSettings = {
      data: {
        payslip: this.payslip,
        item: item,
      },
      template: CustomItemFormComponent,
      title: title,
    }
    
    let callback = (modal) => {
      this.getPayslip(this.objectId);
      modal.componentInstance.activeModal.dismiss('close');
    };

    this.triggerModal(modalOptions, modalSettings, callback);
  }

  emitterHandler(m, callback) {
    let instance = m.componentInstance.componentRef.instance;

    if (!!instance.notesChanged) {
      instance.notesChanged.subscribe((res) => {
        callback(m, res);
        
        // this.getPayslip(this.objectId);
        // m.componentInstance.activeModal.dismiss('close');
      });
    }

    if (!!instance.cancelModalChanged) {
        instance.cancelModalChanged.subscribe((res) => {
          m.componentInstance.activeModal.dismiss('close');
        });
    }

    if (!!instance.closeForm) {
      instance.closeForm.subscribe((res) => {
        m.componentInstance.activeModal.dismiss('close');
      });
    }

    if (!!instance.triggerReloadPayslipChanged) {
      instance.triggerReloadPayslipChanged.subscribe((res) => {
        m.componentInstance.activeModal.dismiss('close');
        this.getPayslip(this.objectId);
      });
    }
  }

  triggerModal(modalOptions, modalSettings = {}, callback) {
    this.popupFormModalService.setComponent(CustomItemFormComponent);

    let m = this.popupFormModalService.open(modalOptions, modalSettings);

    this.emitterHandler(m, callback);
  }

  deleteAdjustment(item) {
    const sta = this.confirmModalService.open({
      title: 'Are you sure you want to delete "'+item.name+'"?',
      body: 'Deleting this adjustment will recalculate this payslip.',
      ctaPositive: 'Continue',
    },
    () => {
      this.payrollItemService.destroy(item.id).subscribe(
        (res) => {
          this.getPayslip(this.objectId);
          this.toastr.success('Adjustment: "' + item.name + '" removed');
        },
        (err) => {
          console.log('err ', err);
        }
      )
    });
  }

}
