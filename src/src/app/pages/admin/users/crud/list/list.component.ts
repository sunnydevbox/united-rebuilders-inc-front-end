import { Component, OnInit } from '@angular/core';

// IMPORTT HE BASE COMPONENT
import { ListComponent as BaseListComponent } from '@shared/base/list/list.component';

// Others..
import { settings } from './../../settings';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseListComponent {

  public settings;

  _postConstruct() {
    this.settings = settings;
    this.service = this.injector.get(settings.service);


    this.defaultParams = {
      with: 'roles',
    };
  }
}
