import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhilhealthTableComponent } from './philhealth-table.component';
import { RoutingModule } from './routing.module';
import { settings } from './settings';
import { ListModule } from './crud/list/list.module';

@NgModule({
  declarations: [PhilhealthTableComponent],
  imports: [
    CommonModule,
    RoutingModule,
    ListModule
  ],
  providers: [
    settings.service
  ],
})
export class PhilhealthTableModule { }
