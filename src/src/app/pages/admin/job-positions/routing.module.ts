import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { Route } from './../../../core/route.service';

import { ListComponent } from './crud/list/list.component';
import { FormComponent } from './crud/form/form.component';
import { JobPositionsComponent } from './job-positions.component';

const routes: Routes = [
    {
        path: '',
        //component: ListComponent,
        data: { title: 'Job Positions'  },
        children:[
            {
              path: '',
              component: JobPositionsComponent,
            },
            {
              path: 'edit/:id',
              component: FormComponent,
              data: { title: 'Edit Job Posiition'  },
            },
          ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}