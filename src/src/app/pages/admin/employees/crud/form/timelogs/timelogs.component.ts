import { Component, OnInit, Input, EventEmitter, OnChanges, SimpleChanges } from '@angular/core';

import { ListComponent as BaseListComponent } from '@shared/base/list/list.component' ;

import { TimelogService } from '@services/timelog.service';

import { UserService } from '@services/user.service';
import { distanceInWords } from 'date-fns';

import { minutesToHoursMinutes } from '@admin/config';

@Component({
  selector: 'app-timelogs',
  templateUrl: './timelogs.component.html',
  styleUrls: ['./timelogs.component.css']
})
export class TimelogsComponent extends BaseListComponent implements OnChanges {

  public userService;

  @Input() object;

  _postConstruct() {
    // this.loadListOnInit = false;
    this.service = this.injector.get(TimelogService);
    this.userService = this.injector.get(UserService);
  }

  minutesToHoursMinutes(minutes) {
    return minutesToHoursMinutes(minutes);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.object && changes.object.currentValue) {

      this.defaultParams = {
        search        : changes.object.currentValue.id,
        searchFilters : 'employee_id',
        filter: 'id;status;time_in;shift_template_id;shift_template_period_id;'
                + 'time_elapsed;tardy_minutes;overtime_minutes;billable_minutes;'
                + 'time_out;employee_id',
        with: 'employee;shiftTemplatePeriod;shiftTemplate',
        orderBy: 'time_in',
        sortedBy: 'desc'
      };
    }
  }

}
