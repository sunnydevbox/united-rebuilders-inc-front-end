import { TestBed, inject } from '@angular/core/testing';

import { RouteDashboardService } from './route-dashboard.service';

describe('RouteDashboardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RouteDashboardService]
    });
  });

  it('should be created', inject([RouteDashboardService], (service: RouteDashboardService) => {
    expect(service).toBeTruthy();
  }));
});
