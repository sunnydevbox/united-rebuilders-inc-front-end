import { Component, OnInit } from '@angular/core';
import { settings } from './settings';

@Component({
  selector: 'app-sss-table',
  templateUrl: './sss-table.component.html',
  styleUrls: ['./sss-table.component.css']
})
export class SssTableComponent implements OnInit {

  public settings;
  
  constructor() { 
    this.settings = settings;
  }

  ngOnInit() {
  }

}
