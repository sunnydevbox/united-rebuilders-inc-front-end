import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConfirmService } from './../../confirm/confirm.service';
import { FormsModule } from './../../forms.module';
import { NgxErrorsModule } from '@ultimate/ngxerrors';

import { FormComponent } from './form.component';
import { ErrorMessageService } from '@services/errors-message.service';
/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { OverlayModule } from './../../overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */




@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    OverlayModule,
    NgbModule,
    NgxErrorsModule,
  ],
  declarations: [
    FormComponent,
  ],
  exports: [
    FormComponent,
  ],
  providers: [
    ConfirmService,
    ErrorMessageService,
  ]
})
export class FormModule { }
