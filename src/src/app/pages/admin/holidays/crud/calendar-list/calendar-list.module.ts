import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { DataTableModule } from 'angular5-data-table';
import { OverlayModule } from '@shared/overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */


import { ListModule as BaseListModule } from '@shared/base/list/list.module' ;
import { CalendarListComponent } from './calendar-list.component';
import { CalendarModule } from 'angular-calendar';
import { HolidayService } from '@services/holiday.service';

@NgModule({
  imports: [
    CommonModule,
    CalendarModule.forRoot(),
    BaseListModule,
    OverlayModule,
    NgbModule
  ],
  declarations: [
    CalendarListComponent,
  ],
  exports: [
    CalendarListComponent,
  ],
  providers: [
    HolidayService,
  ]
})
export class CalendarListModule { }
