import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule as OrigFormModule, ReactiveFormsModule } from '@angular/forms';
import { NgxErrorsModule } from '@ultimate/ngxerrors';


@NgModule({
  imports: [
    CommonModule,
    OrigFormModule,
    ReactiveFormsModule,
    NgxErrorsModule,
  ],
  declarations: [],
  exports: [
    OrigFormModule,
    ReactiveFormsModule,
    NgxErrorsModule
  ]
})
export class FormsModule { }
