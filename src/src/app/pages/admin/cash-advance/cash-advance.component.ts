import { Component, OnInit } from '@angular/core';
import { settings } from './settings';

@Component({
  selector: 'app-cash-advance',
  templateUrl: './cash-advance.component.html',
  styleUrls: ['./cash-advance.component.css']
})
export class CashAdvanceComponent implements OnInit {

  public settings;
  constructor() { 
    this.settings = settings;
  }

  ngOnInit() {
  }

}
