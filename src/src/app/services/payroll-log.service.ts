import { Injectable } from '@angular/core';
import { AbstractService } from './AbstractService';

@Injectable()
export class PayrollLogService extends AbstractService {
    endpoint = 'payroll-logs';

    triggerGenerate(payrollLogId) {
        return this.http.post(this.apiURL() + '/generate', {
                payroll_log_id: payrollLogId
            }, {
            reportProgress: true,
            responseType: 'json'
          })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
        
    }
}
