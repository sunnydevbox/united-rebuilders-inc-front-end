import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopupsearchComponent } from './popupsearch.component';

describe('PopupsearchComponent', () => {
  let component: PopupsearchComponent;
  let fixture: ComponentFixture<PopupsearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopupsearchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopupsearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
