import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterComponent } from './filter.component';
import { settings } from './settings';
import { FormsModule } from '@shared/forms.module';
import { FormModule as BaseFormModule } from '@shared/base/form/form.module';
import { PayrollLogService } from '@services/payroll-log.service';
/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { OverlayModule } from '@shared/overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */
import { MyDatePickerModule } from 'mydatepicker';

@NgModule({
  declarations: [
    FilterComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MyDatePickerModule,
  ],
  exports: [
    FilterComponent,
    BaseFormModule,
  ],
  providers: [
    settings.service,
    PayrollLogService,
  ]
})
export class FilterModule { }
