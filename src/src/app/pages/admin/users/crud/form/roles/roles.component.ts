import { Component, Output, EventEmitter, Input, SimpleChanges, OnChanges } from '@angular/core';

import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';

import { MODEL } from './model';

import { RoleService } from '@services/role.service';
import { UserService } from '@services/user.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.scss']
})
export class RolesComponent extends BaseFormComponent {

  public userService;
  // @Output() triggerReloadList = new EventEmitter();
  @Input() object;
  @Input() objectId;
  @Input() lists;

  _postConstruct() {
    this.service = this.injector.get(RoleService);
    this.form = this.formBuilder.group(MODEL);
    this.userService = this.injector.get(UserService);
  }

  assignRole(role) {
    console.log(role.checked, role.value) ;

    if (role.checked) {

      this.userService.setRoles(this.objectId, {roles: role.value}).subscribe(
        (res) => {
          this.toastr.success('Role attached to user');
        },
        (err) => {
          this.toastr.warn('There was a problem adding the role. Please try again.');
        }
      );
    } else {
      this.userService.removeRole(this.objectId, {roles: role.value}).subscribe(
        (res) => {
          this.toastr.success('Role detached from user');
        },
        (err) => {
          this.toastr.warn('There was a problem removing the role. Please try again.');
        }
      );
    }
  }

  isAttached(role) {
    if (this.object && this.object.roles) {
      for (const key in this.object.roles) {
        if (this.object.roles[key].name === role) {
          return true;
        }
      }
    }

    return false;
  }
}
