import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutingModule } from './routing.module';
import { PayrollGeneratorComponent } from './payroll-generator.component';
import { FiltersModule } from './filters/filters.module';
import { ListModule } from './crud/list/list.module';
import { FormModule } from './crud/form/form.module';
@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    FiltersModule,
    ListModule,
    FormModule,
  ],
  declarations: [
    PayrollGeneratorComponent,
  ],
  exports: [
    PayrollGeneratorComponent,
  ],
  providers: [
  ]
})
export class PayrollGeneratorModule { }
