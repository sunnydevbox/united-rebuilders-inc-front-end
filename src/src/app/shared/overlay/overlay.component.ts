import { Component, OnInit, Input, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-overlay',
  templateUrl: './overlay.component.html',
  styleUrls: ['./overlay.component.scss']
})
export class OverlayComponent implements OnInit {

  @Input() toggle: Boolean = false;

  ngOnchanges(simple: SimpleChanges) {
    console.log(simple);
  }

  constructor() { }

  ngOnInit() {}
}
