import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { DropdownMultiselectorComponent } from './dropdown-multiselector.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgMultiSelectDropDownModule
  ],
  declarations: [
    DropdownMultiselectorComponent,
  ],
  exports: [
    DropdownMultiselectorComponent,
  ]
})
export class DropdownMultiselectorModule { }
