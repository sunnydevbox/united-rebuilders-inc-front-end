import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { ListComponent as BaseListComponent } from '@shared/base/list/list.component' ;

import { settings } from './../../settings';
import { UserService } from '@services/user.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseListComponent {

  public userService;

  @Output() object = new EventEmitter();

  _postConstruct() {
    this.service = this.injector.get(settings.service);
    this.userService = this.injector.get(UserService);
  }

  edit(object) {
    this.object.emit(object);
  }

}
