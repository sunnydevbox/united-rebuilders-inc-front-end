// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  version: require('./../../package.json').version,
  API_BASE_URL: 'http://cebuunitedrebuilders-api.localhost/api/',
  API_HEADER_ACCEPT: 'application/x.cebuunitedrebuilders.v1+json',
  BASE_URL: '/',

  LOGIN_URL: 'login',
  LOGIN_LABEL: 'Email', // email / username

  admin: {
    ENABLED: true,
    TITLE: 'United Rebuilders Inc. (Dev)',
    REDIRECT_URL: 'app',
  },

  app: {
    ENABLED: false,
    TITLE: 'United Rebuilders Inc.',
    REDIRECT_URL: 'me',
  }
};
