import { Injectable } from '@angular/core';

import { HttpService } from './../core/http/http.service';
import { HttpCacheService } from './../core/http/http-cache.service';

import { HttpClient } from '@angular/common/http';

import { environment } from './../../environments/environment';

// import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
// export abstract class AbstractService implements ServiceInterface {
export class AbstractService {
    public endpoint: String = 'changethis';

    constructor(
        public http: HttpClient
    ) {
       // this.setEndpoint();
    }

    index(e?: any): Observable<any> {
        // {
        //     search: string,
        //     orderBy: column name,
        //     sortedBy: asc|desc,
        //     dateRange: 0


        // take
        // page
        // }
        let sq = '';

        if (e) {
            if (e.offset) {
                sq += '&page=' + (e.offset + 1);
            } else if (e.pageSize) {
                sq += '&take=' + e.pageSize;
            } else if (typeof(e) === 'string') {

                sq += '&' + e;
            }
        }

        sq = this.buildQueryString2(e);
        // console.log('sq', sq)

        return this.http.get(this.apiURL() + '?' + sq)
            .map((result: any) => {
                return result;
            })
            .catch(this.handleError);
    }

    show(id: any, params?: any): Observable<any> {
        return this.http.get(this.apiURL() + '/' + id + this.buildQueryString(params))
        .map((result: any) => {
            return result;
        })
        .catch(this.handleError);
    }

    destroy(id: any) {
        return this.http.delete(this.apiURL() + '/' + id)
        .map((result: any) => {
            return result;
        })
        .catch(this.handleError);
    }

    update(id: any, params: any) {
        return this.http.put(this.apiURL() + '/' + id, params)
            .map((result: any) => {
                return result;
            })
            .do(data => {
                // console.log('createProduct: ' + JSON.stringify(data))
            })
            .catch(this.handleError)
            ;
            // .map((result: any) => {
            //     return result;
            // })
            // .catch(this.handleError)
    }

    store(data: any, options: any) {
        return this.http.post(this.apiURL(), data, {
            reportProgress: true,
            responseType: 'json'
          })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
    }

    upload(data) {
        return this.http.post(this.apiURL(), data, {
            reportProgress: true,
            responseType: 'json'
          })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
    }

    upload2(endpoint, data) {
        return this.http.post(this.apiURL() + endpoint, data, {
            reportProgress: true,
            responseType: 'json'
          })
            .map((result: any) => {
                return result;
            }, (err: any) => {
                return err;
            })
            .catch(this.handleError);
    }

    buildQueryString2(filterQuery?) {
        let sq = '';
        for (const filter in filterQuery) {
            if (typeof(filter) === 'string') {
                sq += filter + '=' + filterQuery[filter] + '&';
            } else if (typeof(filter) === 'object') {
                Object.keys(filter).map(
                    (key) => {
                        sq += key + '=' + filter[key];
                    }
                );
            } else {}
        }
        return sq;
    }

    buildQueryString(params: any) {
        let sq = '';

        if (params) {
            sq += '?';
            if (typeof(params) === 'string') {
                sq += params;
            } else if (typeof(params) === 'object') {
                Object.keys(params).map(
                    (key) => {
                        // console.log(params[key])
                        sq += '&' + key + '=' + params[key];
                    }
                );
            } else {}
            // if (params.offset) {
            //     sq += '&page=' + (params.offset + 1);
            // } else if (params.pageSize) {
            //     sq += '&take=' + params.pageSize;
            // } else if (typeof(params) == 'string') {

            //     sq += '&' + params
            // } else {
            //     sq +=
            // }
        }

        return sq;
    }

    apiURL() {
        return environment.API_BASE_URL + this.endpoint;
    }

    handleError(error: Response | any) {
        return Observable.throw(error); // <= B
    }


    paramsToQueryString(params: any) {
        let result = {};

        result = Object.assign(result, params);

        if (params.offset != null) {
            result['page'] = (params.offset / params.limit) + 1;
        }
        if (params.limit != null) {
            result['limit'] = params.limit;
            // result.push(['limit', params.limit]);
        }
        if (params.sortBy != null) {
            result['sortedBy'] = params.sortAsc ? 'ASC' : 'DESC';
            // result.push(['sortedBy', params.sortBy]);
        }
        if (params.sortAsc != null) {
            result['orderBy'] = params.sortBy;
        }

        return result;
    }

}


export interface ServiceInterface {
    endpoint: String;

    index: Function;
    show: Function;
    destroy: Function;
    update: Function;
    store: Function;
}
