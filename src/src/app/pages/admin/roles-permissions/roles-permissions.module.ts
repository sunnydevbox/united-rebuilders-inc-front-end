import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RolesPermissionsComponent } from './roles-permissions.component';
import { ListModule } from './crud/list/list.module';
import { RoutingModule } from './routing.module';
import { settings } from './settings';
import { PermissionService } from '@services/permission.service';

@NgModule({
  imports: [
    CommonModule,
    ListModule,
    RoutingModule,
  ],
  declarations: [
    RolesPermissionsComponent,
  ],
  exports: [
    RolesPermissionsComponent,
  ],
  providers: [
    settings.service,
    PermissionService,
  ],
})
export class RolesPermissionsModule { }
