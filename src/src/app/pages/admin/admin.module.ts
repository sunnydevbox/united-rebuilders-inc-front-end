import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// import { UsersModule  } from './users/users.module'
// import { RolesModule  } from './roles/roles.module'
// import { HomeModule  } from './home/home.module'


import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { CheckPermissionGuard } from './../../core/guards/check-permissions.guard';

import { OverlayComponent  } from '@shared/overlay/overlay.component';
import { NoPermissionViewComponent } from './../no-permission-view/no-permission-view.component';

@NgModule({
  imports: [
    CommonModule,

    // UsersModule,
    // RolesModule,
    // HomeModule,
    AdminRoutingModule,
    // CrudModule,
  ],
  declarations: [
    // OverlayComponent, // Use by majority of the cruds
    AdminComponent,
    NoPermissionViewComponent,
  ],
  exports: [
    // OverlayComponent, // Use by majority of the cruds
    AdminComponent,
  ],
  providers: [
    CheckPermissionGuard
  ]
})
export class AdminModule { }
