import { Component, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';
import { MODEL } from './model';
import { settings } from './settings';
import { ERRORS, datePickerOptions1, formatDateFix, myDatePickerFormatter } from '@admin/config';

// import { SupplementDataService } from '@services/supplement-data.service';
//import { IMyDpOptions, IMyDateModel } from 'mydatepicker';

@Component({
  selector: 'app-custom-payroll-period-form',
  templateUrl: './custom-payroll-period-form.component.html',
  styleUrls: ['./custom-payroll-period-form.component.css']
})
export class CustomPayrollPeriodFormComponent extends BaseFormComponent implements OnChanges {

  public datePickerOptions = datePickerOptions1;

  @Input() object;
  @Input() payrollTemplateId;
  @Output() objectChanged = new EventEmitter();
  @Output() triggerReloadList = new EventEmitter();

  _postConstruct() {
    this.errors = ERRORS;
    this.form = this.formBuilder.group(MODEL);
    this.service = this.injector.get(settings.service);
  }

  _preSubmit() {

    this.form.get('start').setValue(
      (typeof this.form.get('start').value == 'string') 
        ? this.form.get('start').value 
        : (this.form.get('start').value.date.year + '-' +
          this.form.get('start').value.date.month + '-' +
          this.form.get('start').value.date.day)
    );

    this.form.get('end').setValue(
      (typeof this.form.get('end').value == 'string') 
        ? this.form.get('end').value 
        : (this.form.get('end').value.date.year + '-' +
          this.form.get('end').value.date.month + '-' +
          this.form.get('end').value.date.day)
    );

    this.form.get('pay_coverage_end').setValue(
      (typeof this.form.get('pay_coverage_end').value == 'string') 
        ? this.form.get('pay_coverage_end').value 
        : (this.form.get('pay_coverage_end').value.date.year + '-' +
          this.form.get('pay_coverage_end').value.date.month + '-' +
          this.form.get('pay_coverage_end').value.date.day)
    );

    this.form.get('pay_coverage_start').setValue(
      (typeof this.form.get('pay_coverage_start').value == 'string') 
        ? this.form.get('pay_coverage_start').value 
        : (this.form.get('pay_coverage_start').value.date.year + '-' +
          this.form.get('pay_coverage_start').value.date.month + '-' +
          this.form.get('pay_coverage_start').value.date.day)
    );

    this.form.get('pay_date').setValue(
      (typeof this.form.get('pay_date').value == 'string') 
        ? this.form.get('pay_date').value 
        : (this.form.get('pay_date').value.date.year + '-' +
          this.form.get('pay_date').value.date.month + '-' +
          this.form.get('pay_date').value.date.day)
    );
  }

  _onUpdateSuccess(res) {
    super._onUpdateSuccess(res);
    this.resetForm();

    this.triggerReloadList.emit(true);
  }

  includeSSSCheckbox(e) {
    this.form.get('include_sss').setValue((e.target.checked) ? 1 : 0);
  }

  resetForm() {
    this.cancel();
  }

  cancel() {
    this.form.get('start').reset();
    this.form.get('end').reset();

    // Close the form
    let customPayrollBtn: HTMLElement = document.getElementById('custom-payroll-form-btn') as HTMLElement;
    customPayrollBtn.click();
    this.objectChanged.emit(null);
    this.objectId = null;
    this.formSubmitting = false;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.payrollTemplateId && changes.payrollTemplateId.currentValue) {
      this.form.get('payroll_template_id').patchValue(changes.payrollTemplateId.currentValue);
    }

    if (changes.object && changes.object.currentValue) {
      this.formMode = 'update';
      this.objectId = changes.object.currentValue.id;

      this.form.get('include_sss').setValue(changes.object.currentValue.include_sss);
      this.form.get('period_count').setValue(changes.object.currentValue.period_count);
    
      this.form.get('start').setValue( myDatePickerFormatter(changes.object.currentValue.start) );
      this.form.get('end').setValue( myDatePickerFormatter(changes.object.currentValue.end) );
      this.form.get('pay_date').setValue( myDatePickerFormatter(changes.object.currentValue.pay_date) );
      this.form.get('pay_coverage_start').setValue( myDatePickerFormatter(changes.object.currentValue.pay_coverage_start) );
      this.form.get('pay_coverage_end').setValue( myDatePickerFormatter(changes.object.currentValue.pay_coverage_end) );

      this.form.get('start').markAsTouched();
      this.form.get('end').markAsTouched();
      this.form.get('pay_date').markAsTouched();
      this.form.get('pay_coverage_start').markAsTouched();
      this.form.get('pay_coverage_end').markAsTouched();
      
    }
  }

}
