import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { Route } from './../../../core/route.service';

import { ListComponent } from './crud/list/list.component';
import { FormComponent } from './crud/form/form.component';
import { CompanyDepartmentsComponent } from './company-departments.component';

const routes: Routes = [
    {
        path: '',
        //component: ListComponent,
        data: { title: 'Company Departments'  },
        children:[
            {
              path: '',
              component: CompanyDepartmentsComponent,
            },
            {
              path: 'edit/:id',
              component: FormComponent,
              data: { title: 'Edit Company Department'  },
            },
          ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}