import { Component, OnInit } from '@angular/core';

import { settings } from './settings';

@Component({
  selector: 'app-timelogs',
  templateUrl: './timelogs.component.html',
  styleUrls: ['./timelogs.component.css']
})
export class TimelogsComponent implements OnInit {
  
  public settings;

  constructor() {
    this.settings = settings;
  }

  ngOnInit() {
  }

}
