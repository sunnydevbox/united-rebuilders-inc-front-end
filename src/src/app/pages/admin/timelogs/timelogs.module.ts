import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RoutingModule } from './routing.module';
import { TimelogsComponent } from './timelogs.component';
import { settings } from './settings';

// import { EmployeLogsListModule } from './crud/employe-logs-list/employe-logs-list.module';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    // ListModule,
    // FormModule,
    // FormImportModule,
//   EmployeLogsListModule,
  ],
  declarations: [
    TimelogsComponent,
  ],
  exports: [
    TimelogsComponent,
  ],
  providers: [
    settings.service
  ]
})
export class TimelogsModule { }
