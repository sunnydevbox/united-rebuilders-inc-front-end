// import 'zone.js/dist/zone-mix';
import 'reflect-metadata';
// import 'polyfills';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';


// APP-SPECIFIC
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ApiInterceptor } from './interceptors/api.interceptor';
import { HttpStatusInterceptor } from './interceptors/httpStatus.interceptor';

import { CoreModule } from './../app/core/core.module';
import { AdminModule } from './pages/admin/admin.module';
import { PublicModule } from './pages/public/public.module';

import { UserService  } from '@services/user.service';

import { NoContentComponent } from './pages/no-content/no-content.component';

import { ConfirmModule } from './shared/confirm/confirm.module';
import { ModalModule } from './shared/modal/modal.module';
import { ToastModule } from 'ng2-toastr/ng2-toastr';

@NgModule({
  declarations: [
    // OverlayComponent, // Use by majority of the cruds
    AppComponent,
    HomeComponent,
    NoContentComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    CoreModule,

    ToastModule.forRoot(),
    PublicModule,
    AdminModule,

    ConfirmModule,
    ModalModule,

    AppRoutingModule,
  ],
  providers: [
    UserService,
    { provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: HttpStatusInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }