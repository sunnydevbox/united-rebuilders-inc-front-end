import { Component, OnInit, Output, EventEmitter, OnChanges, SimpleChanges, Input } from '@angular/core';

import { ListComponent as BaseListComponent } from '@shared/base/list/list.component' ;

import { settings } from './../../settings';
import { UserService } from '@services/user.service';
import { distanceInWords } from 'date-fns';

import { minutesToHoursMinutes } from '@admin/config';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent extends BaseListComponent implements OnChanges {

  public userService;

  @Output() object = new EventEmitter();
  @Input() filters;


  _postConstruct() {
    // this.loadListOnInit = false;
    this.service = this.injector.get(settings.service);
    this.userService = this.injector.get(UserService);
    this.defaultParams = {
      filter: 'id;purpose;start;end;status;employee_id;leave_type_id;created_at;duration_in_minutes',
      with: 'employee;type',
      // orderBy: 'time_in',
      // sortedBy: 'desc'
    };
  }

  edit(object) {
    this.object.emit(object);
  }

  minutesToHoursMinutes(minutes) {
    return minutesToHoursMinutes(minutes);
  }

  ngOnChanges(changes: SimpleChanges) {
    super.ngOnChanges(changes);
    if (changes.filters && changes.filters.currentValue) {
      // console.log(changes.filters.currentValue, this.defaultParams);

      if (typeof changes.filters.currentValue.query === 'string') {
        this.defaultParams['query'] = changes.filters.currentValue.query;
        this.reloadItems(this.defaultParams);
      }
    }
  }
}
