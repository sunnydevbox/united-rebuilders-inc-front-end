
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
  'year': [null, Validators.compose([Validators.required])],
  'month': [null, Validators.compose([Validators.required])],
  'payroll_template_id': [null, Validators.compose([Validators.required])],
  'pay_date': [null, Validators.compose([])],
  'period_count': [null, Validators.compose([])],
};
