import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { PayrollComponent } from './payroll.component';
// import { Route } from './../../../core/route.service';

const routes: Routes = [
    {
        path: '',
        //component: ListComponent,
        data: { title: 'Payroll'  },
        children:[
            {
              path: '',
              component: PayrollComponent,
            }
          ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}