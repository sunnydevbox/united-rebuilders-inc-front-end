export const environment = {
  production: true,
  version: require('../../package.json').version,
  API_BASE_URL: 'http://cebuunitedrebuilders-api.sunnydevbox.com/api/',
  API_HEADER_ACCEPT: 'application/x.cebuunitedrebuilders.v1+json',
  BASE_URL: '/',

  LOGIN_URL: 'login',
  LOGIN_LABEL: 'Email', // email / username

  admin: {
    ENABLED: true,
    TITLE: 'United Rebuilders Inc.',
    REDIRECT_URL: 'app',
  },

  app: {
    ENABLED: false,
    TITLE: 'United Rebuilders Inc.',
    REDIRECT_URL: 'me',
  }
};
