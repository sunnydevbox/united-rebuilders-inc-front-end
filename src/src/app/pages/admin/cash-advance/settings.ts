import { CashAdvanceService } from '@services/cash-advance.service';

export const settings = {
    plural: 'Cash Advances',
    singular: 'Cash Advance',
    service: CashAdvanceService,    
  
    crud: {
      create: {
        allow: true, // true/false
        url: '/app/settings/system/cash-advances/add'
      },
      read:{
  
      },
      update:{
        allow: true,
        url: '/app/settings/system/cash-advances/edit/'
      },
      delete:{
        allow: true
      },
      list: {
        allow: true,
        url: '/app/settings/system/cash-advances'
      }
    }
  };