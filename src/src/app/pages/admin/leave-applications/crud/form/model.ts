
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
  'employee_full_name': [null],
  'employee_id' : [null, Validators.compose([Validators.required])],
  'leave_type_id': [null, Validators.compose([Validators.required])],
  'purpose': [null, Validators.compose([Validators.required])],
  'start': [null, Validators.compose([])],
  'end': [null, Validators.compose([])],
  'time_in_date': [null, Validators.compose([])],
  'time_in_time': [null, Validators.compose([])],
  'time_out_date': [null, Validators.compose([])],
  'time_out_time': [null, Validators.compose([])],

  // CASH ADVANCE OPTION
  'cash_advance_amount': [null],
  'cash_advance_duration': [null], // Months
};
