import { CompanyDepartmentsService } from '@services/company-departments.service';

export const settings = {
    plural: 'Company Departments',
    singular: 'Company Department',
    service: CompanyDepartmentsService,    
  
    crud: {
      create: {
        allow: true, // true/false
        url: '/app/system/company-departments'
      },
      read:{
  
      },
      update:{
        allow: true,
        url: '/app/system/company-departments/edit/'
      },
      delete:{
        allow: true
      },
      list: {
        allow: true,
        url: '/app/system/company-departments'
      }
    }
  };