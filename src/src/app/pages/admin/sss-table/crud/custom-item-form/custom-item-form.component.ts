import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FormComponent as BaseFormComponent } from '@shared/base/form/form.component';
import { MODEL } from './model';
import { settings } from './../../settings';

@Component({
  selector: 'app-custom-item-form',
  templateUrl: './custom-item-form.component.html',
  styleUrls: ['./custom-item-form.component.css']
})
export class CustomItemFormComponent extends BaseFormComponent implements OnInit {

  
  private activeModal;
  public socialSecurityTotal;
  public totalContributionTotal;
  
  @Input() item;
  @Output() closeForm = new EventEmitter();
  @Output() triggerReloadList = new EventEmitter();

  _postConstruct() {
    this.service = this.injector.get(settings.service);
    this.form = this.formBuilder.group(MODEL);
  }

  cancel() {
    this.closeForm.emit(true);
  }

  _onUpdateSuccess(res) {
    super._onUpdateSuccess(res);
    this.triggerReloadList.emit(true);
  }

  _onSaveSuccess(res) {
    super._onSaveSuccess(res);
    this.triggerReloadList.emit(true);
  }

  calcSecurityTotal() {

    let ss_er = parseFloat(this.form.get('social_security_er').value);
    let ss_ee = parseFloat(this.form.get('social_security_ee').value);

    let tc_er = parseFloat(this.form.get('total_contribution_er').value);
    let tc_ee = parseFloat(this.form.get('total_contribution_ee').value);

    this.socialSecurityTotal = (ss_er && ss_ee)? (ss_er + ss_ee) : 'N/A';
    this.totalContributionTotal = (tc_er && tc_ee)? (tc_er + tc_ee) : 'N/A';
  }

  ngOnInit() {
    if (this.item) {
      this.objectId = this.item.id;
      this.formMode = 'update';

      this.form.get('compensation_from').setValue(this.item.compensation_from);
      this.form.get('compensation_to').setValue(this.item.compensation_to);
      this.form.get('monthly_salary_credit').setValue(this.item.monthly_salary_credit);
      this.form.get('social_security_er').setValue(this.item.social_security_er);
      this.form.get('social_security_ee').setValue(this.item.social_security_ee);
      this.form.get('social_security_total').setValue(this.item.social_security_total);
      this.form.get('ec_er').setValue(this.item.ec_er);
      this.form.get('total_contribution_er').setValue(this.item.total_contribution_er);
      this.form.get('total_contribution_ee').setValue(this.item.total_contribution_ee);
      this.form.get('total_contribution_er_ee').setValue(this.item.total_contribution_er_ee);
      this.form.get('total_contribution_se_vm_ofw').setValue(this.item.total_contribution_se_vm_ofw);

      this.socialSecurityTotal = this.item.social_security_total;
      this.totalContributionTotal = this.item.total_contribution_er_ee;

      // this.calcSecurityTotal(); 
      
    }
  }  
}
