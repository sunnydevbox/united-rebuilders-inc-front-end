
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


export const MODEL = {
    // 'name' : [null, Validators.compose([Validators.required, Validators.minLength(2)])],
    'description': [null],
    'time_in': [null, Validators.compose([])],
    'time_in_time': [null, Validators.compose([Validators.required])],
    'time_in_date': [null, Validators.compose([Validators.required])],
    'time_out': [null],
    'time_out_time': [null, Validators.compose([Validators.required])],
    'time_out_date': [null, Validators.compose([Validators.required])],
    'shift_template_period_id': [null, Validators.compose([])],
    'shift_template_id': [null, Validators.compose([])],
};
