import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { Route } from './../../../core/route.service';

import { ListComponent } from './crud/list/list.component';
import { RolesPermissionsComponent } from './roles-permissions.component';

const routes: Routes = [
    {
        path: '',
        //component: ListComponent,
        data: { title: 'Roles'  },
        children:[
            {
              path: '',
              component: RolesPermissionsComponent,
            },
          ]
    },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RoutingModule {
}