import { Component, OnInit } from '@angular/core';

import { settings } from './settings';

@Component({
  selector: 'app-benefits',
  templateUrl: './benefits.component.html',
  styleUrls: ['./benefits.component.css']
})
export class BenefitsComponent implements OnInit {

  public settings;
  public _triggerReloadList;
  public _object;

  constructor() {
    this.settings = settings;
  }

  triggerReloadList(e) {
    if (e) {
      this._triggerReloadList = e;
    }
  }

  getObject(object) {
    console.log(object);
    this._object = object;
  }

  ngOnInit() {
  }
}
