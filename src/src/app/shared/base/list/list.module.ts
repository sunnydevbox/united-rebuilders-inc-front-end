import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListComponent } from './list.component';
import { RouterModule } from '@angular/router';

import { ConfirmService } from './../../confirm/confirm.service';
import { DataTableResource, DataTable } from 'angular5-data-table';

/**
 * COPY THIS TO YOUR IMPORTING MODULE
 * START ...
 */
import { DataTableModule } from 'angular5-data-table';
import { OverlayModule } from './../../overlay/overlay.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
/**
 * ... END
 */

@NgModule({
  imports: [
    CommonModule,
    OverlayModule,
    DataTableModule,
    NgbModule,
    RouterModule
  ],
  declarations: [
    ListComponent,
  ],
  exports: [
    ListComponent,
    RouterModule,
  ],
  providers: [
    ConfirmService,
    DataTable,
  ]
})
export class ListModule { }
