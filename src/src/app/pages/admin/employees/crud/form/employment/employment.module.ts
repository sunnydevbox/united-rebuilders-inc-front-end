import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsModule } from '@shared/forms.module';

/*****************************************/
/**** IMPORT THE BASE MODULE for FORM ****/
/*****************************************/
/***/
/***/ import { FormModule as BaseFormModule } from '@shared/base/form/form.module';
/***/
/** INSTRUCTED FROM BASE FORM MODULE */
/***/
/***/ import { OverlayModule } from '@shared/overlay/overlay.module';
/***/

/*****************************************/
/***/ // import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { MyDatePickerModule } from 'mydatepicker';
import { EmploymentComponent } from './employment.component';
import { DropdownMultiselectorModule } from '@shared/dropdown-multiselector/dropdown-multiselector.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BaseFormModule,
    OverlayModule,
    MyDatePickerModule,
    DropdownMultiselectorModule,
  ],
  declarations: [
    EmploymentComponent,
  ],
  exports: [
    EmploymentComponent,
  ]
})
export class EmploymentModule { }
