import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BenefitsComponent } from './benefits.component';
import { RoutingModule } from './routing.module';
import { BenefitsService } from '@services/benefits.service';
import { ListModule } from './crud/list/list.module';
import { FormModule } from './crud/form/form.module';

@NgModule({
  imports: [
    CommonModule,
    RoutingModule,
    FormModule,
    ListModule,
  ],
  declarations: [
    BenefitsComponent
  ],
  exports: [
    BenefitsComponent,
  ],
  providers: [
    BenefitsService,
  ],
})
export class BenefitsModule { }
