import { TestBed, inject } from '@angular/core/testing';

import { PopupsearchService } from './popupsearch.service';

describe('PopupsearchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PopupsearchService]
    });
  });

  it('should be created', inject([PopupsearchService], (service: PopupsearchService) => {
    expect(service).toBeTruthy();
  }));
});
